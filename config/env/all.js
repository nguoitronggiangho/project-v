'use strict';

var path = require('path'),
  rootPath = path.normalize(__dirname + '/../..');

module.exports = {
  root: rootPath,
  http: {
    port: process.env.PORT || 3000
  },
  https: {
    port: false,

    // Paths to key and cert as string
    ssl: {
      key: '',
      cert: ''
    }
  },
  hostname: process.env.HOST || process.env.HOSTNAME,
  db: process.env.MONGOHQ_URL,
  templateEngine: 'swig',

  // The secret should be set to a non-guessable string that
  // is used to compute a session hash
  sessionSecret: 'MEAN',

  // The name of the MongoDB collection to store sessions in
  sessionCollection: 'sessions',

  // The session cookie settings
  sessionCookie: {
    path: '/',
    httpOnly: true,
    // If secure is set to true then it will cause the cookie to be set
    // only when SSL-enabled (HTTPS) is used, and otherwise it won't
    // set a cookie. 'true' is recommended yet it requires the above
    // mentioned pre-requisite.
    secure: false,
    // Only set the maxAge to null if the cookie shouldn't be expired
    // at all. The cookie will expunge when the browser is closed.
    maxAge: null
  },
  public: {
    languages: [{
      locale: 'en',
      direction: 'ltr',
    }, {
      locale: 'he',
      direction: 'rtl',
    }],
    currentLanguage: 'en',
    cssFramework: 'bootstrap'
  },
  // The session cookie name
  sessionName: 'connect.sid',

  // link check: https://datamarket.azure.com/account/datasets

  // for auto translate text
  // mail: dinhbn@hotmail.com
  // bingClientId: 'subtitle_ai_translator_bing',
  // bingClientSecret: 'qeQr2cu3N6W58b+WdJosx4H6ttCnK2eViinjmhRxcko=',

  //mail: buingocdinh@hotmail.com
  bingClientId: 'subtitle_ai_translator_bing_2',
  bingClientSecret: 'NnPagaiuZv0aQbh0jJNXoudanSCqgULEW3gLwCnZ6Dk=',
  bingLimitText: 2000,
  bingLimitArray: 50,


  // magic number optimize using in auto translate
  bingJobBatchSize: 10,
  bingQueryTextArraySize: 100,
  bingDelayTimeCheckJob: 100,
  
  //youtube API key
  youtubeSearchKey: 'AIzaSyCvGuSEbIhVohtXs_l_zrwOSbwoDcILD64'
  

};
