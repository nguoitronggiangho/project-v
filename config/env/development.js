'use strict';

module.exports = {
  db: 'mongodb://' + (process.env.DB_PORT_27017_TCP_ADDR || 'localhost') + '/mean-dev',
  debug: true,
  logging: {
    format: 'dev',
    options: {
      skip: function (req, res) { return res.statusCode < 400 }
    }
  },
  //  aggregate: 'whatever that is not false, because boolean false value turns aggregation off', //false
  aggregate: false,
  mongoose: {
    debug: true
  },
  hostname: 'http://localhost:3000',
  app: {
    name: 'NEXT SHARE - WE SHARE THE WORLD'
  },
  strategies: {
    local: {
      enabled: true
    },
    landingPage: '/',
    facebook: {
      clientID: '162328294123964',
      clientSecret: '9a835ab29b560d8f19e86b6671490d6a',
      callbackURL: 'http://votesub.local/api/auth/facebook/callback',
      enabled: true
    },
    twitter: {
      clientID: 'Q4MaDVSCL9gJQcWLXhWT4LGTG',
      clientSecret: 'Z9XbFYa7CXBegHg5gSx56TNRv2mx4wxtZgEXZOAd2ETd8gJYpt',
      callbackURL: 'http://localhost:3000/api/auth/twitter/callback',
      enabled: false
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/github/callback',
      enabled: false
    },
    google: {
      clientID: '282164272069-656bll51ps0npka1c2cc18h1tn4v90uj.apps.googleusercontent.com',
      clientSecret: 'pXBorLb-vtcOHzVC_E71QU4z',
      callbackURL: 'http://localhost:3000/api/auth/google/callback',
      enabled: true
    },
    linkedin: {
      clientID: 'DEFAULT_API_KEY',
      clientSecret: 'SECRET_KEY',
      callbackURL: 'http://localhost:3000/api/auth/linkedin/callback',
      enabled: false
    }
  },
  emailFrom: 'SENDER EMAIL ADDRESS', // sender address like ABC <abc@example.com>
  mailer: {
    service: 'SERVICE_PROVIDER', // Gmail, SMTP
    auth: {
      user: 'EMAIL_ID',
      pass: 'PASSWORD'
    }
  }, 
  secret: 'SOME_TOKEN_SECRET'
};
