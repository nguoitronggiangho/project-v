'use strict';

module.exports = {
  // db: 'mongodb://jang:123456@ds061681.mongolab.com:61681/starter',
  db: 'mongodb://' + (process.env.DB_PORT_27017_TCP_ADDR || 'localhost') + '/project-v',
  /**
   * Database options that will be passed directly to mongoose.connect
   * Below are some examples.
   * See http://mongodb.github.io/node-mongodb-native/driver-articles/mongoclient.html#mongoclient-connect-options
   * and http://mongoosejs.com/docs/connections.html for more information
   */
  dbOptions: {
    /*
    server: {
        socketOptions: {
            keepAlive: 1
        },
        poolSize: 5
    },
    replset: {
      rs_name: 'myReplicaSet',
      poolSize: 5
    },
    db: {
      w: 1,
      numberOfRetries: 2
    }
    */
  },
  hostname: 'http://votesub.com',
  app: {
    name: 'Votesub - Everything is translated'
  },
  logging: {
    format: 'combined'
  },
  strategies: {
    local: {
      enabled: true
    },
    landingPage: '/',
    facebook: {
      clientID: '152153228474804',
      clientSecret: '331298ba1761f801c6e9a4355c07818c',
      callbackURL: 'http://votesub.com/api/auth/facebook/callback',
      enabled: true
    },
    twitter: {
      clientID: 'Q4MaDVSCL9gJQcWLXhWT4LGTG',
      clientSecret: 'Z9XbFYa7CXBegHg5gSx56TNRv2mx4wxtZgEXZOAd2ETd8gJYpt',
      callbackURL: 'http://votesub.com/api/auth/twitter/callback',
      enabled: false
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/github/callback',
      enabled: false
    },
    google: {
      clientID: '282164272069-656bll51ps0npka1c2cc18h1tn4v90uj.apps.googleusercontent.com',
      clientSecret: 'pXBorLb-vtcOHzVC_E71QU4z',
      callbackURL: 'http://votesub.com/api/auth/google/callback',
      enabled: true
    },
    linkedin: {
      clientID: 'API_KEY',
      clientSecret: 'SECRET_KEY',
      callbackURL: 'http://localhost:3000/api/auth/linkedin/callback',
      enabled: false
    }
  },
  emailFrom: 'SENDER EMAIL ADDRESS', // sender address like ABC <abc@example.com>
  mailer: {
    service: 'SERVICE_PROVIDER',
    auth: {
      user: 'EMAIL_ID',
      pass: 'PASSWORD'
    }
  },
  secret: 'SOME_TOKEN_SECRET'
};
