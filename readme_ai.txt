I. New collection to store information about: group, channel, play list:
	- meta_data_group
	- meta_data_channel
	- meta_data_play_list
Pls check models of ai_helper package to known how to interact with these collection
II. API provied for tracking and trending: init and using
1. Once time initialize database:
	- call link: /api/aiHelper/init_database
	- http method: GET
2. tracking data:
	- call link: /api/aiHelper/tracking
	- HTTP method: POST
	- body: json format
	- list field:
		+ (!)video_id: String ==> using to create object id and find video information.
		+ user_id: String
		+ (!)action: one of list values: "user_view_video", "user_view_play_list", "user_view_channel"
		+ time_stamp: String, (date-time)
	- return json object: field "tracking" : failed/true
3. trending data:
	- call link: /api/aiHelper/trending
	- HTTP method: POST
	- body: json format
	- list field:
		+ (!)action: one of list values: "top_view_video", "top_view_play_list", "top_view_channel"	
		+ limit_count: limit result json object return
		+ user_id: String
	- return json object: field "trending" : failed/true		
(!): must have, other is optional, will using in update version later