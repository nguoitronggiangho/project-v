'use strict';

require('./packages/custom/videos/server/models/video');
const chalk = require('chalk');
const debug = require('debug')('app');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/mean-dev');
const Video = mongoose.model('Video');
const isAsync = { async: true };

/*
 |--------------------------------------------------------------------------
 | test task
 |--------------------------------------------------------------------------
 */
desc('This is the default task.');
task('test', [], (params) => {
  Video.findById('5736e26c8fe411980c94f4b7', function(err, video) {
    if (err) console.log(chalk.red(err));
    debug(video.toJSON());
  });
  console.log('This is the default task.');
});

/*
 |--------------------------------------------------------------------------
 | test async and call other task after complete using built-in complete()
 |--------------------------------------------------------------------------
 */
desc('This is test async task');
task('testAsync', isAsync, () => {
  setTimeout(function () {
    console.log(chalk.magenta.bgBlue("Yay, I'm asynchronous!"));
    complete(jake.Task['test'].invoke());
  }, 1000);
});