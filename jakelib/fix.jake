'use strict';

require('../packages/custom/videos/server/models/video');
const chalk = require('chalk');
const debug = require('debug')('app');
const mongoose = require('mongoose');
const Video = mongoose.model('Video');

/*
 |--------------------------------------------------------------------------
 | Task with namespace
 |--------------------------------------------------------------------------
 */
namespace('fix', () => {
  desc('This the fix:test task');
  task('test', [], () => {
    console.log(chalk.magenta('doing fix:test task'));
  });
});