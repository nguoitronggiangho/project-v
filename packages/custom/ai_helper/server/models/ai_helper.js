'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// field of language: generic, physic, chemical..
var ai_field_language_schema = new Schema({
    field_language_symbol: {
        type: String,
        unique: true
    },
    field_language_description: {
        type: String
    }
});

// job attribute: pending, running, error, done...
var ai_job_attribute_schema =  new Schema({
    job_attribute_symbol: {
        type: String,
        unique: true
    },
    job_attribute_description: {
        type: String
    }
});

// language support: en-english, vn-vietnam
var ai_language_support_schema =  new Schema({
    language_symbol: {
        type: String,
        unique: true
    },
    language_description: {
        type: String
    },
    active: {
        type: Boolean,
        default: false
    }
});

var ai_reporter_schema =  new Schema({
    time_start: {
        type: Date
    },
    time_end: {
        type: Date
    },
    description: {
        type: String
    }
});

var ai_translate_job_schema = new Schema({
    caption_id:{
        type: Schema.ObjectId
    },
    language_symbol_target: {
        type: String
    },
    job_attribute_symbol: {
        type: String
    },
    created_date: {
        type: Date
    }
});

var ai_cache_translate_text_schema = new Schema({
    raw_text: {
        type: String
    },
    field_language_symbol : {
        type: String
    },
    language_symbol_source: {
        type: String
    },
    language_symbol_target: {
        type: String
    },
    translated_text: {
        type :String
    },
    crow_source_type: {
        type :String
    }
});
// example: google_ai, bing_ai ...
var ai_crow_source_schema = new Schema({
    crow_source_symbol: {
        type :String,
        unique: true
    },
    crow_source_description: {
        type :String
    }
});

// using for trending and tracking information
var meta_data_group_schema = new Schema({
    group_symbol: {
        type :String,
        unique: true
    },
    group_description: {
        type :String
    }
});

var meta_data_video_schema = new Schema({
    video_id: {
        type: Schema.ObjectId,
        ref: 'videos'
    },
    order: {
        type: Number,
        min: 1
    }
});

var meta_data_channel_schema = new Schema({
    channel_name: {
        type :String,
        unique: true
    },
    channel_group_symbol:{type: String},
    channel_description: {type: String},
    channel_keyword: [String],
    chanel_data: [meta_data_video_schema],
    view: {
        type: Number,
        min: 0
    }
});


var meta_data_play_list_schema = new Schema({
    play_list_name: {
        type :String,
        unique: true
    },
    play_list_description: {type: String},
    play_list_keyword: [String],
    play_list_data: [meta_data_video_schema],
    view: {
        type: Number,
        min: 0
    }
});

// for statistic trending and tracking
var tracking_data_view_video_schema = new Schema({
    video_id: {
        type: Schema.ObjectId,
        unique: true,
        ref: 'videos'
    },
    view: {
        type: Number,
        min: 0
    }
});

var tracking_data_view_channel_schema = new Schema({
    channel_id: {
        type: Schema.ObjectId,
        unique: true,
        ref: 'meta_data_channel'
    },
    view: {
        type: Number,
        min: 0
    }
});

var tracking_data_view_play_list_schema = new Schema({
    play_list_id: {
        type: Schema.ObjectId,
        unique: true,
        ref: 'meta_data_play_list'
    },
    view: {
        type: Number,
        min: 0
    }
});

mongoose.model('ai_field_language',ai_field_language_schema);
mongoose.model('ai_job_attribute',ai_job_attribute_schema);
mongoose.model('ai_language_support',ai_language_support_schema);
mongoose.model('ai_reporter',ai_reporter_schema);
mongoose.model('ai_cache_translate_text',ai_cache_translate_text_schema);
mongoose.model('ai_translate_job',ai_translate_job_schema);
mongoose.model('ai_crow_source',ai_crow_source_schema);


// using for trending and tracking information
mongoose.model('meta_data_group',meta_data_group_schema);
mongoose.model('meta_data_channel',meta_data_channel_schema);
mongoose.model('meta_data_play_list',meta_data_play_list_schema);

// using for tracking
mongoose.model('tracking_data_view_video',tracking_data_view_video_schema);
mongoose.model('tracking_data_view_channel',tracking_data_view_channel_schema);
mongoose.model('tracking_data_view_play_list',tracking_data_view_play_list_schema);