/**
 * Created by DinhBN on 11/28/2015.
 */
"use strict";

/**
 * Module dependencies
 *
 *
 */
var mongoose = require('mongoose'),
    config = require('../../../../../core').loadConfig();

var AI_LANGUAGE_SUPPORT = mongoose.model('ai_language_support');

module.exports = function (ai_helper) {
    return {
        aiHelperInitDatabase: function (req, res) {
            // init ai language support
            {
                var language_symbol_array = ['ar', 'bg', 'ca', 'zh-CHS', 'zh-CHT', 'cs', 'da', 'nl', 'en', 'et', 'fi', 'fr', 'de', 'el', 'ht',
                    'he', 'hi', 'mww', 'hu', 'id', 'it', 'ja', 'ko', 'lv', 'lt', 'ms', 'no', 'fa', 'pl', 'pt', 'ro', 'ru', 'sk',
                    'sl', 'es', 'sv', 'th', 'tr', 'uk', 'ur', 'vi', 'ALL'];
                var language_description_array = ['Arabic', 'Bulgarian', 'Catalan', 'Chinese(Simplified)', 'Chinese(Traditional)', 'Czech', 'Danish',
                    'Dutch', 'English', 'Estonian', 'Finnish', 'French', 'German', 'Greek', 'Haitian Creole', 'Hebrew', 'Hindi',
                    'Hmong Daw', 'Hungarian', 'Indonesian', 'Italian', 'Japanese', 'Korean', 'Latvian', 'Lithuanian', 'Malay',
                    'Norwegian', 'Persian', 'Polish', 'Portuguese', 'Romanian', 'Russian', 'Slovak', 'Slovenian', 'Spanish', 'Swedish'
                    , 'Thai', 'Turkish', 'Ukrainian', 'Urdu', 'Vietnamese', 'ALL'];

                var language_symbol_array_active = ['en', 'ja', 'zh-CHS','zh-CHT','ko','vi'];


                for (var i = 0; i < language_symbol_array.length; i++) {
                    var language = new AI_LANGUAGE_SUPPORT;
                    language.language_symbol = language_symbol_array[i];
                    language.language_description = language_description_array[i];

                    if(language_symbol_array_active.indexOf(language.language_symbol) > -1){
                        language.active = true;
                    }else{
                        language.active = false;
                    }

                    language.save();
                }
            }



            res.status(200).json({msg: "ok"});
        }
    }
};