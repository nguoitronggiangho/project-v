'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(AiHelper, app, auth, database) {

  var ai_helper = require('../controllers/ai_helper')(AiHelper);

  app.get('/api/aiHelper/example/anyone', function(req, res, next) {
    res.send('Anyone can access this');
  });

  app.get('/api/aiHelper/example/auth', auth.requiresLogin, function(req, res, next) {
    res.send('Only authenticated users can access this');
  });

  app.get('/api/aiHelper/example/admin', auth.requiresAdmin, function(req, res, next) {
    res.send('Only users with Admin role can access this');
  });

  // init database
  app.route('/api/aiHelper/init_database').get(ai_helper.aiHelperInitDatabase);

  app.get('/api/aiHelper/example/render', function(req, res, next) {
    AiHelper.render('index', {
      package: 'ai_helper'
    }, function(err, html) {
      //Rendering a view from the Package server/views
      res.send(html);
    });
  });
};
