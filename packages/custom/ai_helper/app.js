'use strict';

/*
 * Defining the Package
 */
var Module = require('../../../core').Module;

var AiHelper = new Module('ai_helper');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
AiHelper.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  AiHelper.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  //AiHelper.menus.add({
  //  title: 'aiHelper example page',
  //  link: 'aiHelper example page',
  //  roles: ['authenticated'],
  //  menu: 'main'
  //});
  
  //AiHelper.aggregateAsset('css', 'aiHelper.css');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    AiHelper.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    AiHelper.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    AiHelper.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return AiHelper;
});
