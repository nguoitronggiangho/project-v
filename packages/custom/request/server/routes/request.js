'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Request, app, auth, database) {

  var request = require('../controllers/request')(Request);

  app.route('/api/request')
      .get(request.all)
      .post(auth.requiresLogin, request.create);
  app.route('/api/request/:requestId')
      .get(auth.isMongoId, request.show);
  //     .put(auth.isMongoId, auth.requiresLogin , request.update)
  //     .delete(auth.isMongoId, auth.requiresLogin, request.destroy);

  app.route('/api/request/findForHome/get')
      .get(request.findForHome);
  app.route('/api/request/vote/post')
      .post(auth.isMongoId, auth.requiresLogin, request.vote);
  
  app.route('/api/request/youtube/:id')
      .get(request.getYoutubeInfoById);
  
  app.route('/api/request/search/:term')
      .get(request.search);
  app.route('/api/request/search')
      .get(request.all);
  
  app.route('/api/request/find/approved')
      .get(request.findApproved);
  
  // Finish with setting up the requestId param
  // app.param('requestId', request.article);
};
