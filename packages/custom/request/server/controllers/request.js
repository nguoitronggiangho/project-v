'use strict';

var debug = require('debug')('app:request:controller:' + process.pid);
var mongoose = require('mongoose');
var Request = mongoose.model('Request');
var Youtube = require('youtube-node');
var config = require('../../../../../core').loadConfig();
var _ = require('lodash');

module.exports = function (Requests) {
  return {

    create: function (req, res) {
      console.log(req.body);
      var request = new Request();
      request.user = req.user;
      request.title = req.body.title;
      request.language = req.body.language;
      request.youtubeId = req.body.videoId;
      request.url = 'https://www.youtube.com/watch?v=' + req.body.videoId;
      request.thumbnail = 'https://i.ytimg.com/vi/'+req.body.videoId+'/hqdefault.jpg';

      request.save(function (err) {
        if (err) {
          var modelErrors = [];

          if (err.errors) {
          
            for (var x in err.errors) {
              modelErrors.push({
                param: x,
                msg: err.errors[x].message,
                value: err.errors[x].value
              });
            }
          
            return res.status(400).json(modelErrors);
          }

          return res.status(500).json({
            error: 'Cannot save the request'
          });
        }

        res.json(request)
      })

    },

    all: function(req, res) {

      Request.find({approved: false}).sort('-created').limit(10).populate('user', 'name username').populate('language').exec(function(err, requests) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot list the requests'
          });
        }

        res.json(requests)
      });

    },
    
    findApproved: function(req, res) {
      Request.find({approved: true})
          .limit(10)
          .populate('user', 'name username')
          .populate('language')
          .exec(function(err, requests) {
            if (err) {
              debug('Error while get approved request translate video '.red.inverse, err);
              return res.status(500).json({
                error: err
              })
            }
            res.json(requests)
      })
    },

    show: function(req, res) {
      Request.findById(req.params.requestId).populate('user', 'name username')
          .populate('language')
          .exec(function(err, request) {
            if (err) {
              debug('Error while get request translate data '.red.inverse, err);
              return res.status(500).json({error : err})
            }

            res.json(request)
          })
    },

    findForHome: function (req, res) {
      Request.find({approved: false}).sort('-voteCount').limit(2).populate('user', 'name username avatar').exec(function(err, requests) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot list the requests'
          });
        }

        res.json(requests)
      });
    },

    vote: function (req, res) {
      Request.update({ _id: req.body.requestId },
          { $addToSet: { voted: req.user } },
          function(err, result) {
            res.json(result)
          }
      );
    },

    getYoutubeInfoById: function(req, res) {
      var youtube = new Youtube();

      youtube.setKey(config.youtubeSearchKey);

      youtube.getById(req.params.id, function(err, result) {
        if (err) {
          return res.status(500).json({
            error: 'cannot get youtube video info'
          })
        }

        res.json(result)
      })
    },

    search: function(req, res) {
      var query = {};
      query.title =  new RegExp(req.params.term, 'i');
      query.approved = false;
      Request.find(query).sort('-voteCount').limit(10).populate('user', 'name username avatar')
          .exec(function(err, requests) {
            if (err) {
              debug('Error while search request video '.red.inverse, err);
              return res.status(500).json({
                error: err
              })
            }

            debug(`Search success with ${requests.length} result`.green.inverse);
            res.json(requests)
          })
    }
  }
};