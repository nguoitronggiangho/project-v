'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

/**
 * Validate
 */
var validateUniqueYoutubeId = function(value, callback) {
  var Request = mongoose.model('Request');
  Request.find({
    $and: [{
      url: value
    }, {
      _id: {
        $ne: this._id
      }
    }]
  }, function(err, video) {
    callback(err || video.length === 0);
  })
};

/**
 * Request Schema
 */
var RequestSchema = new Schema({
  youtubeId: {
    type: String,
    unique: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  language: {
    type: Schema.ObjectId,
    ref: 'ai_language_support'
  },
  url: {
    type: String,
    required: true,
    trim: true,
    validate: [validateUniqueYoutubeId, 'Request already exits']
  },
  thumbnail: {
    type: String,
    required: true,
    trim: true
  },
  voted: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  approved: {
    type: Boolean,
    default: false
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

RequestSchema.virtual('voteCount').get(function () {
  return this.voted.length;
});

RequestSchema.plugin(findOrCreate);

mongoose.model('Request', RequestSchema);