'use strict';

angular.module('mean.request').factory('Request', [ '$resource',
  function($resource) {
    return $resource('api/request/:requestId',{
      requestId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      createRequest: {
        url: 'api/request',
        method: 'POST'
      },
      findForHome: {
        url: 'api/request/findForHome/get',
        method: 'GET',
        isArray: true
      },
      vote: {
        url: 'api/request/vote/post',
        method: 'POST'
      },
      getYoutubeInfoById: {
        url: 'api/request/youtube/:id',
        method: 'GET'
      },
      search: {
        url: 'api/request/search/:term',
        method: 'GET',
        isArray: true
      },
      findApprove: {
        url: 'api/request/find/approved',
        method: 'GET',
        isArray: true
    }
    });
  }
]);
