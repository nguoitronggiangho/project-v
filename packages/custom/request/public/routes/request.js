'use strict';

angular.module('mean.request').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider
      .state('list request', {
        url: '/request',
        templateUrl: 'request/views/list.html'
      })
      .state('view request', {
        url: '/request/:requestId',
        templateUrl: 'request/views/list.html'
      })
      .state('create request', {
        url: '/request/create',
        templateUrl: 'request/views/create.html'
      });
  }
]);
