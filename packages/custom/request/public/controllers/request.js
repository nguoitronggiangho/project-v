(function () {
  'use strict';
  angular
      .module('mean.request')
      .controller('RequestController', RequestController);

  RequestController.$inject = ['$scope', 'Global', 'Request', '$location', 'MeanUser', 'Captions', '$mdToast', '$stateParams', '$q', '$debounce'];
  
  function RequestController ($scope, Global, Request, $location, MeanUser, Captions, $mdToast, $stateParams, $q, $debounce) {
    var self = this;
    self.API = null;
    self.host = $location.host();
    
    self.onPlayerReady = function(API) {
      self.API = API;
    };

    self.config = {
      theme: '/videos/assets/css/videogular.css'
    };
    self.hdrvars = {
      authenticated: MeanUser.loggedin,
      user: MeanUser.user,
      isAdmin: MeanUser.isAdmin
    };

    if ($stateParams.v) {
      console.log($stateParams.v);
      Request.getYoutubeInfoById({
        id: $stateParams.v
      }, function(response) {
        console.log(response);
        self.youtubeVideoInfo = {
          title: response.items[0].snippet.title,
          id: response.items[0].id,
          channelTitle: response.items[0].snippet.channelTitle
        };
      });

      self.source = [{
        src: 'https://www.youtube.com/watch?v='+$stateParams.v,
        type: 'video/mp4'
      }]
    }

    if ($stateParams.requestId) {
      Request.get({requestId: $stateParams.requestId}, function(response) {
        // console.log(response);
        self.request = response;
        self.source = [{
          src: 'https://www.youtube.com/watch?v=' + response.youtubeId,
          type: 'video/mp4'
        }]
      });

    }

    this.setVideo = function (index) {
      $('#request-video-info').hide();
      $("html, body").animate({ scrollTop: 0 }, "slow");
      self.API.stop();
      self.source = [{
        src: $scope.requests[index].url,
        type: 'video/mp4'
      }]
    };

    $scope.find = function () {
      // console.log(MeanUser.user);
      Request.query(function (requests) {
        $scope.requests = requests;
        if (!$stateParams.v || !$stateParams.requestId) {
          self.source = [{
            src: requests[0].url,
            type: 'video/mp4'
          }]
        }
      })
    };

    $scope.findApproved = function() {
      Request.findApprove(function(response) {
        // console.log(response);
        self.approved = response;
      })
    };

    this.findForHome = function () {
      Request.findForHome(function (requests) {
        self.requests =requests;
      })
    };
    
    this.getLanguages = function() {
      Captions.findLanguage(function(languages) {
        self.languages = languages;
      })
    };

    $scope.create = function(isValid) {

      var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
      var videoId = $scope.request.url.match(myregexp)[1];

      if (isValid) {
        // $scope.article.permissions.push('test test');

        var data = {};
        data.title = $scope.request.title;
        data.videoId = videoId;
        data.language = self.selectedLanguage;
        var request = new Request(data);

        request.$save(function(response) {
          // $location.path('request/' + response._id);
          console.log(response);
          $scope.requests.push(response);
          $mdToast.show(
              $mdToast.simple()
                  .textContent('Gửi đề xuất thành công!')
                  .position('top right')
                  .hideDelay(3000)
          );
        }, function(err) {
          console.log(err);
          if (err.status === 400) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Đã có người khác gửi đề xuất video này, bạn hãy vote cho video để được dịch nhanh hơn')
                    .position('top right')
                    .hideDelay(3000)
            );
          } else {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(err.data.error)
                    .position('top right')
                    .hideDelay(3000)
            );
          }
        });

        $scope.request = {};

      } else {
        $scope.submitted = true;
      }
    };

    this.requestSub = function() {
      if (self.youtubeVideoInfo.videoId.length > 0) {
        var data = {};
        data.title = self.youtubeVideoInfo.title;
        data.videoId = self.youtubeVideoInfo.videoId;
        data.language = null;


        var request = new Request(data);

        request.$save(function(response) {
          // $location.path('request/' + response._id);
          console.log(response);
          $mdToast.show(
              $mdToast.simple()
                  .textContent('Gửi đề xuất thành công!')
                  .position('top right')
                  .hideDelay(3000)
          );
        })
      }

    };

    $scope.testToas = function() {
      $mdToast.show(
          $mdToast.simple()
              .textContent('Simple Toast!')
              .position('top right')
              .hideDelay(30000)
      );
    };

    this.vote = function (id, index) {
      var data = {
        requestId: id
      };
      Request.vote(data, function (response) {
        // console.log(response);
        if (response.nModified === 0) {
          alert('you already voted')
        } else {
          $scope.requests[index].voteCount += 1;
        }
      })
    };
    
    /*
     |--------------------------------------------------------------------------
     |  Search request
     |--------------------------------------------------------------------------
     */
    self.searchChange = $debounce(function(term) {
      Request.search({term: term}, function(data) {
        console.log(data);
        $scope.requests = data;
      });
    }, 400  );
    
  }
})();