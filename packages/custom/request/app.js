'use strict';

/*
 * Defining the Package
 */
var Module = require('../../../core').Module;

var Request = new Module('request');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Request.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Request.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  // Request.menus.add({
  //   title: 'request example page',
  //   link: 'request example page',
  //   roles: ['authenticated'],
  //   menu: 'main'
  // });
  
  Request.aggregateAsset('css', 'request.css');
  Request.aggregateAsset('css', 'card.css');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Request.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Request.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Request.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Request;
});
