'use strict';
var redis = require('redis');
var sub = redis.createClient();
var pub = redis.createClient();

module.exports = function(io) {

  var exports = module.exports = {};



  io.sockets.on('connection', function(socket) {
    sub.subscribe('database');

    // Handle receiving messages
    var callback = function (channel, data) {
      console.log('send a messageeeeeeeeeeee');
      socket.emit('message', data);
    };
    sub.on('message', callback);


    exports.fire = function(event, data) {
      // console.log('hola');
      // console.log(data);
      socket.broadcast.emit('time', data)
    };
    console.log('1 socket connected '+ socket.id);
    console.log(socket.handshake.session);

    socket.on('time', function(time) {
      console.log(time);
      console.log('timemmmmemememe')
    });

    socket.on('test', function(data) {
      socket.emit('test', Math.random()*10000)
    })

    // Handle disconnect
    socket.on('disconnect', function () {
      sub.removeListener('message', callback);
    });
  });

  // return {
  //   fire: function (event, data) {
  //     console.log('socket event fired');
  //     io.emit(event, data)
  //   }
  // };
};