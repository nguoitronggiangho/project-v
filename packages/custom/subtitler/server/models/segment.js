'use strict';

var debug = require('debug')('app:segment:model');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SegmentSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  video: {
    type: Schema.ObjectId,
    ref: 'Video'
  },
  language: {
    type: Schema.ObjectId,
    ref: 'ai_language_support'
  },
  channel: {
    type: Schema.ObjectId,
    ref: 'Channel'
  },
  segment: Array
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

mongoose.model('Segment', SegmentSchema);