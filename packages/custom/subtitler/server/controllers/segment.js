module.exports = (function() {
  'use strict';

  require('../../../videos/server/models/video');
  const debug = require('debug')('app:segment:controller' + process.pid);
  const mongoose = require('mongoose');
  const Segment = mongoose.model('Segment');
  const Video = mongoose.model('Video');
  
  class Segments {
    
    static createOrUpdate(req, res) {
      debug('request body: \n'.blue.inverse, req.body);
      Video.findById(req.body.video, function(err, video) {
        if (err) {
          debug('Error while checkvideo hasSegment state '.red.inverse, err)
        }

        if (video && video.hasCaption === false) {
          Segment.findOneAndUpdate({video: req.body.video},
              {
                $setOnInsert: {
                  channel: req.body.channel,
                  user: req.user._id
                },
                $set: {
                  language: req.body.language,
                  segment: req.body.segment
                }
              },
              { upsert: true },
              function(err, segment) {
                if (err) {
                  debug('Cannot save or update segment'.red, err);
                  return res.status(500).json({
                    error: 'Cannot save or update segment'
                  })
                }

                res.json(segment);

                video.hasSegment = true;
                video.save(function(err) {
                  if (err) debug('Error while save video hasSegment state '.red.inverse, err)
                })
              });
        } else {
          return res.status(400).send({
            message: 'Cannot find video or Video already has captions'
          })
        }
      });

    }
    
    static getSegmentForVideo(req, res) {
      Segment.findOne({video: req.params.videoId}, function(err, segment) {
        if (err) {
          debug(`Error while get segments for video: ${req.params.videoId} `.red.inverse, err);
          return res.status(500).json({
            error: 'Error while get segments for video'
          })
        }
        
        res.json(segment)
      })
    }
    
  }
  
  return Segments;
  
})();