(function () {
  'use strict';

  /* jshint -W098 */
  // The Package is past automatically as first parameter
  module.exports = function (Subtitler, app, auth, database) {

    let segment = require('../controllers/segment');

    app.route('/api/segment')
        .post(auth.requiresLogin, segment.createOrUpdate);
    app.route('/api/segment/:videoId')
        .get(auth.isMongoId, auth.requiresLogin, segment.getSegmentForVideo)
  };
})();
