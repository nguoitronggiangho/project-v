(function () {
  'use strict';

  /* jshint -W098 */
  angular
    .module('mean.subtitler')
    .controller('SubtitlerController', SubtitlerController);

  SubtitlerController.$inject = ['$scope', '$sce', '$mdConstant', '$stateParams', 'Segment', 'Videos', 'Captions', '$mdToast', '$location', '$mdDialog'];

  function SubtitlerController($scope, $sce, $mdConstant, $stateParams, Segment, Videos, Captions, $mdToast, $location, $mdDialog) {

    var vm = this;

    var videoId = $stateParams.videoId;

    Videos.get({videoId: videoId}, function(response) {
      vm.source = [{src: response.url}];
      vm.poster = response.thumbnail;
      vm.video = response;
    }, function(err) {
      console.log(err)
    });

    Segment.resource.get({videoId: videoId}, function(response) {
      if (response.segment) {
        captionsArray = response.segment;
        angular.forEach(captionsArray, function(segment) {
          var newTemplate = $(template).addClass('ui-resizable')
              .removeClass('new').css('left', segment.start*30).
              width((segment.end - segment.start)*30);
          $('#segments').append(newTemplate);
        });
      }
    }, function(err) {
      console.log(err);
    });

    Captions.findLanguage(function (languages) {
      vm.languages = languages;
      vm.selectedLang = vm.languages[0];
    });

    this.config = {
      sources: [
        {src: $sce.trustAsResourceUrl("/videos/assets/video/payphone.mp4"), type: "video/mp4"},
        // {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
        // {src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
      ],
      theme: "/videos/assets/css/videogular.css",
      plugins: {
        poster: "http://www.videogular.com/assets/images/videogular.png"
      }
    };

    var template = '<li class="segment new"><em></em> \
                        <div class="action"> \
                          <a class="replay-segment tooltip-north" href="javascript:void(0)"> \
                            Replay \
                          </a> \
                          <a class="delete-segment tooltip-north" href="javascript:void(0)"> \
                              Delete \
                          </a> \
                        </div> \
                      </li> \
                      ';

    vm.onUpdateState = function($state) {
      if ($state === 'play') {
        $(document).find('.segment.selected')
            .removeClass('selected')
            .addClass('ui-resizable-disabled ui-state-disabled');
      }
    };

    vm.onPlayerReady = function($API) {
      // console.log($API);
      vm.onUpdateState = function(state) {
        vm.state = state;
      };
      vm.log = function() {
        console.log($API.currentTime/1000);
      };
      vm.logCapArr = function() {
        console.log(captionsArray);
      };

      vm.playPause = function() {$API.playPause();};

      vm.rewind = function() {
        var currentTime = $API.currentTime;
        $API.seekTime((currentTime - 1000)/1000)
      };
      vm.forward = function() {
        var currentTime = $API.currentTime;
        $API.seekTime((currentTime + 1000)/1000)
      };

      vm.save = function() {
        // console.log(captionsArray);
        // $('#textCaptionEntry').val('').blur();
        $(document).find('.segment.selected').each(function() {
          var start = CssToNumber($(this).css('left'))/30 + 1;
          var ci = FindCaptionIndex(start);
          // console.log(ci);
          if (ci !== -1) UpdateCaption(ci, $('#textCaptionEntry').val())
        });
        if (captionsArray.length > 0) {
          var data = {};
          data.segment = captionsArray;
          data.video = videoId;
          data.language = vm.selectedLang._id;
          data.channel = vm.video.channel._id;
          
          Segment.resource.save(data, function(response) {
            // console.log(response);
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Saved subtitle')
                    .position('top right')
                    .hideDelay(3000)
            );
          }, function(err) {
            console.log(err);
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Save subtitle Error')
                    .position('top right')
                    .hideDelay(3000)
            );
          })
        }
      };

      function publishCaption() {
        if (captionsArray.length > 0) {
          var data = {};
          data.sentence = captionsArray;
          data.videoId = videoId;
          data.language = vm.selectedLang._id;
          var caption = new Captions(data);
          caption.$save(function(response) {
            console.log(response);
            $location.path('/videos/' + videoId + '/edit');
          })
        }
      }
      vm.showConfirm = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Would you like to Publish Captions?')
            .textContent('When you click YES you cannot edit the timing of the caption, you only can change the text')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Please do it!')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
          publishCaption()
        }, function() {

        });
      };

      // vm.selectedLang = null;
      // vm.selectLang = function() {
      //   console.log(vm.selectedLang);
      // };

      vm.segmentState = false;
      var left = null;
      var vid = document.getElementById("videoElm");
      var segmentNew = $('.segment.new');


      vm.createSegment = function() {
        var a,b;
        createSegment(a,b)
      };

      $(document).keydown(function(e){
        // console.log(e);
        var tag = e.target.tagName.toLowerCase();
        switch (e.keyCode) {
        case 83:
            // console.log('`S` press, State:'+vm.segmentState);
            // console.log(vm.currentTime);
            if (tag != 'input' && tag != 'textarea') {
              createSegment(vm.segmentState, tag);
            }

            break;
        case 32:
          // console.log('space press');
            break;
        default:

        }
        if (e.keyCode === 32) {

        }
      });

      function createSegment(segmentState, tag) {
        if (vm.segmentState === false && tag != 'input' && tag != 'textarea') {
          var currentTime =  -vm.currentTime;
          var checkInsideOtherCaption = FindCaptionIndex(currentTime/30);
          // console.log(currentTime, checkInsideOtherCaption);
          // console.log(captionsArray);
          if (checkInsideOtherCaption !== -1) {
            $API.pause();
            // vm.segmentState = true;
            return false;
          }

          $API.play();
          var newtemplate = $(template).css('left', currentTime).width(0);
          left = currentTime;
          $('#segments').append(newtemplate);
        }
        if (vm.segmentState === true && tag != 'input' && tag != 'textarea') {
          $API.pause();
          $(document).find('.segment.new').each(function() {
            $(this).removeClass('new')
                .addClass('ui-resizable ui-resizable-disabled ui-state-disabled')
                .click();
            $("#textCaptionEntry").show();
            var start = CssToNumber($(this).css('left'))/30;
            var end = $API.currentTime/1000;
            // console.log(start, end);
            AddCaption(start, end, $("#textCaptionEntry").val());
          })

        }

        vm.segmentState = !vm.segmentState;
      }

      $(document).on('click', '.segment', function() {
        $API.pause();
        $(this).removeClass('ui-resizable-disabled ui-state-disabled')
            .addClass('selected');
        $(this).siblings().addClass('ui-resizable-disabled ui-state-disabled')
            .removeClass('selected');
        var start = CssToNumber($(this).css('left'))/30 + 1;
        var ci = FindCaptionIndex(start);
        var theCaption = captionsArray[ci];
        if (theCaption) {
          // console.log(theCaption.caption);
          $('#textCaptionEntry').val(theCaption.caption)
        }
        $('#textCaptionEntry').show().focus();
      });

      $(document).on('click', '.delete-segment', function(e) {
        e.stopPropagation();
        $('#textCaptionEntry').val('').blur().hide();
        $(document).find('.segment.selected').each(function() {
          var start = CssToNumber($(this).css('left'))/30 + 1;
          var ci = FindCaptionIndex(start);
          captionsArray.splice(ci, 1);
        }).remove();

      });

      $('#textCaptionEntry').on('blur', function() {
        // var ci = FindCaptionIndex(-vm.currentTime/30);
        // console.log(ci);
        // if (ci !== -1) UpdateCaption(ci, $('#textCaptionEntry').val());
        $(document).find('.segment.selected').each(function() {
          var start = CssToNumber($(this).css('left'))/30 + 1;
          var ci = FindCaptionIndex(start);
          if (ci !== -1) UpdateCaption(ci, $('#textCaptionEntry').val())
        })
      });

      $(document).on('click', '.replay-segment', function(e) {
        e.stopPropagation();
        $(document).find('.segment.selected').each(function() {
          var start = CssToNumber($(this).css('left'))/30;
          var ci = FindCaptionIndex(start);
          var time = captionsArray[ci].end - captionsArray[ci].start;
          autoPauseAtTime = captionsArray[ci].end;
          $API.seekTime(start);$API.play();
        })
      });

      // setInterval(function () {
        // console.log(Math.floor(vid.currentTime*1000/30)); // will get you a lot more updates.

      // }, 30);

      vm.onUpdateTime = function(a, b) {
        $scope.duration = b;
        DisplayExistingCaption($API.currentTime/1000);
        if (FindCaptionIndex($API.currentTime/1000) === -1 && $API.currentState === 'play') {
          $('#textCaptionEntry').hide().val('');
          $(document).find('.segment').each(function() {
            $(this).removeClass('selected')
          })
        }
        if (autoPauseAtTime >= 0 && $API.currentTime/1000 >= autoPauseAtTime) {
          autoPauseAtTime = -1;
          $API.pause();
          console.log($API.currentTime);
          return;
        }
        var ratio = 100/3;
        var milisec = $API.currentTime;
        var tranform = -Math.floor(milisec/ratio);
        var right = Math.floor(milisec/ratio);
        $('#segments').css({
          transform: "translate3d(" + tranform + "px, 0px, 0px)"
        });
        $('#waveform').css({
          transform: "translate3d(" + tranform + "px, 0px, 0px)"
        });
        $('#timelines').css({
          transform: "translate3d(" + tranform + "px, 0px, 0px)"
        });
        // $('#timescales').style["background-position"] = Math.floor( - vid.currentTime*1000/30) + "px center";
        $('#timescales').css({
          backgroundPosition: tranform + "px center"
        });
        updatePlayProgress($API.currentTime, $API.totalTime);
        $('.play_range').width();
        vm.currentTime = tranform;
        // console.log(left);
        var time = right - left;
        $(document).find('.segment.new').width(time)
      };

      /*
       |--------------------------------------------------------------------------
       | Progress Bar
       |--------------------------------------------------------------------------
       */
      var progressHolder = $('.full_bar').get(0);
      var playProgressBar = $('.play_range').get(0);
      function setPlayProgress(clickX) {
        var newPercent = Math.max(0, Math.min(1, (clickX - findPosX(progressHolder)) / progressHolder.offsetWidth));
        $API.seekTime(newPercent * $API.totalTime/1000);
        playProgressBar.style.width = newPercent * (progressHolder.offsetWidth) + "px";
        $('.current_knob.tooltip-south').css({left: newPercent * (progressHolder.offsetWidth) + 'px'});
      }
      function updatePlayProgress(time, duration) {
        var fullWidth = $('.full_bar').width();
        var width = (time/duration)*fullWidth;
        $('.play_range').css({width: width+'px'});
        $('.current_knob.tooltip-south').css({left: width-2 + 'px'});
      }
      function findPosX(obj) {
        var curleft = obj.offsetLeft;
        while(obj = obj.offsetParent) {
          curleft += obj.offsetLeft;
        }
        return curleft;
      }
      $('.full_bar').mousedown(function(e) {
        setPlayProgress(e.clientX);
        var videoWasPlaying;
        if ($API.currentState !== 'play') {
          videoWasPlaying = false;
        } else {
          videoWasPlaying = true;
          $API.pause();
        }
        document.onmousemove = function(e) {
          $API.pause();
          setPlayProgress(e.pageX);
        };
        document.onmouseup = function() {
          document.onmousemove = null;
          document.onmouseup = null;
          if (videoWasPlaying) {
            $API.play();
          }
        }

      });

    }; //End $API ready


    vm.timeRule = [];

    $scope.$watch('duration', function(duration) {
      vm.timeRule = [];
      var time = 90;
      var ruleLeft = 3160;
      if (duration) {
        for (var i=1;i<Math.floor((duration - 90)/5);i++) {
          // $('#timeline-0').append("<li style='left: "+ruleLeft+"px;'>"+FormatTime(time)+"</li>");
          vm.timeRule.push({
            time: FormatTime(time),
            left: ruleLeft
          });
          time += 5;
          ruleLeft += 150;
        }
      }
      // console.log(timeRule);
    });
    
    // vm.a = timeRule;

    function FormatTime(seconds) {
      var hh = Math.floor(seconds / (60 * 60));
      var mm = Math.floor(seconds / 60) % 60;
      var ss = seconds % 60;

      return (hh == 0 ? "" : (hh < 10 ? "0" : "") + hh.toString() + ":") + (mm < 10 ? "0" : "") + mm.toString() + ":" + (ss < 10 ? "0" : "") + ss.toFixed(3);
    }
    function Trim(s) {
      return s.replace(/^\s+|\s+$/g, "");
    }
    function CssToNumber(str) {
      var newstr = str.replace(/px/i, '');
      return Number(newstr);
    }
    function XMLEncode(s) {
      return s.replace(/\&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');   //.replace(/'/g, '&apos;').replace(/"/g, '&quot;');
    }

    function XMLDecode(s) {
      return s.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&apos;/g, "'").replace(/&quot;/g, '"').replace(/&amp;/g, '&');
    }

    var minWidth = 10;
    var minHeight = 10;
    var FULLSCREEN_MARGINS = -10;
    var widthBefore;
    var leftBefore;
    var MARGINS = 4;
    var clicked = null;
    var onRightEdge, onBottomEdge, onLeftEdge, onTopEdge;
    var rightScreenEdge, bottomScreenEdge;
    var preSnapped;
    var b, x, y;
    var redraw = false;
    var selectedIndex = -1;

    var pane = $(document).find('.ui-resizeable.selected');
    var ghostpane = $(document).find('#ghostpane');

    function setBounds(element, x, y, w, h) {
      element.css('left', x, 'top', y, 'width', w, 'height', h);
    }
    function hintHide() {
      setBounds(ghostpane, b.left, b.top, b.width, b.height);
      ghostpane.css('opacity', '0')
    }
    $(document).on('mousedown', '.segment.selected', onMouseDown);
    $(document).on('mousemove', onMove);
    $(document).on('mouseup', onUp);

    function onMouseDown(e) {
      // console.log(e);
      widthBefore = $(document).find('.segment.selected').width();
      var str = $(document).find('.segment.selected').css('left');
      var newstr = str.replace(/px/i, '');
      leftBefore = Number(newstr);
      selectedIndex = FindCaptionIndex(leftBefore/30);
      // console.log(widthBefore);
      // console.log(leftBefore);
      onDown(e);
      e.preventDefault();
    }
    function onDown(e) {
      calc(e);
      var isResizing = onRightEdge || onLeftEdge;
      clicked = {
        x: x,
        y: y,
        cx: e.clientX,
        cy: e.clientY,
        w: b.width,
        h: b.height,
        isResizing: isResizing,
        isMoving: !isResizing && canMove(),
        onLeftEdge: onLeftEdge,
        onRightEdge: onRightEdge
      };
      // console.log(clicked);
    }
    function canMove() {
      return x > 0 && x < b.width && y > 0 && y < b.height && y < 30
    }
    function calc(e) {
      if ($(document).find('.segment.selected').get(0)) {
        b = $(document).find('.segment.selected').get(0).getBoundingClientRect();
        x = e.clientX - b.left;
        y = e.clientY - b.top;
        onLeftEdge = x < MARGINS;
        onRightEdge = x >= b.width - MARGINS;
        rightScreenEdge = $('#segments').get(0).innerWidth - MARGINS;
      } else {
        return false
      }
    }
    var e;
    function onMove(ee) {
      calc(ee);
      e = ee;
      redraw = true;
    }
    function animate() {
      // console.log('call animate');
      // console.log(clicked);
      requestAnimationFrame(animate);
      if (!redraw) return;
      redraw = false;
      if (clicked && clicked.isResizing) {

        if (clicked.onRightEdge) {
          // $(document).find('.segment.selected').get(0).style.width = Math.max(x, minWidth) + 'px';
          $(document).find('.segment.selected').each(function() {
            $(this).width(Math.max(x, minWidth));
            var left = CssToNumber($(this).css('left'));
            if (selectedIndex !== -1) captionsArray[selectedIndex].end = (left+Math.max(x, minWidth))/30;
          })
        }

        if (clicked.onLeftEdge) {
          // console.log(clicked);
          // console.log(e.clientX);
          var currentWidth = Math.max(clicked.cx - e.clientX  + clicked.w, minWidth);
          // console.log($(document).find('.segment.selected').width());
          // console.log(currentWidth);
          var left = leftBefore + 6 - (currentWidth - widthBefore);
          if (currentWidth > minWidth) {
            $(document).find('.segment.selected').get(0).style.width = currentWidth + 'px';
            $(document).find('.segment.selected').each(function() {
              if (selectedIndex !== -1) captionsArray[selectedIndex].start = left/30;
              $(this).css('left', left + 'px');

            })
          }
        }

        hintHide();

        return;
      }
      // style cursor
      if ($(document).find('.segment.selected').get(0)) {
        if (onRightEdge || onLeftEdge) {
          $(document).find('.segment.selected').get(0).style.cursor = 'ew-resize';
        }  else {
          $(document).find('.segment.selected').get(0).style.cursor = 'default';
        }
      }
    }
    animate();
    function onUp(e) {
      calc(e);
      if (clicked && clicked.isMoving) {
        var snapped = {
          width: b.width,
          height: b.height
        };

        // if (b.top < b.top < FULLSCREEN_MARGINS || b.left < FULLSCREEN_MARGINS || b.right > $('.middleframe').get(0).innerWidth - FULLSCREEN_MARGINS || b.bottom > $('.middleframe').get(0).innerHeight - FULLSCREEN_MARGINS) {
        //   // hintFull();
        //   setBounds(pane, 0, 0, $('.middleframe').get(0).innerWidth, $('.middleframe').get(0).innerHeight);
        //   preSnapped = snapped;
        // } else if (b.top < MARGINS) {
        //   // hintTop();
        //   setBounds(pane, 0, 0, $('.middleframe').get(0).innerWidth, $('.middleframe').get(0).innerHeight / 2);
        //   preSnapped = snapped;
        // } else if (b.left < MARGINS) {
        //   // hintLeft();
        //   setBounds(pane, 0, 0, $('.middleframe').get(0).innerWidth / 2, $('.middleframe').get(0).innerHeight);
        //   preSnapped = snapped;
        // } else if (b.right > rightScreenEdge) {
        //   // hintRight();
        //   setBounds(pane, $('.middleframe').get(0).innerWidth / 2, 0, $('.middleframe').get(0).innerWidth / 2, $('.middleframe').get(0).innerHeight);
        //   preSnapped = snapped;
        // } else if (b.bottom > bottomScreenEdge) {
        //   // hintBottom();
        //   setBounds(pane, 0, $('.middleframe').get(0).innerHeight / 2, $('.middleframe').get(0).innerWidth, $('.middleframe').get(0).innerWidth / 2);
        //   preSnapped = snapped;
        // } else {
        //   preSnapped = null;
        // }

        hintHide();

      }
      clicked = null;
    }

    /*
     |--------------------------------------------------------------------------
     | Create logic
     |--------------------------------------------------------------------------
     */
    var captionsArray = [];
    var autoPauseAtTime = -1;

    //  index into captionsArray of the caption being displayed. -1 if none.
    var captionBeingDisplayed = -1;

    function DisplayExistingCaption(seconds) {
      var ci = FindCaptionIndex(seconds);
      // console.log(seconds, ci);
      if (ci != captionBeingDisplayed) {
        captionBeingDisplayed = ci;
        if (ci != -1) {
          var theCaption = captionsArray[ci];
          // $("#captionTitle").text("Caption for segment from " + FormatTime(theCaption.start) + " to " + FormatTime(theCaption.end) + ":");
          $("#textCaptionEntry").val(theCaption.caption).show();
        } else {
          // $("#captionTitle").html("&nbsp;");
          $("#textCaptionEntry").val("").hide();
        }
      }
    }

    function FindCaptionIndex(seconds) {
      // var below = -1;
      var above = captionsArray.length;
      // var i = Math.floor((below + above) / 2);
      var i = 0;

      while (i < above) {

        if (captionsArray[i].start <= seconds && seconds < captionsArray[i].end) {
          // console.log(i);
          return i;
        }

        // if (seconds < captionsArray[i].start) {
        //   above = i;
        // } else {
        //   below = i;
        // }

        // i = Math.floor((below + above) / 2);
        i++
      }

      return -1;
    }

    function UpdateCaption(ci, captionText) {
      captionsArray[ci].caption = captionText;
      // $("#ci" + ci.toString() + " span:last-child").html(XMLEncode(captionText).replace(/\r\n|\r|\n/g, "<br/>"));

      // RefreshCaptionFileDisplay();
    }
    function AddCaption(captionStart, captionEnd, captionText) {
      captionsArray.push({start: captionStart, end: captionEnd, caption: Trim(captionText)});
      // AddCaptionListRow(captionsArray.length - 1);
      // RefreshCaptionFileDisplay();
    }

  }
})();