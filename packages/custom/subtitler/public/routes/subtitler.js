(function () {
  'use strict';

  angular
    .module('mean.subtitler')
    .config(subtitler);

  subtitler.$inject = ['$stateProvider'];

  function subtitler($stateProvider) {
    $stateProvider
        .state('subtitler example page', {
          url: '/subtitler/example',
          templateUrl: 'subtitler/views/index.html'
        })
        .state('segment', {
          url: '/subtitler/:videoId',
          templateUrl: 'subtitler/views/segment.html',
          resolve: {
            loggedin: function (MeanUser) {
              return true;
            }
          }
        });
  }

})();
