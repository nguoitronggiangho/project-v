(function () {
  'use strict';

  angular
    .module('mean.subtitler')
    .factory('Segment', Segment);

  Segment.$inject = ['$resource', '$http'];

  function Segment($resource, $http) {
    return {
      name: 'subtitler',
      resource: $resource('/api/segment/:videoId', null, {
        update: {method: 'PUT'}
      })
    };
  }
})();
