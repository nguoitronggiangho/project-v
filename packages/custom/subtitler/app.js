'use strict';

/*
 * Defining the Package
 */
var Module = require('../../../core').Module;

var Subtitler = new Module('subtitler');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Subtitler.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Subtitler.routes(app, auth, database);

  Subtitler.aggregateAsset('css', 'subtitler.css');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Subtitler.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Subtitler.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Subtitler.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Subtitler;
});
