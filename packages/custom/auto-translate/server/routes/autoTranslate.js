(function () {
  'use strict';

  /* jshint -W098 */
  // The Package is past automatically as first parameter
  module.exports = function (AutoTranslate, app, auth, database) {
    var autoTranslateController = require('../controllers/autoTranslateController')(autoTranslateController);

    app.get('/api/autoTranslate/example/anyone', function (req, res, next) {
      res.send('Anyone can access this');
    });

    app.get('/api/autoTranslate/example/auth', auth.requiresLogin, function (req, res, next) {
      res.send('Only authenticated users can access this');
    });

    app.get('/api/autoTranslate/example/admin', auth.requiresAdmin, function (req, res, next) {
      res.send('Only users with Admin role can access this');
    });

    app.route('/api/autoTranslate/initDatabase')
        .get(autoTranslateController.autoTranslateControllerInitDatabase);

    app.route('/api/auto-translate/config')
        .get(autoTranslateController.getCurrentBingServerConfig)
        .post(autoTranslateController.addNewBingServerConfig);

    app.route('/api/auto-translate/config')
        .put(autoTranslateController.activeBingServerConfig);

    
    app.get('/api/autoTranslate/example/render', function (req, res, next) {
      AutoTranslate.render('index', {
        package: 'auto-translate'
      }, function (err, html) {
        //Rendering a view from the Package server/views
        res.send(html);
      });
    });
  };
})();
