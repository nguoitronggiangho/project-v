/**
 * Created by DinhBN on 4/17/2016.
 */
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var translateServerConfigSchema = new Schema({
    serverTranslateType: {
        type: String
    },
    active: {
        type: Boolean,
        default: false
    },
    apiId: {
        type: String
    },
    apiSecret: {
        type: String
    },
    mailAddress: {
        type: String,
        unique: true
    },
    passWord: {
        type: String
    }
});

mongoose.model('translateServerConfig',translateServerConfigSchema);