/**
 * Created by DinhBN on 4/18/2016.
 */
var mongoose = require('mongoose');
var config = require('../../../../../core').loadConfig();
var User = mongoose.model('User');
var TranslateServerConfig = mongoose.model('translateServerConfig');
var ObjectId = mongoose.Types.ObjectId;


module.exports = function (autoTranslateController) {
    return {
        addNewBingServerConfig: function(req, res){
            TranslateServerConfig.find({}, function(error, arrayConfig){
                if(error){
                    console.log("addNewBingServerConfig error: " + error);
                    res.status(500).json({message: false});
                }else{
                    arrayConfig.forEach(function(config){
                        config.active = false;
                        config.save();
                    });

                    var newConfig = new TranslateServerConfig(req.body);
                    newConfig.active = true;
                    newConfig.save(function(error){
                        if(error){
                            console.log("addNewBingServerConfig error: " + error);
                            res.status(500).json({message: false});
                        }else{
                            res.status(200).json({message: true});
                        }
                    });


                }
            });
        },

        autoTranslateControllerInitDatabase: function (req, res) {
            {
                var tempServerConfig = new TranslateServerConfig;

                tempServerConfig.serverTranslateType = "Bing";
                tempServerConfig.active = false;
                tempServerConfig.apiId = "subtitle_ai_translator_bing";
                tempServerConfig.apiSecret = "qeQr2cu3N6W58b+WdJosx4H6ttCnK2eViinjmhRxcko=";
                tempServerConfig.mailAddress = "dinhbn@hotmail.com";
                tempServerConfig.passWord = "";

                tempServerConfig.save();
                tempServerConfig = null;
            }

            {
                var tempServerConfig = new TranslateServerConfig;

                tempServerConfig.serverTranslateType = "Bing";
                tempServerConfig.active = false;
                tempServerConfig.apiId = "subtitle_ai_translator_bing_2";
                tempServerConfig.apiSecret = "NnPagaiuZv0aQbh0jJNXoudanSCqgULEW3gLwCnZ6Dk=";
                tempServerConfig.mailAddress = "buingocdinh@hotmail.com";
                tempServerConfig.passWord = "";

                tempServerConfig.save();
                tempServerConfig = null;
            }

            {
                var tempServerConfig = new TranslateServerConfig;

                tempServerConfig.serverTranslateType = "Bing";
                tempServerConfig.active = false;
                tempServerConfig.apiId = "subtitle_ai_translator_bing_3";
                tempServerConfig.apiSecret = "mCgszRz9LDjnsmNeH3VecN6HnYRL9Kvykt4JGYxOMAg=";
                tempServerConfig.mailAddress = "dinhbn86@hotmail.com";
                tempServerConfig.passWord = "";

                tempServerConfig.save();
                tempServerConfig = null;
            }

            {
                var tempServerConfig = new TranslateServerConfig;

                tempServerConfig.serverTranslateType = "Bing";
                tempServerConfig.active = true;
                tempServerConfig.apiId = "subtitle_ai_translator_bing_4";
                tempServerConfig.apiSecret = "GdVhfXWIBX/0iTD2GYfeLFYzVk4s+Q1cEWkcDZhVXKg=";
                tempServerConfig.mailAddress = "dinhbn1986@hotmail.com";
                tempServerConfig.passWord = "";

                tempServerConfig.save();
                tempServerConfig = null;
            }

            {
                // init for user translate
                var user = new User({
                    name: "BingAiTranslate",
                    email: "admin@bing.com",
                    password: "admin1234",
                    username: "BingAiTranslate",
                    confirmPassword: "admin1234"
                });

                user.save();
            }

            {
                // init for user translate
                var objectPass = new ObjectId();
                var password = String(objectPass);

                var user = new User({
                    name: "VoteSubAI",
                    email: "votesubai@votesub.com",
                    password: password,
                    username: "VoteSubAI",
                    confirmPassword: password
                });

                user.save();
            }

            res.status(200).json({message: "ok"});
        },

        getCurrentBingServerConfig: function (req, res) {
            TranslateServerConfig.find({serverTranslateType: "Bing", active: "true"}, function (error, bingServerConfig) {
                if(error){
                    res.status(500).json({message: "couldn't find current bing server config"});
                }else{
                    res.json(bingServerConfig);
                }
            })
        },

        activeBingServerConfig: function (req, res) {
            var mailAddress = req.body.mailAddress;
            TranslateServerConfig.find({serverTranslateType: "Bing"}, function (error, arrBingServerConfig) {
                if(error){
                    res.status(500).json({message: "couldn't active bing server config"});
                }else{
                    var activeConfig = false;

                    arrBingServerConfig.forEach(function (bingServerConfig) {
                        if(bingServerConfig.mailAddress === mailAddress){
                            activeConfig = true;
                        }
                    });

                    if(activeConfig){
                        arrBingServerConfig.forEach(function (bingServerConfig) {
                            var needSave =false;

                            if(bingServerConfig.mailAddress === mailAddress){
                                if(bingServerConfig.active != true){
                                    bingServerConfig.active = true;
                                    needSave = true;
                                }
                            }else{
                                if(bingServerConfig.active != false){
                                    bingServerConfig.active = false;
                                    needSave = true;
                                }
                            }

                            if(needSave){
                                bingServerConfig.save();
                            }
                        });
                        res.json({message: "ok"});
                    }else{
                        res.status(500).json({message: "Couldn't find email to active"});
                    }
                }
            })
        }
    }
};