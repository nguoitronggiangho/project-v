"use strict";

require('../../../captions/server/models/caption');
require('../../../ai_helper/server/models/ai_helper');

var mongoose = require('mongoose');
var Q = require('q');
var config = require('../../../../../core').loadConfig();

var AI_TRANSLATE_JOB = mongoose.model('ai_translate_job');
var Caption = mongoose.model('Caption');
var Language = mongoose.model('ai_language_support');
var MsTranslator = require('mstranslator');
var bingAiId = undefined;

function getBingTranslateId(){
    var deferred = Q.defer();
    var User = mongoose.model('User');

    User.findOne({email: "votesubai@votesub.com", name: "VoteSubAI"}, function (error, user) {
        if (error) {
            console.log("getBingTranslateId error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(user._id);
        }
    });

    return deferred.promise;
}

function getUser(email, name){
    var deferred = Q.defer();
    var User = mongoose.model('User');

    User.findOne({email: email, name: name}, function (error, user) {
        if (error) {
            console.log("getUser error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(user);
        }
    });

    return deferred.promise;
}

function getBingServerActive(){
    var deferred = Q.defer();
    var TranslateServerConfig = mongoose.model('translateServerConfig');

    TranslateServerConfig.findOne({serverTranslateType: "Bing", active: true}, function (error, serverConfig) {
        if (error) {
            console.log("getBingServerActive error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(serverConfig);
        }
    });

    return deferred.promise;
}

function getLangInfo(langId) {
    var deferred = Q.defer();

    Language.findOne({_id: langId}, function (error, lang) {
        if (error) {
            console.log("getLangInfo error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(lang);
        }
    });

    return deferred.promise;
}

function getJobInfo(jobId) {
    var deferred = Q.defer();

    AI_TRANSLATE_JOB.findOne({_id: jobId}, function (error, job) {
        if (error) {
            console.log("getJobInfo error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(job);
        }
    });

    return deferred.promise;
}

function getPendingJobs() {
    var deferred = Q.defer();

    AI_TRANSLATE_JOB.find({}).limit(10).exec(function (error, jobs) {
        if (error) {
            console.log("getPendingJobs error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(jobs);
        }
    });

    return deferred.promise;
}

function getAllLangSupport() {
    var deferred = Q.defer();

    Language.find({}, function (error, listLang) {
        if (error) {
            console.log("getAllLangSupport error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(listLang);
        }
    });

    return deferred.promise;
}

function getCaption(captionId) {
    var deferred = Q.defer();

    Caption.findOne({_id : captionId}, function (error, caption) {
        if (error) {
            console.log("getCaption error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(caption);
        }
    });

    return deferred.promise;
}

function getCaptions(option) {
    var deferred = Q.defer();

    Caption.find({}, option, function (error, arrayCaption) {
        if (error) {
            console.log("getCaptions error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(arrayCaption);
        }
    });

    return deferred.promise;
}

function getBlockIndexTranslate(arrayText, limitText, limitArraySize) {
    if(arrayText.length == 0){
        throw new Error("Couldn't getBlockIndexTranslate with empty array text");
    }

    var i = 0;
    var totalTextLength = 0;

    for(; i < arrayText.length; i++){
        if(i >= limitArraySize){
            break;
        }

        var currentTextLength = arrayText[i].length;

        if((totalTextLength + currentTextLength) >= limitText)
        {
            break;
        }

        totalTextLength += currentTextLength;
    }

    if(i === 0){
        throw  new Error("Couldn't getBlockIndexTranslate satisfy condition");
    }

    return i;
}

function saferTranslateArrayText(result, translator, arrayText, langFrom, langTo, limitText, limitArraySize) {
    if(arrayText.length == 0){
        return Q(result);
    }

    var indexToSplit = getBlockIndexTranslate(arrayText, limitText, limitArraySize);
    var firstPart = arrayText.slice(0, indexToSplit);
    arrayText = arrayText.slice(indexToSplit);

    return translateByBing(translator, firstPart, langFrom, langTo)
        .then(function (translatedText) {
            result = result.concat(translatedText);

            return saferTranslateArrayText(result, translator, arrayText, langFrom, langTo, limitText, limitArraySize);
        });
}

function translateByBing(translator, arrayText, langFrom, langTo) {
    if(arrayText.length == 0){
        return Q([]);
    }else{
        var deferred = Q.defer();
        var params = { texts: arrayText , from: langFrom, to: langTo, maxTranslations:arrayText.length };

        translator.translateArray(params, function (error, data) {
            if (error) {
                console.log("Failed in translate text, error: " + error);
                deferred.reject(new Error(error));
            } else {
                var retTrans = [];
                data.forEach(function (objectResTranslated) {
                    retTrans.push(objectResTranslated.TranslatedText);
                });
                deferred.resolve(retTrans);
            }
        });

        return deferred.promise;
    }
}

function saveNewCaption(caption, translatedText, idNewLang) {
    var deferred = Q.defer();

    if(translatedText.length == caption.sentence.length){
        var newCaption = new Caption();

        newCaption.language = idNewLang;
        newCaption.rate = 0;
        newCaption.user = bingAiId;
        newCaption.videoId = caption.videoId;
        newCaption.sentence = [];

        for (var i = 0; i < translatedText.length; i++){
            var sentenceElement = caption.sentence[i];
            var tempSentence = {};

            tempSentence.authorId = bingAiId;
            tempSentence.timeStart = sentenceElement.timeStart;
            tempSentence.timeEnd = sentenceElement.timeEnd;
            tempSentence.text = translatedText[i];
            tempSentence.order = sentenceElement.order;
            tempSentence.rate = 0;

            newCaption.sentence.push(tempSentence);
        }

        newCaption.save(function (error) {
            if (error) {
                console.log("failed save caption:" + error);
                deferred.reject(new Error(error));
            } else {                
                deferred.resolve();
            }
        });
    }else{
        deferred.reject(new Error("Have problem with create new caption, don't match translated text"));
    }

    return deferred.promise;
}

function TranslateCaption(listSaveCaptionPromise, translator, caption, captionTextArray, langFrom, mapLang, listLangTarget, limitText, limitArraySize) {
    if (listLangTarget.length === 0) {
        return Q();
    }
    
    var langTo = listLangTarget.shift();
    var langToId = mapLang[langTo];

    return saferTranslateArrayText([], translator, captionTextArray, langFrom, langTo, limitText, limitArraySize)
        .then(function (translatedText) {
            console.log("translatedText(lang: " + langTo + "):");
            console.log(translatedText);
            listSaveCaptionPromise.push(saveNewCaption(caption, translatedText, langToId));

            return TranslateCaption(listSaveCaptionPromise, translator, caption, captionTextArray, langFrom, mapLang, listLangTarget, limitText, limitArraySize);
        });
}

function doJobTranslateByBing(job) {
    var mapLanguage = {};
    var langTarget = job.language_symbol_target;
    var langCaption;
    var listAllLanguageSymbol = [];
    var listLangTranslateTo = [];
    var captionInDatabase = undefined;
    var bingServerConfig = undefined;

    return getBingServerActive()
        .then(function (serverConfig) {
            console.log("serverConfig: " + serverConfig);
            bingServerConfig = serverConfig;
        })
        .then(function(){
            if(bingAiId){
                console.log("using cache bing id: " + bingAiId);
                return null;
            }else{
                return getBingTranslateId();
            }
        })
        .then(function(userId){
            if(userId){
                bingAiId = userId;
                console.log("using new cache bing id: " + bingAiId);
            }
        })
        .then(function () {
            return getAllLangSupport();
        })
        .then(function (arrayLangInfo) {
            arrayLangInfo.forEach(function (langInfo) {
                mapLanguage[langInfo.language_symbol] = langInfo._id;
                mapLanguage[langInfo._id] = langInfo.language_symbol;
                listAllLanguageSymbol.push(langInfo.language_symbol);
            });

            //remove all
            var i = listAllLanguageSymbol.indexOf("ALL");
            if(i != -1) {
                listAllLanguageSymbol.splice(i, 1);
            }

            return job.caption_id;
        })
        .then(function (captionId) {            
            return getCaption(captionId);
        })
        .then(function (captionObj) {
            captionInDatabase = captionObj;
            var captionLangId = captionObj.language;
            langCaption = mapLanguage[captionLangId];

            // construct list language target
            if(langCaption && langCaption !== langTarget){
                if(langTarget != "ALL"){
                    // support translate to array of languages separate by comma
                    var arrayTranslateTmp = langTarget.split(",");

                    var i = arrayTranslateTmp.indexOf(langCaption);
                    if(i != -1) {
                        arrayTranslateTmp.splice(i, 1);
                    }
                    listLangTranslateTo = arrayTranslateTmp;
                }else{
                    listLangTranslateTo = listAllLanguageSymbol.slice();

                    var j = listLangTranslateTo.indexOf(langCaption);
                    if(j != -1) {
                        listLangTranslateTo.splice(j, 1);
                    }
                }
            }

            // Translate from langCaption to each one of listLangTranslateTo
            // Each request a translate to prevent race condition with multi request translate
            console.log("mapLanguage: " + mapLanguage);
            console.log("listLangTranslateTo size: " + listLangTranslateTo.length + ", content: " + listLangTranslateTo);

            var translator = new MsTranslator({client_id: bingServerConfig.apiId, client_secret: bingServerConfig.apiSecret}, true);
            var listSaveCaptionPromise = [];
            var captionTextArray = [];

            captionObj.sentence.forEach(function (sentenceElement) {
                captionTextArray.push(sentenceElement.text);
            } );

            console.log("captionTextArray text(" + langCaption + "):");
            console.log(captionTextArray);

            return TranslateCaption(listSaveCaptionPromise, translator, captionObj, captionTextArray, langCaption, mapLanguage, listLangTranslateTo, config.bingLimitText, config.bingLimitArray)
                .then(function () {
                    return Q.all(listSaveCaptionPromise);
                }).then(function () {
                    return captionInDatabase;
                })
        });
}

function doMigrateSentenceUser(emailFrom, nameFrom, emailTo, nameTo){
    var userFrom = undefined;
    var userTo = undefined;

    return Q([])
        .then(function(){
            return getUser(emailFrom, nameFrom)
                .then(function(user){
                    userFrom = user;
                    console.log("doMigrateSentenceUser userFrom id: " + userFrom._id);
                    return user;
                })
        })
        .then(function(){
            return getUser(emailTo, nameTo)
                .then(function(user){
                    userTo = user;
                    console.log("doMigrateSentenceUser userTo id: " + userTo._id);
                    return user;
                })
        })
        .then(function(){
            return getCaptions("user sentence");
        })
        .then(function(arrayCaption){
            console.log("arrayCaption.length: " + arrayCaption.length);
            var arrayPromise = [];

            arrayCaption.forEach(function (caption) {
                var hasChange = false;
                if(caption.user.equals(userFrom._id)){
                    caption.user = userTo._id;
                    hasChange = true;
                }

                caption.sentence.forEach(function(sentenceInCaption){
                    if(sentenceInCaption.authorId.equals(userFrom._id)){
                        sentenceInCaption.authorId = userTo._id;
                        hasChange = true;
                    }
                });

                if(hasChange){
                    arrayPromise.push(Q.ninvoke(caption, "save"));
                }
            });

            console.log("Total array change: " + arrayPromise.length);

            return Q.all(arrayPromise)
                .then(function(){
                    console.log("doMigrateSentenceUser userFrom ==> userTo");
                });
        });
}

exports.doRemoveSentenceTagError = function(filter){
    return Q([])
        .then(function(){
            return getCaptions("sentence");
        })
        .then(function(arrayCaption){
            console.log("arrayCaption.length: " + arrayCaption.length);
            var arrayPromise = [];

            arrayCaption.forEach(function (caption) {

                caption.sentence.forEach(function(sentenceInCaption){
                    sentenceInCaption.text = filter(sentenceInCaption.text);
                });

                arrayPromise.push(Q.ninvoke(caption, "save"));
            });

            console.log("Total array change: " + arrayPromise.length);

            return Q.all(arrayPromise)
                .then(function(){
                    console.log("doRemoveSentenceTagError finish");
                });
        })
};

exports.fixAiName = function(){
    return doMigrateSentenceUser("admin@bing.com", "BingAiTranslate", "votesubai@votesub.com", "VoteSubAI");
    //return doMigrateSentenceUser("votesubai@votesub.com", "VoteSubAI", "admin@bing.com", "BingAiTranslate");
};

exports.translateByBing = function (requestInfo) {
    return doJobTranslateByBing(requestInfo);      
};

exports.detectLanguageCaption = function (text) {
    var deferred = Q.defer();
    var translator = new MsTranslator({client_id: config.bingClientId, client_secret: config.bingClientSecret}, true);

    translator.detect(text, function (error, lang) {
        if (error) {
            console.log("detectLanguageCaption error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(lang);
        }
    });

    return deferred.promise;
};

exports.detectArrayLanguageCaption = function (arrayText) {
    var deferred = Q.defer();
    var translator = new MsTranslator({client_id: config.bingClientId, client_secret: config.bingClientSecret}, true);

    translator.detectArray(arrayText, function (error, arrayLang) {
        if (error) {
            console.log("detectArrayLanguageCaption error: " + error);
            deferred.reject(new Error(error));
        } else {
            deferred.resolve(arrayLang);
        }
    });

    return deferred.promise;
};
