'use strict';

/*
 * Defining the Package
 */
var Module = require('../../../core').Module;

var AutoTranslate = new Module('auto-translate');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
AutoTranslate.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  AutoTranslate.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  //AutoTranslate.aggregateAsset('css', 'autoTranslate.css');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    AutoTranslate.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    AutoTranslate.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    AutoTranslate.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return AutoTranslate;
});
