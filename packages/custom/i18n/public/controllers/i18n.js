'use strict';

/* jshint -W098 */
angular.module('mean.i18n').controller('I18nController', ['$scope', 'Global', 'I18n', '$translate',
  function($scope, Global, I18n, $translate) {
    
    var self = this;
    
    $scope.global = Global;

    $scope.changeLanguage = function (key) {
      $translate.use(key);
    };
    
    //for footer
    // this.userState = '';
    //
    // this.state = [{
    //   short: 'en',
    //   full: 'E'
    // }];
  }
]);
