'use strict';

/* jshint -W098 */
angular.module('mean.i18n').config(function ($translateProvider) {
  $translateProvider
  .translations('en', {
    HEADLINE: 'Hello there, This is my awesome app!',
    INTRO_TEXT: 'And it has i18n support!',
    NAV_UPLOAD: 'Upload',
    HOME_TITLE_TOP_REQUEST: 'Top request translate',
    HOME_TITLE_TRENDING: 'Trending now',
    HOME_TITLE_TOP_VOLUNTEERS: 'TOP VOLUNTEERS THIS WEEK',
    HOME_SEE_ALL: 'See all',
    HOME_LEARN_MORE: 'Learn more',
    HOME_VOLUNTEER_MESSAGE: 'All subtitles contributed by volunteers!',
    UPLOAD_STEP_1: 'Step 1',
    UPLOAD_STEP_2: 'Step 2',
    UPLOAD_STEP_3: 'Step 3',
    UPLOAD_STEP_1_TITLE: 'Upload your video or provide YOUTUBE video url',
    UPLOAD_STEP_1_DROPFILE_OR_CLICK: 'Drop mp4 video here or click to upload',
    UPLOAD_STEP_1_VIDEO_TITLE: 'Title',
    UPLOAD_STEP_1_UPLOAD_SUCCESS: 'Upload Successful',
    UPLOAD_STEP_1_SELECT_CATEGORY: 'Select category',
    UPLOAD_STEP_1_SELECT_TAGS: 'Tags',
    UPLOAD_STEP_1_SELECT_TAGS_HINT: 'Tags separated by comma or press enter',
    UPLOAD_STEP_1_SELECT_TAGS_2: 'Enter a tag',
    UPLOAD_STEP_1_UPLOAD_BUTTON: 'UPLOAD',
    RATINGS: 'Ratings',
    FORM_REQUIRED: 'This is required.',
    BUTTON_LANG_EN: 'English',
    BUTTON_LANG_VI: 'Vietnamese'
  })
  .translations('vi', {
    HEADLINE: 'Hey, das ist meine großartige App!',
    INTRO_TEXT: 'Und sie untersützt mehrere Sprachen!',
    NAV_UPLOAD: 'Tải lên',
    HOME_TITLE_TOP_REQUEST: 'Video được yêu cầu nhiều nhất',
    HOME_TITLE_TRENDING: 'Video HOT',
    HOME_TITLE_TOP_VOLUNTEERS: 'TOP tình nguyện viên',
    HOME_SEE_ALL: 'Xem thêm',
    HOME_LEARN_MORE: 'Tìm hiểu thêm',
    HOME_VOLUNTEER_MESSAGE: 'Tất cả  phụ đề đều được đóng góp bởi tình nguyện viên!',
    UPLOAD_STEP_1: 'Bước 1',
    UPLOAD_STEP_2: 'Bước 2',
    UPLOAD_STEP_3: 'Bước 3',
    UPLOAD_STEP_1_TITLE: 'Tải lên video của bạn hoặc link video từ YOUTUBE',
    UPLOAD_STEP_1_DROPFILE_OR_CLICK: 'Kéo thả video mp4 vào đây! hoặc click để chọn file',
    UPLOAD_STEP_1_VIDEO_TITLE: 'Tiêu đề',
    UPLOAD_STEP_1_UPLOAD_SUCCESS: 'Tải lên thành công',
    UPLOAD_STEP_1_SELECT_CATEGORY: 'Chọn chủ đề của video',
    UPLOAD_STEP_1_SELECT_TAGS: 'Nhập tên thẻ',
    UPLOAD_STEP_1_SELECT_TAGS_HINT: 'Thẻ cách nhau bởi dấu phẩy hoặc nhấn phím Enter',
    UPLOAD_STEP_1_SELECT_TAGS_2: 'Nhập tên thẻ',
    UPLOAD_STEP_1_UPLOAD_BUTTON: 'Tải lên',
    RATINGS: 'Đánh giá',
    FORM_REQUIRED: 'Vui lòng không để trống',
    BUTTON_LANG_EN: 'Tiếng Anh',
    BUTTON_LANG_VI: 'Tiếng Việt'
  });
  $translateProvider.preferredLanguage('vi')
  .useMissingTranslationHandlerLog();
}).run(function ($rootScope) {
    $rootScope.lang = 'Vietnamese'
});
