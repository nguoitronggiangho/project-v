(function () {
  'use strict';

  /* jshint -W098 */
  // The Package is past automatically as first parameter
  module.exports = function (Channel, app, auth, database) {

    var channel = require('../controllers/channel')(Channel);
    var channelRequest = require('../controllers/channel-request')(Channel);

    app.route('/api/channel')
        .get(channel.all)
        .post(channel.create);

    app.route('/api/channel/:channelId')
        .get(channel.show)
        .put(auth.isMongoId, auth.requiresLogin, channel.update);
    app.route('/api/channel/upload').post(auth.isMongoId, auth.requiresLogin, channel.upload);
    app.route('/api/channel/findbyuser/get').get(auth.isMongoId, auth.requiresLogin, channel.findByUser);
    app.route('/api/channel/subscribe/:channelId').get(auth.requiresLogin, channel.subscribe);
    
    app.route('/api/channelrequest')
        .get(channelRequest.all)
        .post(auth.isMongoId, auth.requiresLogin, channelRequest.create);
    app.route('/api/channelrequest/:requestId')
        .put(auth.isMongoId, auth.requiresLogin, channelRequest.update);

    app.param('channelId', channel.channel);

  };
})();
