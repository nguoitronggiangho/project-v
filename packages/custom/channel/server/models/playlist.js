'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PlaylistSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  channel: {
    type: Schema.ObjectId,
    ref: 'Channel'
  },
  name: {
      type: String,
      trim: true,
      require: true
  },
  description: String,
  avatar: String,
  view: Number,
  video: [{
    video: {
      type: Schema.ObjectId,
      ref: 'Video'
    },
    order: {
      type: Number
    }
  }],
  status: {
    type: Boolean,
    default: true
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Vỉtual
 */
PlaylistSchema.virtual('videoCount').get(function() {
  if (this.video) {
    return this.video.length
  }
});

mongoose.model('Playlist', PlaylistSchema);