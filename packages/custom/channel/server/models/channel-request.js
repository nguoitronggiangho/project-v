'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChannelRequestSchema = new Schema({
  type: {
    type: String,
    required: true,
    enum: ['moderator', 'video', 'create']
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  video: {
    url: String,
    title: String
  },
  create: {
    name: String,
    description: String
  },
  channel: {
    type: Schema.ObjectId,
    ref: 'Channel'
  },
  name: {
    type: String,
    trim: true
  },
  description: {
    type: String,
    trim: true
  },
  status: {
    type: String,
    enum: ['pending', 'approved'],
    default: 'pending'
  }

});

mongoose.model('ChannelRequest', ChannelRequestSchema);