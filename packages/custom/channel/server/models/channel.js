'use strict';

var debug = require('debug');
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

function arrayLimit5(moderator) {
  if (moderator) return moderator.length <= 5;
}
function arrayLimit2(admin) {
  if (admin) return admin.length <= 2;
}

var ChannelSchema = new Schema({
  name: {
    type: String,
    require: true,
    trim: true,
    unique: true,
    index: true,
    // match: [/^[a-zA-Z0-9_'\-[\] ]+$/, 'Please enter valid channel name']
  },
  description: {
    type: String,
    trim: true
  },
  avatar: {
    type: String,
    default: '/channel/assets/img/default-channel-avatar.png'
  },
  cover: {
    type: String,
    default: '/channel/assets/img/default-channel-cover.png'
  },
  trailer: {
    type: Schema.ObjectId,
    ref: 'Video'
  },
  feature: {
    type: Boolean,
    default: false
  },
  admin: {
    type: [{
      type: Schema.ObjectId,
      ref: 'User'
    }],
    validate: [arrayLimit2, '{PATH} exceeds the limit of 2']
  },
  moderator: {
    type: [{
      type: Schema.ObjectId,
      ref: 'User'
    }],
    validate: [arrayLimit5, '{PATH} exceeds the limit of 5']
  },
  video: [{
    type: Schema.ObjectId,
    ref: 'Video'
  }],
  playlist: [{
    type: Schema.ObjectId,
    ref: 'Playlist'
  }],
  subscriber: [{
    type: Schema.ObjectId,
    ref: 'User'
  }]
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  toObject: { 
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Indexes
 */
ChannelSchema.index({name: 'text'});

/**
 * Static
 */
ChannelSchema.statics = {
  load: function(id, cb) {
    this.findOne({
      _id: id
    }).populate('admin', 'name')
        .exec(cb)
  },
  getSubscribeState: function(id, user, cb) {
    this.findOne({
      _id: id
    }, function(err, result) {
      var state = result.subscriber.some(function(u) {
        return u.equals(user._id);
      });
      
      cb(err, state);
    })
  }
};
/**
 * Virtual
 */
// ChannelSchema.virtual('subcribeState').get(function(req) {
//   return this.subscriber.some(function (user) {
//     return user.equals(req.user._id);
//   });
// });
ChannelSchema.virtual('subscribeCount').get(function() {
  if (this.subscriber) {
    return this.subscriber.length;
  }
});
ChannelSchema.virtual('videoCount').get(function() {
  if (this.video) {
    return this.video.length;
  }
});

mongoose.model('Channel', ChannelSchema);