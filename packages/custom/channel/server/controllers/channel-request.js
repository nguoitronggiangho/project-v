'use strict';

var debug = require('debug')('app:channelRequest:' + process.pid);
var mongoose = require('mongoose');
var ChannelRequest = mongoose.model('ChannelRequest');
var _ = require('lodash');

module.exports = function() {
  return {
    create: function(req, res) {
      var request = new ChannelRequest(req.body);
      request.user = req.user;
      request.save(function(err, request) {
        if (err) {
          debug('create channel request error: ', err);
          return res.status(500).json({
            error: 'cannot create channel request'
          })
        }

        res.json(request)
      })
    },
    all: function(req, res) {
      console.log(req.query);
      ChannelRequest.find({ channel: req.query.channelId, status: 'pending'})
          .populate('user')
          .exec(function(err, request) {
            if (err) {
              debug(`Cannot get list request for channel: #{req.query.channelId} error: #{err}`);
              return res.status(500).json({
                error: 'cannot list channel request'
              })
            }

            var type = _.uniq(_.map(request, 'type'));
            var data = _.chain(request).groupBy('type').value();
            res.json(data)
          });
    },
    update: function(req, res) {
      ChannelRequest.findOne({_id: req.params.requestId}, function(err, request) {
        request.status = 'approved';
        console.log(request);
        request.save(function(err) {
          if (err) {
            debug('Cannot save channel request after update err: ', err);
            return res.status(500).json({
              error: 'Cannot save channel request after update'
            })
          }

          res.json({
            success: true
          })
        })
      })
    }
  }

};