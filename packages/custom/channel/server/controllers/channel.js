'use strict';

require('../../../videos/server/models/keyword');
var debug = require('debug')('app:channel:' + process.pid);
var mongoose = require('mongoose');
var Channel = mongoose.model('Channel');
var ChannelRequest = mongoose.model('ChannelRequest');
var Keyword = mongoose.model('Keyword');
var _ = require('lodash');
var multiparty = require('multiparty');
// var multer  = require('multer');
var uuid = require('node-uuid');
var fs = require('fs');
var mkdirp = require('mkdirp');
var Q = require('q');

module.exports = function() {
  return {

    channel: function(req, res, next, id) {
      debug('Get info for channel id = '.green.inverse, id);
      Channel.load(id, function(err, channel) {
        if (err) return next(err);
        if (!channel) return next(new Error('Failed to load channel'));
        req.channel = channel;
        next()
      })
    },

    findByUser: function(req, res) {
      Channel.find({$or: [{admin: req.user}, {moderator: req.user}]})
          .select('name avatar')
          .lean()
          .exec(function(err, channels) {
            console.log(channels);
            if (err) {
              debug('Error when get user onwer channels '.red, err);
              return res.status(500).json({
                error: 'Cannot get list channel for User'
              })
            }

            res.json(channels)
          })
    },

    show: function(req, res) {
      Channel.findOne({_id: req.params.channelId})
          .populate({
            path: 'playlist',
            options: { limit: 20 }
          })
          .populate('admin')
          .populate('moderator')
          // .populate({
          //   path: 'video',
          //   select: 'title created_at views thumbnail',
          //   options: { limit: 20 }
          // })
          .exec(function(err, channel) {
            if (err) {
              return res.status(500).json({
                error: 'cannot get channel info'
              })
            }
            // console.log(channel.toJSON());
            // var channel = req.channel.toJSON();
            channel = channel.toJSON();

            function findUserChannelRequest() {
              var deffered = Q.defer();
              ChannelRequest.find({user: req.user}, function(err, result) {
                if (err) {
                  debug('Error when find User request channel ', err);
                  defered.reject(err)
                }
                deffered.resolve(result);
              });

              return deffered.promise
            }
            function chechMemberState() {
              var defered = Q.defer();
              var adminState = channel.admin.some(function(admin) {
                return admin._id.equals(req.user._id);
              });
              var modState = channel.moderator.some(function(mod) {
                return mod._id.equals(req.user._id)
              });
              var subscribeState = channel.subscriber.some(function (user) {
                return user.equals(req.user._id);
              });
              // if (adminState === true || modState === true) {
              //   return true;
              // } else {
              //   return false;
              // }
              var data = {
                isAdmin: adminState,
                isMod: modState,
                isSubscribe: subscribeState
              };
              return data;

            }

            if (req.user) {
              findUserChannelRequest()
                  .then(function(result) {
                    debug('Step1: chech req.user channel request'.red.inverse);
                    debug(result);
                    return result.length > 0;
                  })
                  .then(function(result) {
                    debug('Step2: Check member state'.red.inverse);
                    // debug(chechMemberState());
                    var data = chechMemberState();
                    data.isRequested = result;
                    debug(data);
                    // debugger;
                    return data
                  })
                  .then(function(result) {
                    debug('Step3: push to channel Object \n'.red.inverse, result);
                    delete channel.subscriber;
                    channel.userState = result;
                    debug(channel);
                    res.json(channel);
                  })
                  .catch(function(error) {
                    debug('Error get member state '.red.inverse, error)
                  });
            } else {
              res.json(channel)
            }


          });

    },

    create: function(req, res) {
      var channel = new Channel();
      // channel.admin = req.user;
      channel.name = req.body.name;

      channel.save(function(err, channel) {
        if (err) {
          debug('Error when create channel '.red.inverse, err );
          return res.status(500).json({
            error: 'Cannot create channel'
          })
        }

        debug('Created channel ' + channel);
        res.json(channel);

        var searchKey = channel.name.toLowerCase();

          Keyword.findOrCreate({keyWord: searchKey}, function(err, click, created) {
            if (req.user) {
              click.update({ $addToSet: { searchPeople: req.user } },
                function(err, result) {
                  if (err) console.log(err);
                })
            }
          });

      })
    },
    /**
     * Update channel data
     * @param req
     * @param res
     */
    update: function(req, res) {
      console.log(req.body);
      // console.log(req.channel);
      var channel = req.channel;

      channel = _.extend(channel, req.body);

      if (req.body.addModerator) {
        channel.moderator.addToSet(req.body.addModerator);
      }

      // console.log(channel);
      // res.json(channel);
      channel.save(function(err) {
        if (err) {
          debug('error when update channel info ' + err);
          return res.status(500).json({
            error: 'cannot update channel infomation'
          })
        }

        debug(`Update success for channel ${req.channel.name}`.green.inverse);
       res.json(channel);
      })
    },
    /**
     * Handle Subscribe an unsubscribe action
     * @param req
     * @param res
     */
    subscribe: function(req, res) {
      debug(`Start findding channel: ${req.params.channelId} ...`.green);
      Channel.findById(req.params.channelId, function(err, channel) {
        if (err) {
          debug('Error when get channel info '.red.inverse, err);
          return res.status(500).json({
            error: 'Error when get channel info'
          })
        }

        debug(`Get channel info success: ${channel.name}`.green);
        debug(channel.subscriber.indexOf(req.user._id));
        if (channel.subscriber.indexOf(req.user._id) === -1) {
          channel.subscriber.addToSet(req.user);
          debug(`User ${req.user.name} Subcribed to channel ${req.channel.name}`.green.inverse);
        } else {
          channel.subscriber.pull(req.user);
          debug(`User ${req.user.name} UnSubcribe to channel ${req.channel.name}`.yellow.inverse);
        }

        channel.save(function(err) {
          if (err) {
            debug('Error when update user subscribe to channel state '.red.inverse, err);
            return res.status(500).json({
              error: 'Error when update user subscribe to channel state'
            })
          }

          debug(`Update channel subscribe success`.green.inverse);
          res.json({
            message: 'success'
          })
        })
        
      })
    },

    upload: function(req, res) {
      var form = new multiparty.Form();
      form.parse(req, function(err, fields, files) {
        // console.log(fields);
        // console.log(files);
        
        var file = files.file[0];
        var channelId = fields.channelId[0];
        var contentType = file.headers['content-type'];
        var extension = file.path.substring(file.path.lastIndexOf('.'));
        var filename = uuid.v4() + extension;
        var destPath;
        var showPath;
        var updateField;

        if (fields.type[0] === 'cover') {
          destPath = 'packages/custom/channel/public/assets/uploads/'+channelId+'/banner-' + filename;
          showPath = '/channel/assets/uploads/'+channelId+'/banner-' + filename;
          updateField = 'cover';
        } else if (fields.type[0] === 'avatar') {
          destPath = 'packages/custom/channel/public/assets/uploads/'+channelId+'/avatar-' + filename;
          showPath = '/channel/assets/uploads/'+channelId+'/avatar-' + filename;
          updateField = 'avatar';
        } else {
          res.status(400).send({
            message: 'invalid request type'
          })
        }

        var path = 'packages/custom/channel/public/assets/uploads/'+channelId;
        function createFolder() {
          var deferred = Q.defer();
          mkdirp(path, function(err) {
            if (err) {
              debug('Cannot create directory for channel art '.red, err);
              deferred.reject(err);
            }
            deferred.resolve(path);
          });
          return deferred.promise;
        }

        function copyFile() {
          var deferred = Q.defer();
          fs.rename(file.path, destPath, function(err) {
            if (err) {
              debug('Error when rename (copy) file '.red, err);
              deferred.reject(err);
            }
            deferred.resolve(showPath)
          });
          return deferred.promise
        }

        function deleteTemp() {
          var deferred = Q.defer();
          fs.unlink(file.path, function(err) {
            if (err) {
              debug('Unable to delete temp file'.red, err);
              deferred.reject(err);
            }
            deferred.resolve()
          });
          return deferred.promise
        }

        createFolder()
            .then(function(result) {
              debug('Step1: Create folder: '.red, result)
            })
            .then(function() {
              debug('Step2: Copy file to folder: '.red, showPath);
              return copyFile()
            })
            // .then(function(result) {
            //   debug('Step3: delete temp file');
            //   return deleteTemp()
            // })
            .then(function() {
              debug('Step4: Save change'.red);
              Channel.findOne({_id: channelId}, function(err, channel) {
                if (err) {
                  debug('cannot find channel for update cover-avatar info '.red, err);
                  return res.status(500).json({
                    error: 'Cannot find channel to upadate'
                  })
                }

                channel[updateField] = showPath;
                console.log(channel);
                channel.save(function(err) {
                  if (err) {
                    debug('Cannot save channel info while update cover-avatar '.red, err);
                    res.status(500).json({
                      error: 'cannot save channel info'
                    })
                  }

                  res.json({
                    cover: showPath
                  })
                })
              })
            })
            .catch(function(err) {
              debug('Error when upload'.red);
              debug(err)
            });

        // mkdirp(path, function(err) {
        //   if (err) {
        //     debug('Cannot create directory for channel art '.red, err);
        //     return res.status(500).json({
        //       error: 'Cannot create dir'
        //     })
        //   }
        //   fs.rename(file.path, destPath, function(err) {
        //     if (err) {
        //       debug('Error when rename (copy) file '.red, err);
        //       return res.status(400).send({
        //         message: 'Cannot save cover image'
        //       })
        //     }
        // });
        //
        //
        //
        //
        //
        //   Channel.findOne({_id: channelId}, function(err, channel) {
        //     if (err) {
        //       debug('cannot find channel for update cover-avatar info '.red, err);
        //       return res.status(500).json({
        //         error: 'Cannot find channel to upadate'
        //       })
        //     }
        //
        //     channel[updateField] = showPath;
        //     console.log(channel);
        //     channel.save(function(err) {
        //       if (err) {
        //         debug('Cannot save channel info while update cover-avatar '.red, err);
        //         res.status(500).json({
        //           error: 'cannot save channel info'
        //         })
        //       }
        //
        //       res.json({
        //         cover: showPath
        //       })
        //     })
        //   })
        // })

      })
    },
    /**
     * Get all channel
     */
    all: function(req, res) {
      var query = {};

      if (req.query.query) {
        query = JSON.parse(req.query.query);
      }
      var limit = req.query.limit || 50;
      var order = req.query.order || '-created';
      var cate = req.query.cate;
      var skip = req.query.skip || 0;

      if (cate) {
        query.categories = cate
      }

      console.log(query);
      Channel.find(query)
          .sort('-'+order)
          .skip(skip)
          .limit(limit)
          .populate('admin', 'username name')
          .populate('moderator', 'name')
          .lean()
          .exec(function (err, channels) {
            if (err) {
              debug('Error when get list channel '.red, err);
              return res.status(500).json({
                error: 'Cannot list the channels'
              })
            }

            res.json(channels)
          })
    }

  }
};