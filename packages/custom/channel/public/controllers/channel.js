(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('ChannelController', ChannelController);

  ChannelController.$inject = ['$scope', '$stateParams', 'Channel', '$mdDialog', 'Videos', 'MeanUser'];

  /* @ngInject */
  function ChannelController($scope, $stateParams, Channel, $mdDialog, Videos, MeanUser) {
    var vm = this;
    var channelId = $stateParams.channelId;
    vm.title = 'ChannelController';
    vm.home = {};
    vm.query= {
      page: 1,
      order: 'view',
      limit: 20,
      cate: null,
      query: {
        channel: channelId
      }
    };

    activate();

    ////////////////

    function activate() {

    }
    
    function isAdmin(data) {
      if (!MeanUser.user) return false;
      if (MeanUser.isAdmin) return true;
      return data.admin.some(function(val) {
        return val._id === MeanUser.user._id;
      })
    }
    function isMember(data) {
      if (!data.userState) return false;
      var state = data.userState;
      return (state.isAdmin || state.isMod || state.isRequested)
    }

    function success(data) {
      // console.log(data);
      if (data.userState && data.userState.isSubscribe === true) {
        var subButton = document.querySelector('.subscribe-button');
        var subbedClass = 'subbed';
        subButton.classList.add(subbedClass);
        subButton.querySelector('.subscribe-text').innerHTML = 'Subscribed';
      }
      
      vm.isAdmin = isAdmin(data);
      vm.isMember = isMember(data);

      return vm.channel = data;
    }

    if (channelId) {
      Channel.channelResource.get({channelId: channelId}, success)
    } else {
      
    }

    vm.getChannelHome = function() {
      Videos.query(vm.query, function(response) {
        // console.log(response);
        return vm.home.videos = response
      });
    };

    vm.getChannelVideos = function() {
      var query = {page: 1, order: 'created_at', limit: 50, cate: null, query: {channel: channelId}};
      Videos.query(query, function(response) {
        // console.log(response);
        return vm.videos = response;
      })
    };

    vm.applyModerator = function() {
      Channel.channelRequest.save({
        type: 'moderator',
        channel: channelId
      }).$promise.then(function(response) {
        console.log(response)
      }).catch(function(err) {
        console.log(err)
      })
    };

    vm.approveUser = function(requestId, userId) {
      console.log(requestId);
      Channel.channelRequest.update({requestId: requestId}, {status: 'approved'}).$promise
          .then(function(response) {
            console.log(response)
          })
          .catch(function(error) {
            console.log(error)
          });
      Channel.channelResource.update({channelId: channelId}, {addModerator: userId}).$promise
          .then(function(response) {
            console.log(response)
          })
    };
    
    vm.getChannelRequest = function() {
      Channel.getChannelRequest(channelId)
          .then(function(res) {
            return vm.request = res;
          })
    };
    
    vm.uploadAvatar = function(event) {
      $mdDialog.show({
        clickOutsideToClose: true,
        controller: 'uploadAvatarController',
        controllerAs: 'vm',
        focusOnOpen: false,
        targetEvent: event,
        locals: {channel: vm.channel},
        templateUrl: '/channel/views/upload-channel-avatar.tpl.html'
      })
    };
    
    vm.uploadCover = function(event) {
      $mdDialog.show({
        clickOutsideToClose: true,
        controller: 'uploadCoverController',
        controllerAs: 'vm',
        focusOnOpen: false,
        targetEvent: event,
        locals: {channel: vm.channel},
        templateUrl: '/channel/views/upload-channel-cover.tpl.html'
      })
    };

    this.handleSubscribe = function() {
      // console.log(vm.channel);
      // Channel.channelResource.update({channelId: channelId}, vm.channel).$promise
      //     .then(function(response) {
      //       console.log(response)
      //     });
      // vm.channel.subscribeState = !vm.channel.subscribeState;
      // console.log(vm.channel.subcribeState);
      Channel.handleSubscribe(channelId)
          .then(function(response) {
            console.log(response);
          })
    };

    var subButton = document.querySelector('.subscribe-button');
    var subbedClass = 'subbed';

    subButton.addEventListener('click', function(e) {
      toggleSubbed();
      e.preventDefault();
    });

      function toggleSubbed() {
        var text;
        var count = subButton.dataset.count;
        console.log(count);

        if (subButton.classList.contains(subbedClass)) {
          subButton.classList.remove(subbedClass);
          text = 'Subscribe';
          count--;
        } else {
          subButton.classList.add(subbedClass);
          text = 'Subscribed';
          count++;
        }

        subButton.querySelector('.subscribe-text').innerHTML = text;
        subButton.dataset.count = count;
      }

    if ('alert' == '') {
      window.setInterval(toggleSubbed, 1000);
    }
  }

})();

