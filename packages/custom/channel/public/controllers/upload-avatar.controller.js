(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('uploadAvatarController', uploadAvatarController);

  uploadAvatarController.$inject = ['channel', 'Upload', '$timeout', '$mdDialog'];

  /* @ngInject */
  function uploadAvatarController(channel, Upload, $timeout, $mdDialog) {
    var vm = this;
    vm.title = 'uploadAvatarController';

    activate();

    ////////////////

    function activate() {

    }

    this.uploadFile = function(file, errorFile) {
      vm.file = file;
      vm.errFile = errorFile && errorFile[0];

      if (file) {
        file.upload = Upload.upload({
          url: '/api/channel/upload',
          data: {type: 'avatar', channelId: channel._id},
          method: 'POST',
          file: file
        });

        file.upload.then(function(response) {
          $timeout(function() {
            console.log(response.data);
            file.result = response.data;
          })
        }, function (response) {
          if (response.status > 0)
            vm.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 *
              evt.loaded / evt.total));
        });

      }
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };
    
    
  }

})();

