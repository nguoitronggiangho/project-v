(function () {
  'use strict';

  angular
    .module('mean.channel')
    .config(channel);

  channel.$inject = ['$stateProvider'];

  function channel($stateProvider) {
    $stateProvider.state('channel example page', {
      url: '/channel/example',
      templateUrl: 'channel/views/index.html'
    })
    .state('channel', {
      abstract: true,
      url: '/channel/:channelId',
      templateUrl: '/channel/views/channel.html'
    })
        .state('channel.home', {
          url: '/home',
          templateUrl: '/channel/views/home.html'
        })
        .state('channel.videos', {
          url: '/videos',
          templateUrl: '/channel/views/channel-list-video'
        })
        .state('channel.playlist', {
          url: '/playlist',
          templateUrl: '/channel/views/channel-playlist.tpl.html'
        })
        .state('channel.request', {
          url: '/requests',
          templateUrl: '/channel/views/channel-list-request.tpl.html'
        })
        .state('channel.manager', {
          url: '/manager',
          templateUrl: '/channel/views/channel-video-manager.tpl.html'
        });
  }

})();
