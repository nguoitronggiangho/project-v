(function() {
  'use strict';

  angular
      .module('mean.videos')
      .factory('Channel', Channel);

  Channel.$inject = ['$resource', '$http'];

  /* @ngInject */
  function Channel($resource, $http) {
    var service = {
      channelResource: $resource('/api/channel/:channelId', null,
          {
            update: {method: 'PUT'}
          }),
      channelRequest: $resource('/api/channelrequest/:requestId', null,
          {
            update: {method: 'PUT'}
          }),
      getChannelRequest: getChannelRequest,
      getChannelByUser: getChannelByUser,
      handleSubscribe: handleSubscribe
    };
    return service;

    ////////////////

    function getChannelRequest(channelId) {
      return $http.get('/api/channelrequest?channelId='+channelId)
          .then(handleSuccess)
          .catch(handleError)
    }
    
    function getChannelByUser() {
      return $http.get('/api/channel/findbyuser/get')
          .then(handleSuccess)
          .catch(handleError)
    }

    function handleSubscribe(channelId) {
      return $http.get('/api/channel/subscribe/' + channelId)
          .then(handleSuccess)
          .catch(handleError)
    }

    function handleSuccess(response) {
      // console.log(response.data);
      return response.data
    }
    function handleError(err) {
      console.log(err)
    }
  }

})();

