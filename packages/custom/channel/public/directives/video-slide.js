(function() {
  'use strict';

  angular
      .module('mean.videos')
      .directive('videoList', videoSlide);

  videoSlide.$inject = ['Videos'];

  /* @ngInject */
  function videoSlide(Videos) {
    var directive = {
      bindToController: true,
      controller: ControllerName,
      controllerAs: 'vm',
      link: link,
      restrict: 'A',
      templateUrl: '/channel/directives/video-slide.html',
      scope: {}
    };
    return directive;

    function link(scope, element, attrs) {
      
    }
  }

  ControllerName.$inject = ['Videos'];

  /* @ngInject */
  function ControllerName(Videos) {
    
  }

})();

