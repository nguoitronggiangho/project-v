'use strict';

angular.module('mean.captions').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('caption admin page', {
      url: '/admin/caption',
      templateUrl: 'captions/views/admin.html',
      resolve: {
        loggedin: function (MeanUser) {
          return MeanUser.checkLoggedin();
        }
      }
    })
    .state('video details', {
      url: '/admin/video/:videoId',
      templateUrl: '/captions/views/video-details.html',
      resolve: {
        loggedin: function (MeanUser) {
          return MeanUser.checkLoggedin();
        }
      }
    });
  }
]);