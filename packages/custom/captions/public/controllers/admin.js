'use strict';

/* jshint -W098 */
angular.module('mean.captions')
  .config(['$mdThemingProvider', function ($mdThemingProvider) {
      'use strict';

      $mdThemingProvider.theme('default')
        .primaryPalette('blue');
  }])
  .controller('CaptionsAdminController', ['$scope', 'Global', 'Captions', 'Videos', '$stateParams', '$mdEditDialog', '$q', '$timeout',
  function($scope, Global, Captions, Videos, $stateParams, $mdEditDialog, $q, $timeout) {
    $scope.global = Global;
    $scope.package = {
      name: 'captions'
    };

    $scope.selected = [];

    $scope.options = {
      autoSelect: true,
      boundaryLinks: true,
      largeEditDialog: true,
      pageSelector: true,
      rowSelection: true
    };

    $scope.query = {
      order: 'name',
      limit: 5,
      page: 1
    };

    $scope.desserts = {
      "count": 9,
      "data": [
        {
          "name": "Frozen yogurt",
          "type": "Ice cream",
          "calories": { "value": 159.0 },
          "fat": { "value": 6.0 },
          "carbs": { "value": 24.0 },
          "protein": { "value": 4.0 },
          "sodium": { "value": 87.0 },
          "calcium": { "value": 14.0 },
          "iron": { "value": 1.0 }
        }, {
          "name": "Ice cream sandwich",
          "type": "Ice cream",
          "calories": { "value": 237.0 },
          "fat": { "value": 9.0 },
          "carbs": { "value": 37.0 },
          "protein": { "value": 4.3 },
          "sodium": { "value": 129.0 },
          "calcium": { "value": 8.0 },
          "iron": { "value": 1.0 }
        }, {
          "name": "Eclair",
          "type": "Pastry",
          "calories": { "value":  262.0 },
          "fat": { "value": 16.0 },
          "carbs": { "value": 24.0 },
          "protein": { "value":  6.0 },
          "sodium": { "value": 337.0 },
          "calcium": { "value":  6.0 },
          "iron": { "value": 7.0 }
        }, {
          "name": "Cupcake",
          "type": "Pastry",
          "calories": { "value":  305.0 },
          "fat": { "value": 3.7 },
          "carbs": { "value": 67.0 },
          "protein": { "value": 4.3 },
          "sodium": { "value": 413.0 },
          "calcium": { "value": 3.0 },
          "iron": { "value": 8.0 }
        }, {
          "name": "Jelly bean",
          "type": "Candy",
          "calories": { "value":  375.0 },
          "fat": { "value": 0.0 },
          "carbs": { "value": 94.0 },
          "protein": { "value": 0.0 },
          "sodium": { "value": 50.0 },
          "calcium": { "value": 0.0 },
          "iron": { "value": 0.0 }
        }, {
          "name": "Lollipop",
          "type": "Candy",
          "calories": { "value": 392.0 },
          "fat": { "value": 0.2 },
          "carbs": { "value": 98.0 },
          "protein": { "value": 0.0 },
          "sodium": { "value": 38.0 },
          "calcium": { "value": 0.0 },
          "iron": { "value": 2.0 }
        }, {
          "name": "Honeycomb",
          "type": "Other",
          "calories": { "value": 408.0 },
          "fat": { "value": 3.2 },
          "carbs": { "value": 87.0 },
          "protein": { "value": 6.5 },
          "sodium": { "value": 562.0 },
          "calcium": { "value": 0.0 },
          "iron": { "value": 45.0 }
        }, {
          "name": "Donut",
          "type": "Pastry",
          "calories": { "value": 452.0 },
          "fat": { "value": 25.0 },
          "carbs": { "value": 51.0 },
          "protein": { "value": 4.9 },
          "sodium": { "value": 326.0 },
          "calcium": { "value": 2.0 },
          "iron": { "value": 22.0 }
        }, {
          "name": "KitKat",
          "type": "Candy",
          "calories": { "value": 518.0 },
          "fat": { "value": 26.0 },
          "carbs": { "value": 65.0 },
          "protein": { "value": 7.0 },
          "sodium": { "value": 54.0 },
          "calcium": { "value": 12.0 },
          "iron": { "value": 6.0 }
        }
      ]
    };

    $scope.editComment = function (event, dessert) {
      event.stopPropagation(); // in case autoselect is enabled

      var editDialog = {
        modelValue: dessert.comment,
        placeholder: 'Add a comment',
        save: function (input) {
          if(input.$modelValue === 'Donald Trump') {
            return $q.reject();
          }
          if(input.$modelValue === 'Bernie Sanders') {
            return dessert.comment = 'FEEL THE BERN!'
          }
          dessert.comment = input.$modelValue;
        },
        targetEvent: event,
        title: 'Add a comment',
        validators: {
          'md-maxlength': 30
        }
      };

      var promise;

      if($scope.options.largeEditDialog) {
        promise = $mdEditDialog.large(editDialog);
      } else {
        promise = $mdEditDialog.small(editDialog);
      }

      promise.then(function (ctrl) {
        var input = ctrl.getInput();

        input.$viewChangeListeners.push(function () {
          input.$setValidity('test', input.$modelValue !== 'test');
        });
      });
    };

    $scope.getTypes = function () {
      return ['Candy', 'Ice cream', 'Other', 'Pastry'];
    };

    $scope.loadStuff = function () {
      $scope.promise = $timeout(function () {
        // loading
      }, 2000);
    };

    $scope.logItem = function (item) {
      console.log(item.name, 'was selected');
    };

    $scope.logOrder = function (order) {
      console.log('order: ', order);
    };

    $scope.logPagination = function (page, limit) {
      console.log('page: ', page);
      console.log('limit: ', limit);
    };

    $scope.findVideo = function () {
      Videos.query(function (videos) {
        $scope.videos = videos;
        $scope.totalVideos = videos.length;
        console.log($scope.total)
      })
    };

    $scope.findOne = function () {
      Videos.get({
        videoId: $stateParams.videoId
      }, function (video) {
        $scope.video = video;
        $scope.source = [{
          src: video.url,
          type: 'video/mp4'
        }];

        Captions.findVideoCaption({
          videoId: $stateParams.videoId
        }, function (captionCollection) {
          $scope.captionCollection = captionCollection;
          Captions.findLanguage(function (language) {
            $scope.languages = language;
            console.log(captionCollection);

            //manipulate data for video detail table
            var info = [];
            for (var i=0 ;i < language.length; i++) {
              if (captionCollection.hasOwnProperty(language[i].language_symbol)){
                if (video.lockedLanguage.length > 0 ) {
                  console.log(video);
                  for (var j=0; j < video.lockedLanguage.length; j++ ) {
                    console.log(video.lockedLanguage[j]);
                    console.log(language[i]._id);
                    if (video.lockedLanguage[j]._id === language[i]._id) {
                      info.push({language: language[i], qty: captionCollection[language[i].language_symbol].length, status: true});
                      console.log('inloop-true')
                    } else {
                      info.push({language: language[i], qty: captionCollection[language[i].language_symbol].length, status: false});
                      console.log('inloop-false')
                    }
                  }
                } else {
                  console.log('out loop');
                  info.push({language: language[i], qty: captionCollection[language[i].language_symbol].length, status: false})
                }
              }
            }
            $scope.info = info;

            //save language lock status when change
            $scope.status = function (status, lang) {
              if (status === true) {
                console.log('push');
                video.lockedLanguage.push(lang);
                if (!video.updated) {
                  video.updated = []
                }
                video.updated.push(new Date().getTime());
                video.$update()
              } else {
                console.log('unset');
                var index = video.lockedLanguage.indexOf(lang._id);
                if (index > -1) {
                  video.lockedLanguage.splice(index, 1);
                }
                if (!video.updated) {
                  video.updated = []
                }
                video.updated.push(new Date().getTime());
                video.$update()
              }
              console.log(video);
            };
          })
        })
      })
    };



    $scope.getAllLanguage = function () {
      Captions.findLanguage(function (language) {
        $scope.languages = language;
      })
    };

    //$scope.captionCount = function (langs) {
    //  //var langs = $scope.languages;
    //  for (var i=0 ;i < langs.length; i++) {
    //    console.log(langs[i].language_symbol);
    //    console.log($scope.captionCollection)
    //  }
    //};

    $scope.findCaption = function () {
      Captions.query(function (captions) {
        $scope.captions = captions;
        $scope.totalCaptions = captions.length;
        console.log(captions);
      })
    }

  }
]);
