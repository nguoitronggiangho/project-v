'use strict';

/* jshint -W098 */
angular.module('mean.captions').controller('CaptionsController', ['$scope', 'Global', 'Captions',
  function($scope, Global, Captions) {
    $scope.global = Global;
    $scope.package = {
      name: 'captions'
    };
  }
]);
