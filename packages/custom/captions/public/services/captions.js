'use strict';

angular.module('mean.captions').factory('Captions', ['$resource',
  function($resource) {
    return $resource('api/captions/:captionId', null, {
      update: {
        method: 'PUT'
      },
      video: {
        method: 'GET',
        params: {videoId:'@_id'},
        url: 'api/captions/:videoId'
      },
      findVideoCaption: {
        method: 'GET',
        params: {videoId:'@_id'},
        url: 'api/captions/collection/:videoId',
        isArray: false
      },
      createLang: {
        method: 'GET',
        url:'api/captions/create/lang'
      },
      getCaptionByLang: {
        method: 'GET',
        params: {videoId:'@_id'},
        url:'api/captions/get/:videoId',
        isArray: true
      },
      findLanguage: {
        method: 'GET',
        url:'api/captions/get/lang',
        isArray: true
      },
      checkout: {
        method: 'POST',
        url: 'api/captions/checkout'
      },
      translate: {
        method: 'POST',
        url: 'api/captions/translator/translate'
      },
      findUserCaptions: {
        method: 'GET',
        params: {userId: '@_id'},
        url: 'api/captions/find-user-captions/:userId',
        isArray: true
      },
      vote: {
        method: 'POST',
        url: 'api/captions/vote/post'
      },
      saveSentence: {
        method: 'POST',
        url: 'api/captions/sentence/save'
      },
      test: {
        method: 'POST',
        url: 'api/captions/translator/done'
      }
    });
  }
]);
