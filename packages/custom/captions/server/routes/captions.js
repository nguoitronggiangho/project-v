'use strict';

// Caption authorization helpers
var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin && !req.video.user._id.equals(req.user._id)) {
    return res.status(401).send('User is not authorized');
  }
  next();
};

module.exports = function(Captions, app, auth) {

  var captions = require('../controllers/captions')(Captions);

  app.route('/api/captions')
      .get(captions.all)
      .post(auth.requiresLogin,  captions.create);
  app.route('/api/captions/:captionId')
      .get(auth.isMongoId, captions.show)
      .put(auth.isMongoId, auth.requiresLogin, captions.update);
  //    .delete(auth.isMongoId, auth.requiresLogin, hasAuthorization, captions.destroy);
  //
  //// Finish with setting up the captionId param
  //app.param('captionId', captions.caption);
  app.route('/api/captions/:videoId')
      .get(auth.isMongoId, auth.requiresLogin, captions.video);

  app.route('/api/captions/get/lang')
      .get(auth.isMongoId, auth.requiresLogin, captions.getLang);

  app.route('/api/captions/get/:videoId')
      .get(auth.isMongoId, captions.getAllCaptionBelongToVideoId);

  app.route('/api/captions/collection/:videoId')
      .get(auth.isMongoId, captions.findVideoCaption);

  app.route('/api/captions/checkout')
      .post(auth.requiresLogin, captions.checkout);

  app.route('/api/captions/translator/translate')
      .post(auth.isMongoId, captions.translator);

  app.route('/api/captions/translator/done')
      .post(auth.isMongoId, captions.translated);

  app.route('/api/captions/find-user-captions/:userId')
      .get(auth.isMongoId, captions.findUserCaptions);
  
  app.route('/api/captions/sentence/save')
      .post(auth.isMongoId, captions.saveSentence);

  app.route('/api/captions/vote/post')
      .post(auth.isMongoId, auth.requiresLogin, captions.vote);

  app.route('/api/captions/volunteer/get')
      .get(captions.getTopVolunteers);

  app.route('/api/fix/contribute')
      .get(auth.isMongoId, captions.fixDataVolunteers);

  app.route('/api/fix/ai-name')
      .get(auth.isMongoId, captions.fixAiName);

  app.route('/api/fix/sentence')
      .get(auth.isMongoId, captions.fixSentence);

  app.route('/api/fix/video')
      .get(captions.fixVideoCaptionAttribute);



  app.route('/api/caption/insert-sentence').get(function () {
    var mongoose = require('mongoose')
    var Caption = mongoose.model('Caption');
    Caption.findById('56e2725e2b9701a0131e06a3', function (err, cap) {
      if (err) {
        res.status(401)
      }
      cap.sentence.push({
        rate: Math.floor(Math.random() * 10),
        order: 4,
        text: makeid(),
        timeEnd: 17.454,
        timeStart: 15.354,
        authorId: '5619461c3b13f9e23ac57486'
      });
      cap.save(function (err) {
        if (!err) res.status(403);
      });
    });
  });
  app.route('/api/caption/get-new').get(function (req, res) {
    var mongoose = require('mongoose')
    var Caption = mongoose.model('Caption');
    var _ = require('lodash');

    Caption.findById('56e2725e2b9701a0131e06a3', function (err, cap) {
      if (err) {
        res.json({message:'get fail'})
      }

      var data1 = cap.sentence;
      var data2 = {};
      data1 = _.chain(data1).groupBy('order').forEach(function(value, key){
            value = _.chain(value).orderBy(['rate'], ['desc']).value();
            //console.log(key);
            data2[key] = value;
            //console.log(value);
          })
          .value();
      //console.log(data2);
      var bestVersionSubtitle = [];
      _.forEach(data2, function (value, key) {
        bestVersionSubtitle.push(value[0])
      });

      res.json({
        caption: bestVersionSubtitle,
        all: data2
      })

    });
  });
  function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 15; i++ )
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
};