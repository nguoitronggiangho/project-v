'use strict';

/**
 * Module dependencies
 */
require('../../../ai_helper/server/models/ai_helper');
require('../../../dashboard/server/models/tracking-event-subbing');
require('../../../videos/server/models/video');
var debug = require('debug')('app:captions:controller' + process.pid);
var mongoose = require('mongoose'),
    Contribute = mongoose.model('Contribute'),
    Job = mongoose.model('ai_translate_job'),
    Caption = mongoose.model('Caption'),
    User = mongoose.model('User'),
    Video = mongoose.model('Video'),
    config = require('../../../../../core').loadConfig(),
    SubbingEvent = mongoose.model('TrackingEventSubbing'),
    _ = require('lodash'),
    fs = require('fs'),
    Language = mongoose.model('ai_language_support'),
    Emitter = require('events'),
    emtr = new Emitter(),
    Q = require('q'),
    ObjectId = mongoose.Types.ObjectId;

var AutoTranslate = require('../../../auto-translate/server/controllers/autoTranslate');

function fixUserModifyText(sentenceText){
    var stringGood = sentenceText.replace(/<\s*\/\s*br\s*>/gi,"</br>");
    stringGood = stringGood.replace(/<\s*br\s*\/\s*>/gi,"<br/>");
    console.log("string after fixed: " + stringGood);
    return stringGood;
}

module.exports = function (Captions) {
    return {
        getLang: function (req, res) {
            Language.find({active: true}).sort('name')
                .lean()
                .exec(function (err, lang) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the Language'
                    })
                }

                res.json(lang)
            })
        },
        getAllCaptionBelongToVideoId: function (req, res) {
            Caption.find({videoId: req.params.videoId})
                .populate('sentence.authorId', 'name')
                .populate('language')
                .lean()
                .exec(function (err, data) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot find captions'
                    })
                }


                var output = [];
                _.forEach(data, function (value, key) {
                    var data2 = value;
                    var sortedSentence = {};
                    _.chain(data2.sentence).groupBy('order').forEach(function (value, key) {
                        value = _.chain(value).orderBy(['rate'], ['desc']).value();
                        sortedSentence[key] = value;
                    }).value();
                    var bestVersionSubtitle = [];
                    _.forEach(sortedSentence, function (value, key) {
                        bestVersionSubtitle.push(value[0])
                    });
                    //data2[sentence] = sortedSentence;
                    output.push({
                        _id: data2._id,
                        videoId: data2.videoId,
                        user: data2.user,
                        language: data2.language,
                        sentence: sortedSentence,
                        display: bestVersionSubtitle
                    });
                });

                res.json(output)
            })
        },

        getCaptionByLang: function (req, res) {
            Caption.find({videoId: req.params.videoId})
                .sort('-rate')
                .populate('language')
                .populate('user')
                .lean()
                .exec(function (err, lang) {
                    if (err) {
                        return res.status(500).json({
                            error: 'Cannot list the videos'
                        })
                    }
                    var data = lang;
                    var languageName = _.uniq(_.map(_.map(lang, 'language'), 'language_symbol'));

                    var a = _(languageName).forEach(function (n) {
                        _.forEach(lang, function (m) {
                            if (n == m.language.language_symbol) {
                                m.languageName = n
                            }
                        })
                    });

                    var result2 = _.chain(data)
                        .groupBy('languageName')
                        .value();

                    var result3 = [];
                    _(result2).forEach(function (n) {
                        var maxObj = _.max(n, function (m) {
                            return m.rate
                        });
                        result3.push(maxObj);
                    });

                    var result4 = [];
                    for (var i = 0; i < result3.length; i++) {
                        result4.push({
                            kind: 'caption',
                            srclang: result3[i].language.language_symbol,
                            label: result3[i].language.language_symbol,
                            default: null,
                            sentence: result3[i].sentence,
                            language: result3[i].language,
                            caption: result3[i]
                        });
                        result3[i].srclang = result3[i].language.language_symbol;
                    }

                    _(result3).forEach(function (n) {

                        data = _.reject(data, n)

                    });

                    _(languageName).forEach(function (n) {
                        _.forEach(data, function (m) {
                            if (n == m.language.language_symbol) {
                                m.languageName = n
                            }
                        })
                    });

                    data = _.chain(data)
                        .groupBy('languageName')
                        .value();

                    res.json({
                        captionSrc: result4,
                        subCaption: data
                    });
                })
        },
        /**
         * @return all captions of video
         * @param req
         * @param res
         */
        findVideoCaption: function (req, res) {
            Caption.find({videoId: req.params.videoId})
                .sort('-rate')
                .populate('language')
                .populate('user', 'username')
                .populate('sentence.authorId', 'name')
                .lean()
                .exec(function (err, lang) {
                    if (err) {
                        return res.status(500).json({
                            error: 'cannot find caption'
                        })
                    }

                    var language = _.uniq(_.map(lang, 'language'));

                    var output = [];
                    _.forEach(lang, function (value, key) {
                        var data2 = value;
                        var sortedSentence = {};
                        _.chain(data2.sentence).groupBy('order').forEach(function (value, key) {
                            value = _.chain(value).orderBy(['rate'], ['desc']).value();
                            sortedSentence[key] = value;
                        }).value();
                        // var bestVersionSubtitle = [];
                        // _.forEach(sortedSentence, function (value, key) {
                        //     bestVersionSubtitle.push(value[0])
                        // });
                        //data2[sentence] = sortedSentence;
                        output.push({
                            _id: data2._id,
                            videoId: data2.videoId,
                            user: data2.user,
                            language: data2.language,
                            sentence: sortedSentence,
                            // display: bestVersionSubtitle
                        });
                    });

                    var result2 = _.chain(output)
                        .groupBy('language.language_symbol')
                        .value();

                    res.json(result2);

                })
        },
        /**
         * Find caption by videoId
         */
        video: function (req, res) {
            Caption.find({videoId: req.params.videoId}, function (err, caption) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot find caption'
                    })
                }

                var fileName = 'caption-en.vtt';
                var wstream = fs.createWriteStream('packages/custom/captions/public/assets/video/' + fileName);
                var cap = caption[0].sentence;

                function FormatTime(seconds) {
                    var hh = Math.floor(seconds / (60 * 60));
                    var mm = Math.floor(seconds / 60) % 60;
                    var ss = seconds % 60;

                    return (hh == 0 ? '' : (hh < 10 ? '0' : '') + hh.toString() + ':') + (mm < 10 ? '0' : '') + mm.toString() + ':' + (ss < 10 ? '0' : '') + ss.toFixed(3);
                }

                wstream.write('WEBVTT\r\n\r\n');
                for (var i = 0; i < cap.length; i++) {
                    wstream.write(FormatTime(cap[i].timeStart) + ' --> ' + FormatTime(cap[i].timeEnd) + '\r\n');
                    wstream.write(cap[i].text + "\r\n\r\n");
                }
                wstream.end(function () {
                    //var url = config.hostname + '/captions/assets/video/' + fileName;
                    res.json({captionArr: cap});
                });
                //res.json(caption)
            })
        },
        /**
         * Create a caption
         */
        create: function (req, res) {
            var caption = new Caption(req.body);
            var sentence = req.body.sentence;

            caption.user = req.user;
            caption.video = req.body.videoId;
            caption.language = req.body.language;
            caption.rate = 0;

            for (var i = 0; i < sentence.length; i++) {
                //caption.sentence.push({authorId: req.user, order: i+1, timeStart: sentence[i].start, timeEnd: sentence[i].end, text: sentence[i].caption})
                caption.sentence[i].authorId = req.user;
                caption.sentence[i].timeStart = sentence[i].start;
                caption.sentence[i].timeEnd = sentence[i].end;
                caption.sentence[i].text = fixUserModifyText(sentence[i].caption);
                caption.sentence[i].order = i + 1;
                caption.sentence[i].rate = 0
            }

            caption.save(function (err) {
                if (err) {
                    debug('Error while save captions '.red, err);
                    return res.status(500).json({
                        error: 'Cannot save caption'
                    })
                }

                Video.findOneAndUpdate(
                    {_id: req.body.videoId},
                    { $set:
                        {hasCaption: true}
                    },
                    function(err) {
                        if (err) {
                          debug('Error while change video `hasCaption` status '.red.inverse, err);
                          return res.status(500).json({
                              error: 'Error while change video `hasCaption` status'
                          })
                        }
                        
                        debug('Change video `hasCaption` status success'.green.inverse)
                    }
                );

                debug('Create captions done'.green.inverse);
                res.json({
                    caption: caption,
                    message: 'success: Your caption were created'
                });
            })
        },

        /**
         * checkout new caption version
         */
        checkout: function (req, res) {
            // internal function helper
            function getCaptionPromise(captionId) {
                var deferred = Q.defer();

                Caption.findById(captionId, function (error, caption) {
                    if (error) {
                        console.log("getCaptionPromise error: " + error);
                        deferred.reject(new Error(error));
                    } else {
                        deferred.resolve(caption);
                    }
                });

                return deferred.promise;
            };

            console.log(req.body);
            //console.log("req.user:" + req.user._id);

            getCaptionPromise(req.body.captionId)
                .then(function (caption) {
                    var sentenceArr = caption.sentence;
                    var add = true;
                    var idSentenceContribute;
                    var sentenceText = fixUserModifyText(req.body.text);

                    //Check if req user already contribute this sentence -> update it, if not create new one
                    for (var i = 0; i < sentenceArr.length; i++) {                        
                        if (sentenceArr[i].order === req.body.order && sentenceArr[i].authorId.equals(req.user.id)) {
                            var sentenceToUpdate = sentenceArr[i];
                            console.log("Found sentence contributed by user");
                            sentenceToUpdate.text = sentenceText;
                            add = false;
                            idSentenceContribute = sentenceToUpdate._id;
                            break;
                        }
                    }

                    if (add) {
                        console.log("Create new sentence and push to caption.sentence");
                        caption.sentence.push({
                            rate: 0,
                            order: req.body.order,
                            text: sentenceText,
                            timeEnd: req.body.timeEnd,
                            timeStart: req.body.timeStart,
                            authorId: req.user.id
                        });

                        idSentenceContribute = caption.sentence[caption.sentence.length - 1]._id;
                    }

                    console.log("step 1: prepare save caption");
                    return Q.ninvoke(caption, "save")
                        .then(function () {
                            console.log("Finish save caption");
                            var objectInfoContribute = {};

                            objectInfoContribute.add = add;
                            objectInfoContribute.videoId = caption.videoId;
                            objectInfoContribute.idSentenceContribute = idSentenceContribute;
                            objectInfoContribute.sentenceText = sentenceText;
                            objectInfoContribute.language = caption.language

                            return objectInfoContribute;
                        });
                })
                .then(function (infoContribute) {
                    console.log("step 2: async update contribute, run parallel with step 3, infoContribute: " + JSON.stringify(infoContribute));

                    // in the case add, id has just created and unique, so, using push is good enough
                    if(infoContribute.add){
                        console.log("step 2: user add new sentence. So, update contribute data");
                        Contribute.update(
                            {user: req.user._id},
                            {$push: {sentence:  infoContribute.idSentenceContribute}, $inc: {sentenceAdded: 1, contributed: 1}},
                            {upsert: true, new: true},
                            function(error, contributeInfo){
                                if(error){
                                    console.log("Update contribute video failed, error: " + error);
                                }else{
                                    console.log("Update contribute video success, contributeInfo: ");
                                    console.log(contributeInfo);
                                }
                            }
                        );
                    }else{
                        console.log("step 2: user only modifies sentence. So, don't update contribute data");
                    }

                    var subbingAction = new SubbingEvent();
                    subbingAction.user = req.user._id;
                    subbingAction.sentence = infoContribute.sentenceText;
                    subbingAction.language = infoContribute.language;
                    subbingAction.video = infoContribute.videoId;
                    subbingAction.save();
                })
                .then(function () {
                    console.log("step 3: response client");
                    res.status(200).json({message: 'created'});
                })
                .catch(function (error) {
                    console.log("step 3: catch and response error client, error: " + error);
                    res.status(500).json({
                        error: error
                    });
                })
                .done();
        },

        /**
         * Vote a sentence
         */
        vote: function (req, res) {
            Caption.update(
                { _id: req.body.captionId,  'sentence._id': req.body.sentenceId},
                { $addToSet: { 'sentence.$.ratedUser': req.user._id } },
                function(err, infoAdd){
                    if(err){
                        res.status(500).json({message: "Can't update vote"})
                    }else{
                        if (!infoAdd){
                            return res.status(500).json({ message: "Can't update vote count" });
                        }else{
                            if(infoAdd.nModified){
                                var data = {captionId: req.body.captionId,sentenceId: req.body.sentenceId};

                                Contribute.update(
                                    {user: req.user._id},
                                    {$push: {votes:  data}, $inc: {voteCount: 1, contributed: 1}},
                                    {upsert: true, new: true},
                                    function(error, contributeInfo){
                                        if(error){
                                            console.log("Update contribute vote failed, error: " + error);
                                        }else{
                                            console.log("Update contribute vote success, contributeInfo: ");
                                            console.log(contributeInfo);
                                        }
                                    }
                                );

                                return res.status(200).json({ message: 'Voted' });
                            }else{
                                return res.status(200).json({ message: "Voted found, don't count" });
                            }
                        }
                    }
                }
            );
        },

        /***
         * save sentence
         */
        saveSentence: function (req, res) {
            console.log(req.body);
            // debugger;
            Caption.findById(req.body.captionId, function (err, caption) {
                if (err) {
                    return res.status(500).json({
                        error: 'cannot find caption'
                    })
                }
                // var caption = new Caption();
                caption.sentence.push({
                    authorId: req.user,
                    text: req.body.text,
                    timeStart: req.body.timeStart,
                    timeEnd: req.body.timeEnd,
                    order: req.body.order
                });
                caption.save(function (err, caption) {
                    if (err) console.log(err.message);

                    res.json(caption)
                })
            })
        },

        /**
         * find one caption
         */
        show: function (req, res) {
            res.json(req.caption)
        },

        /**
         * update Caption
         */
        update: function (req, res) {

            Caption.findById(req.body._id, function (err, caption) {
                if (!caption) {
                    return next(new Error('Could not load Document'));
                } else {
                    caption = _.extend(caption, req.body);
                    caption.save(function (err) {
                        if (err) {
                            return res.status(500).json({
                                error: 'Cannot save caption'
                            })
                        }

                        res.json({message: req.flash('message')});
                    })
                }
            });
        },

        all: function (req, res) {

            Caption.find({})
                .sort('-created')
                .populate('user', 'name username')
                .populate('language')
                .populate('videoId')
                .lean()
                .exec(function (err, captions) {
                    if (err) {
                        return res.status(500).json({
                            error: 'Cannot list the captions'
                        });
                    }

                    res.json(captions);
            });
        },

        translator: function (req, res) {
            var paramTranslate = {};

            paramTranslate.caption_id = req.body.captionId;
            paramTranslate.language_symbol_target = 'en,ja,zh-CHS,zh-CHT,ko,vi';
            AutoTranslate.translateByBing(paramTranslate)
                .then(function (captionInDatabase) {
                    res.status(200).json( {
                        "result": true,
                        "videoId": captionInDatabase.videoId,
                        "language": captionInDatabase.language,
                        "caption_id": paramTranslate.caption_id,
                        "language_symbol_target": paramTranslate.language_symbol_target
                    });

                    var translateJob = new Job();
                    translateJob.job_attribute_symbol = 'Done';
                    translateJob.language_symbol_target = paramTranslate.language_symbol_target;
                    translateJob.caption_id = paramTranslate.caption_id;
                    translateJob.save();
                })
                .catch(function (error) {
                    console.log("catch error: " + error);
                    res.status(500).json({
                        "result": false,
                        "caption_id": paramTranslate.caption_id,
                        "language_symbol_target": paramTranslate.language_symbol_target
                    });

                    var translateJob = new Job();
                    translateJob.job_attribute_symbol = 'Failed';
                    translateJob.language_symbol_target = paramTranslate.language_symbol_target;
                    translateJob.caption_id = paramTranslate.caption_id;
                    translateJob.save();
                })
                .done();

            emtr.on('translator', function (data) {
                console.log('event emitted');
                if (data.videoId === req.body.videoId) {
                    res.json(data);
                }
            });
        },

        translated: function (req, res) {
            console.log('call test and emit \'translator\' event');
            emtr.emit('translator', req.body);
            res.status(200).json({message: 'success'})
        },

        /**
         * Find video created by user Id
         */
        findUserCaptions: function (req, res) {
            Caption.find({user: req.user})
                .populate('language')
                .populate('videoId')
                .lean()
                .exec(function (err, captions) {
                    if (err) {
                        return res.status(500).json({
                            error: 'Cannot list captions'
                        })
                    }

                    res.json(captions)
            })
        },
        /**
         * get top volunteers
         */
        getTopVolunteers: function (req, res) {
            Contribute.find({}, "user contributed videoUploaded sentenceAdded")
                .sort('-contributed')
                .limit(10)
                .populate('user', 'name username avatar')
                .lean()
                .exec(function (err, volunteers) {
                    if (err) {
                        console.log("getTopVolunteers err: " + err);
                        return res.status(500).json({
                            error: 'canot get list volunteer'
                        })
                    }

                    res.json(volunteers)
            })
        },

        /**
         * fix database contribute wrong in server
         */

        fixDataVolunteers: function(req, res)
        {
            Contribute.find({}).exec(function(err, arrayContribute){
                if(err){
                    console.log("fixDataVolunteers err: " + err);
                    return res.status(500).json({error: "can't get contribute"});
                }

                console.log("arrayContribute size: " + arrayContribute.length);

                arrayContribute.forEach(function(contribute){
                    // need fix arrayContribute.video and arrayContribute.sentence
                    //console.log(contribute);
                    var uniqueVideo = contribute.video.filter(function(item, pos) {
                        return contribute.video.indexOf(item) == pos;
                    });
                    contribute.video.remove();
                    contribute.video = uniqueVideo;
                    var uniqueSentence = contribute.sentence.filter(function(item, pos) {
                        return contribute.sentence.indexOf(item) == pos;
                    });
                    contribute.sentence.remove();
                    contribute.sentence = uniqueSentence;
                    contribute.contributed = contribute.sentence.length;

                    contribute.save();
                });

                res.status(200).json({message: "Finish fix data"});
            })
        },

        fixAiName: function(req, res){
            AutoTranslate.fixAiName()
                .then(function(){
                    res.status(200).json({message: true});
                })
                .catch(function(error){
                    console.log("fixAiName error" + error);
                    res.status(500).json({message: false});
                })
                .done();
        },

        fixSentence: function(req, res){
            AutoTranslate.doRemoveSentenceTagError(fixUserModifyText)
                .then(function(){
                    res.status(200).json({message: true});
                })
                .catch(function(error){
                    console.log("fixSentence error" + error);
                    res.status(500).json({message: false});
                })
                .done();
        },

        fixVideoCaptionAttribute: function (req, res) {
            function checkVideoHasCaption(video) {
                var deferred = Q.defer();

                Caption.findOne({videoId: video._id}, "", function (error, caption) {
                    if (error) {
                        console.log("doUpdateVideoAttribute error: " + error);
                        deferred.reject(new Error(error));
                    } else {
                        if(caption){
                            deferred.resolve(true);
                        }else{
                            deferred.resolve(false);
                        }
                    }
                });

                return deferred.promise;
            }

            function doUpdateVideoCaptionContribute(video) {
                return checkVideoHasCaption(video)
                    .then(function(hasCaption){
                        var needSave = false;

                        if(video.hasCaption){
                            if(video.hasCaption != hasCaption){
                                video.hasCaption = hasCaption;
                                needSave = true;
                            }
                        }else{
                            video.hasCaption = hasCaption;
                            needSave = true;
                        }

                        if(needSave){
                            return Q.ninvoke(video, "save")
                                .then(function () {
                                    // empty case but needed
                                });
                        }
                    })
            }

            Video.find({}, "hasCaption hasSegment", function (error, arrayVideo) {
                if(error){
                    res.status(500).json({message: "couldn't get list videos"});
                }else{
                    console.log("Total number video: " + arrayVideo.length);

                    var arrayPromise = [];
                    arrayVideo.forEach(function (video) {
                        arrayPromise.push(doUpdateVideoCaptionContribute(video));
                    });

                    Q.all(arrayPromise)
                        .then(function () {
                            res.json({message: "finish"});
                        })
                        .catch(function (error) {
                            res.status(500).json({message: "failed"});
                        })
                        .done();
                }
            })
        }
    }
};