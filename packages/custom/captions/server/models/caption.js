'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;


/**
 * Caption Schema
 */
var SentenceSchema = new Schema ({
  authorId: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  order: {
    type: Number,
    min: 1
  },
  rate: {
    type: Number,
    default: 0,
    min: 0
  },
  ratedUser: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  timeStart: String,
  timeEnd: String,
  text: String
},{
  timestamps: {
    createdAt: 'created_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});
var CaptionSchema = new Schema({
  videoId: {
    type: Schema.ObjectId,
    ref: 'Video'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  language: {
    type: Schema.ObjectId,
    ref: 'ai_language_support'
  },
  rate: {
    type: Number,
    min:0
  },
  url: {
    type: String
  },
  sentence: [SentenceSchema]
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Language
 */
var LanguageSchema = new Schema({
  created_at: {
    type: Date,
    default: Date.now
  },
  name: {
    type: String,
    unique: true
  }
});

CaptionSchema.plugin(findOrCreate);

mongoose.model('Sentence', SentenceSchema);
mongoose.model('Language',LanguageSchema);
mongoose.model('Caption',CaptionSchema);