'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * Caption Schema
 */
var ContributeSchema = new Schema ({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  video: [{
    type: Schema.ObjectId,
    ref: 'Video'
  }],
  videoUploaded: {
    type: Number,
    min: 0,
    default: 0
  },
  sentence: [{
    type: Schema.ObjectId,
    ref: 'Sentence'
  }],
  sentenceAdded: {
    type: Number,
    min: 0,
    default: 0
  },
  votes: [{
    _id:false,
    captionId:Schema.ObjectId,
    sentenceId:Schema.ObjectId
  }],
  voteCount: {
    type: Number,
    min: 0,
    default: 0
  },
  contributed: {
    type: Number,
    min: 0,
    default: 0
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

mongoose.model('Contribute', ContributeSchema);