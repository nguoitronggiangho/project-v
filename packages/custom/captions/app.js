'use strict';

/*
 * Defining the Package
 */
var Module = require('../../../core').Module;

var Captions = new Module('captions');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Captions.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Captions.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  Captions.menus.add({
    title: 'CaptionManager',
    link: 'caption admin page',
    roles: ['authenticated'],
    menu: 'admin'
  });

  Captions.angularDependencies([
    'ngMaterial',
    'ngMessages',
    'md.data.table'
  ]);
  
  Captions.aggregateAsset('css', 'captions.css');
  Captions.aggregateAsset('js', 'jwplayer.js', {group: 'header'});
  Captions.aggregateAsset('js', '../lib/angular-material-data-table/dist/md-data-table.js');
  Captions.aggregateAsset('css', '../lib/angular-material-data-table/dist/md-data-table.css');
  //Captions.aggregateAsset('js', 'demo.js');
  //Captions.aggregateAsset('js', 'ocLazyload.js');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Captions.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Captions.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Captions.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Captions;
});
