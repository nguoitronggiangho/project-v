(function () {
  'use strict';

  angular
    .module('mean.dashboard')
    .config(dashboard);

  dashboard.$inject = ['$stateProvider'];

  function dashboard($stateProvider) {
    $stateProvider.state('dashboard example page', {
      url: '/dashboard/example',
      templateUrl: 'dashboard/views/index.html'
    })
        .state('user page', {
          url: '/user/:userId',
          templateUrl: '/dashboard/views/profile.html',
          resolve: {
            loggedin: function (MeanUser) {
              return MeanUser.checkLoggedin();
            }
          }
        });
  }

})();
