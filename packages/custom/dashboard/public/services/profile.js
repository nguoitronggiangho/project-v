(function() {
  'use strict';

  angular
      .module('mean.dashboard')
      .factory('Profile', Profile);

  Profile.$inject = ['$resource', '$http'];

  /* @ngInject */
  function Profile($resource, $http) {
    
    return {
      user: $resource('/api/user/:id', null,
          {
            update: {method: 'PUT'}
          }),
      getContribData: getContribData,
      getUserMetadata: getUserMetadata,
      getUserSubbingData: getUserSubbingData,
      deleteFavorite: deleteFavorite
    };

    ////////////////////

    function getContribData(userId) {
      return $http.get('/api/contribute/'+userId)
          .then(getContribDataSuccess)
          .catch(getContribDataFailed);

      function getContribDataSuccess(response) {
        return response.data;
      }
      function getContribDataFailed(error) {
        console.log('XHR failed for Contribute data '+error.data);
      }
    }

    function getUserMetadata(userId) {
      return $http.get('/api/user-metadata/'+userId)
          .then(getMetadataSuccess)
          .catch(getMetadataFailed);

      function getMetadataSuccess(response) {
        return response.data;
      }
      function getMetadataFailed(error) {
        console.log('XHR failed for UserMetadata '+error.data);
      }
    }
    
    function getUserSubbingData(userId) {
      return $http.get('/api/TrackingEventSubbing/'+userId)
          .then(getSubbingDataSuccess)
          .catch(getSubbingDataFailed);

      function getSubbingDataSuccess(response) {
        return response.data
      }
      function getSubbingDataFailed(error) {
        console.log('XHR failed for User Subbing Data '+error.data);
      }
    }
    
    function deleteFavorite(videoId) {
      return $http.delete('/api/user-metadata/favorite/'+videoId)
          .then(deleteSuccess)
          .catch(deleteFailed);
      function deleteSuccess(response) {
        return response.data
      }
      function deleteFailed(error) {
        console.log('XHR failed for delete favorite '+error.data)
      }
    }

  }

})();

