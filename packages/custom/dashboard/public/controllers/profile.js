(function() {
  'use strict';

  angular
      .module('mean.dashboard')
      .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['$stateParams', 'Profile', '$scope', '$mdToast'];

  /* @ngInject */
  function ProfileController($stateParams, Profile, $scope, $mdToast) {
    var vm = this;
    var userId = $stateParams.userId;
    vm.title = 'ProfileController';

    activate();

    ////////////////

    function activate() {
      Profile.user.get({id:userId}, getProfileSuccess, error);
      getContribData();
      getUserMetadata();
      getUserSubbingData();
    }
    
    function getProfileSuccess(info) {
      return vm.user = info;
    }

    function error(e) {
      console.log(e)
    }

    function getContribData() {
      // console.log('gecontribute data');
      Profile.getContribData(userId)
          .then(function(data) {
            vm.contribute = data;
            return vm.contribute;
          })
    }
    
    function getUserMetadata() {
      Profile.getUserMetadata(userId)
          .then(function(data) {
            vm.metadata = data;
            return vm.metadata;
          })
    }
    
    function getUserSubbingData() {
      Profile.getUserSubbingData(userId)
          .then(function(data) {
            vm.recentContributions = data;
            return vm.recentContributions;
          })
    }

    this.deleteFavorite= function (videoId, index) {
      console.log(videoId);
      Profile.deleteFavorite(videoId)
          .then(function(response) {
            if (response.success === true) {
              $mdToast.show(
                  $mdToast.simple()
                      .textContent('Removed from favorites')
                      .position('top right')
                      .hideDelay(3000)
              );
              return vm.metadata.favorite.splice(index)
            }
          })
    };

    this.selectedTab = function(index) {
      // console.log(index);
      switch (index) {
        case 1:
          // console.log('your subscribe chanel');
          break;
        case 2:
          // console.log('your contributed videos');
          break;
        case 3:
          // console.log('recent contributes');
          break;
        default:
          // console.log('your favorite videos')
      }
    }



  }

})();

