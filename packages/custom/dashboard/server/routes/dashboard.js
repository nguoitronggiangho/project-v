(function () {
  'use strict';

  /* jshint -W098 */
  // The Package is past automatically as first parameter
  module.exports = function (Dashboard, app, auth, database) {
    
    var profile = require('../controllers/profile')();

    app.route('/api/user/:id')
        .get(profile.findOne);
    
    app.route('/api/contribute/:userId')
        .get(profile.getContribData);
    
    app.route('/api/user-metadata/:userId')
        .get(profile.getUserMetadata);
    
    app.route('/api/TrackingEventSubbing/:userId')
        .get(profile.getUserSubbingData)
  };
})();
