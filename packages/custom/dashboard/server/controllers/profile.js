'use strict';

var Contrib = require('../../../captions/server/models/contribute');
var mongoose = require('mongoose');
var Contribute = mongoose.model('Contribute');
var User = mongoose.model('User');
var UserMetadata = mongoose.model('UserMetadata');
var UserSubbingData = mongoose.model('TrackingEventSubbing');

module.exports = function() {
  return {
    findOne: function(req, res) {
      // console.log(req.params);
      User.findOne({_id: req.params.id}, function(err, user) {
        if (err) {
          return res.status(500).json({
            error: 'cannot get user info'
          })
        }

        res.json(user)
      })
    },
    
    getContribData: function(req, res) {
      Contribute.findOne({user: req.params.userId})
          .populate('video')
          .lean()
          .exec(function(err, contrib) {
            if (err) {
              console.log(err);
              return res.status(500).json({
                error: 'cannot get contribute data'
              })
            }

            res.json(contrib)
          })
    },

    getUserMetadata: function(req, res) {
      UserMetadata.findOne({user: req.params.userId})
          .populate('favorite')
          .exec(function(err, metadata) {
            if (err) {
              console.log(err);
              return res.status(500).json({
                error: err
              })
            }

            res.json(metadata)
          })
    },

    getUserSubbingData: function(req, res) {
      UserSubbingData.find({user: req.params.userId})
          .sort('-created_at')
          .limit(20)
          .populate('video')
          .populate('language')
          .lean()
          .exec(function(err, data) {
            if (err) {
              console.log(err);
              return res.status(500).json({
                error: err
              })
            }

            res.json(data)
          })
    }
  }
};