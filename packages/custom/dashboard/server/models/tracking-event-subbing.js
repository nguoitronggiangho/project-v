var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TrackingEventSubbingSchema = new Schema ({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  sentence: String,
  language: {
    type: Schema.ObjectId,
    ref: 'ai_language_support'
  },
  video: {
    type: Schema.ObjectId,
    ref: 'Video'
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  }
});

mongoose.model('TrackingEventSubbing', TrackingEventSubbingSchema);