var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserMetadataSchema = new Schema ({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    require: true
  },
  favorite: [{
    type: Schema.ObjectId,
    ref: 'Video'
  }],
  channel: [{
    role: {type: String, enum: ['admin', 'moderator']},
    channel: { type: Schema.ObjectId, ref: 'Channel' }
  }]

},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * virtual
 */
UserMetadataSchema.virtual('favoriteCount').get(function() {
  return this.favorite.length;
});

mongoose.model('UserMetadata', UserMetadataSchema);