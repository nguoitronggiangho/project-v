'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TrackingEventViewedSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  video: [{
    type: Schema.ObjectId,
    ref: 'Video'
  }]
});

TrackingEventViewedSchema.virtual('viewedCounnt').get(function() {
  return this.video.length;
});

mongoose.model('TrackingEventViewed', TrackingEventViewedSchema);