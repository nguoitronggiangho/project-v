'use strict';

var debug = require('debug')('app:report:model:' + process.pid);
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReportSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    reuired: true
  },
  video: {
    type: Schema.ObjectId,
    ref: 'Video'
  },
  content: {
    type: String,
    trim: true,
    required: true
  }
});

mongoose.model('Report', ReportSchema);