'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

/**
 * Tags Schema
 */
var CategorySchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  order: {
    type: Number,
    min: 0,
    default: 0
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  }
});

CategorySchema.path('name').validate(function (name) {
  return !!name;
}, 'Tag name cannot be blank');

CategorySchema.plugin(findOrCreate);

mongoose.model('Category', CategorySchema);