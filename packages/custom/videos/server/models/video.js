'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    debug = require('debug')('app:videos:model:', + process.pid),
    random = require('mongoose-simple-random'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

/**
 * Validate
 */
var validateUniqueYoutubeId = function(value, callback) {
  var Video = mongoose.model('Video');
  Video.find({
    $and: [{
      youtubeId: value
    }, {
      _id: {
        $ne: this._id
      }
    }]
  }, function(err, video) {
    callback(err || video.length === 0);
  })
};


/**
 * Video Schema
 */
var VideoSchema = new Schema({
  title: {
    type: String,
    required: true,
    trim: true,
    index: true
  },
  youtubeId: {
    type: String,
    unique: true,
    validate: [validateUniqueYoutubeId, 'Video already exits']
  },
  url: {
    type: String,
    required: true,
    trim: true
  },
  thumbnail: {
    type: String
  },
  view: {
    type: Number,
    default: 0
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  channel: {
    type: Schema.ObjectId,
    ref: 'Channel',
    required: true
  },
  hasSegment: {
    type: Boolean,
    default: false
  },
  hasCaption: {
    type: Boolean,
    default: false
  },
  lockedLanguage: [{
      type: Schema.Types.ObjectId,
      ref: 'ai_language_support'
    }],
  tags: [{
    type: Schema.ObjectId,
    ref: 'Tag'
  }],
  categories: [{
    type: Schema.ObjectId,
    ref: 'Category'
  }],
  description: {
    type: String
  },
  voted: [{
    type: Schema.ObjectId,
    ref: 'User'
  }]
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

/**
 * Indexes
 */
VideoSchema.index({ title: 'text'});

/**
 * Validations
 */
VideoSchema.path('title').validate(function(title) {
  return !!title;
}, 'Title cannot be blank');

VideoSchema.path('url').validate(function(url) {
  return !!url;
}, 'Url cannot be blank');

/**
 * Statics
 */
VideoSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('user', 'name username').exec(cb);
};

/***
 * virtual
 */
VideoSchema.virtual('voteCount').get(function () {
  return this.voted.length;
});

/**
 * Methods
 */
VideoSchema.method('getSegment', function(cb) {
  var Segment = mongoose.model('Segment');
  Segment.findOne({video: this._id}, function(err, segment) {
   cb(err, !!segment);
  })
});

/***
 * Plugin
 */
VideoSchema.plugin(findOrCreate);
VideoSchema.plugin(random);

mongoose.model('Video', VideoSchema);
