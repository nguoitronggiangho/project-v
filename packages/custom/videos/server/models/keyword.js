'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

/**
 * Keyword Schema
 */

var KeywordSchema = new Schema({
  keyWord: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    index: true
  },
  searchPeople: [{
    type: Schema.ObjectId,
    ref: 'User'
  }]
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  },
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});
/**
 * Indexes
 */
KeywordSchema.index({ keyWord: 'text'});

/**
 * Validations
 */
KeywordSchema.path('keyWord').validate(function(title) {
  return !!title;
}, 'Keyword cannot be blank');

/***
 * virtual
 */
KeywordSchema.virtual('searchCount').get(function () {
  if (this.searchPeople) {
    return this.searchPeople.length;
  }

  return false
});

/***
 * Plugin
 */
KeywordSchema.plugin(findOrCreate);

/***
 * Indexes
 */
// KeywordSchema.index({})

mongoose.model('Keyword', KeywordSchema);