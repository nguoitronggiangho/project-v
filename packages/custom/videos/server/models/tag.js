'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

/**
 * Tags Schema
 */
var TagSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    unique: true
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  }
});

TagSchema.path('name').validate(function (name) {
  return !!name;
}, 'Tag name cannot be blank');

TagSchema.plugin(findOrCreate);

mongoose.model('Tag', TagSchema);