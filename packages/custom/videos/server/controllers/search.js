'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
    Video = mongoose.model('Video'),
    Keyword = mongoose.model('Keyword'),
    YouTube = require('youtube-node'),
    config = require('../../../../../core').loadConfig(),
    _ = require('lodash'),
    Q = require('q'),
    exec = require('child_process').exec;

module.exports = function(Videos) {
  
  return {
    /**
     * search video
     */
    search: function(req, res) {
      // console.log(req.params);
      // debugger;

      var searchKey = req.params.keyWord.toLowerCase();
      Video.find(
          { $text : { $search : req.params.keyWord } },
          { score : { $meta: "textScore" } }
          )
          .sort({ score : { $meta : 'textScore' } })
          .limit(10)
          .lean()
          .exec(function(err, results) {
            if (err) console.log(err);
            // console.log(results);

            var youTube = new YouTube();

            youTube.setKey(config.youtubeSearchKey);

            youTube.search(searchKey, 10, function(error, result) {
              if (error) {
                console.log(error);
              }
              else {
                // console.log(JSON.stringify(result, null, 2));
                res.json({
                  server: results,
                  youtube: result
                })
              }
            });

          });

      Keyword.findOrCreate({keyWord: searchKey}, function(err, click, created) {
        if (req.user) {
          click.update({ $addToSet: { searchPeople: req.user } },
              function(err, result) {
                if (err) console.log(err);
                console.log(result);
              })
        }
      })
    },

    find: function(req, res) {
      var searchKey = req.params.keyWord.toLowerCase();
      
      var query =  Keyword.find(
          { $text : { $search : searchKey } },
          { score : { $meta: "textScore" } }
          );

      query.select('keyWord');
      
      query.sort({ score : { $meta : 'textScore' } })
      .limit(10)
      .lean()
      .exec(function(err, results) {
        if (err) console.log(err);
        // console.log(results);
        res.json(results)
      });
    },
  }
};