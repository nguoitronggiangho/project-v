'use strict';

var debug = require('debug')('app:videov2:controller');
var mongoose = require('mongoose');
var Video = mongoose.model('Video');
var Category = mongoose.model('Category');
var Tag = mongoose.model('Tag');
var meanio = require('../../../../../core').loadConfig();
var _ = require('lodash');

module.exports = function(Videos) {
  return {
    /**
     * Find Video by Id
     */
    video: function (req, res, next, id) {
      Video.load(id, function (err, video) {
        if (err) return next(err);
        if (!video) return next(new Error('Failed to load video'));
        req.video = video;
        next();
      })
    },

    /**
     * List Video
     */
    all: function (req, res) {
      // console.log(req.acl);
      // console.log(req.query);
      console.log(req.user);
      //var query = req.acl.query(Video);
      var limit = req.query.limit || 50;
      var skip  = req.query.page === 1 ? 0 : (req.query.page - 1) * limit;
      var order = req.query.order || '-created';
      var filter = req.query.filter || ' ';
      var re = new RegExp(filter, 'i');

      Video.count({}, function (err, result) {
        if (err) {
          next(err);
        } else {
          Video.find({title: { $regex : re }})
              .limit(limit)
              .sort(order)
              .skip(skip)
              .populate('user', 'user username name')
              .populate('tags', 'name')
              .populate('categories', 'name')
              .populate('lockedLanguage', 'language_symbol')
              .exec(function (err, videos) {
                if (err) {
                  return res.status(500).json({
                    error: 'Cannot list the videos'
                  })
                }

                res.json({
                  data: videos,
                  count: result
                })
              });
        }
      });


    },
    /**
     * Delete a video
     */
    destroy: function (req, res) {
      var video = req.video;

      video.remove(function (err) {
        if (err) {
          debug('Error while delete video '.red, err);
          return res.status(500).json({
            error: 'Cannot delete video'
          })
        }

        Videos.events.publish({
          action: 'deleted',
          user: {
            name: req.user.name
          },
          name: video.title
        });

        res.json(video);
      })
    },
    /**
     * Update a video
     */
    update: function (req, res) {
      var video = req.video;

      video = _.extend(video, req.body);

      video.save(function (err) {
        if (err) {
          debug('Error while update video'.red, err);
          return res.status(500).json({
            error: 'Cannot update the video'
          })
        }

        res.json(video);
        debug('Update video success'.yellow);
      })
    },
    /**
     * Show a video
     */
    show: function (req, res) {
      Video.findOne({_id: req.video._id}).populate('lockedLanguage').populate('tags').populate('categories').exec( function (err, video) {
        video.view += 1;
        video.save(function (err) {
          if (err) {
            debug('Error get video'.red, err);
            console.log(err)
          }
          console.log('view +1')
        });

        res.json(video);
      });
    }
  }
};