'use strict';

require('../../../dashboard/server/models/user-metadata');
var mongoose = require('mongoose');
var Metadata = mongoose.model('UserMetadata');
var debug = require('debug')('app:usermetadata'+process.pid);

module.exports = function(Videos) {
  return {
    addFavorite: function(req, res) {
      Metadata.findOne({user: req.user}, function(err, data) {
        if (err) {
          debug('cannot find user metadata');
          res.status(500).json({
            error: err,
            success: false
          })
        }

        if (!data) {
          debug('user not have metadata yet');
          var metadata = new Metadata();
          metadata.user = req.user;
          metadata.favorite.addToSet(req.params.videoId);
          metadata.save(function(err, data) {
            if (err) {
              debug('error when save favorite video '+ err.message);
              return res.status(500).json({
                error: err,
                success: false
              })
            }

            debug('save favorite done');
            res.json(data)
          })
        } else {
          debug('update user metadata');
          data.favorite.addToSet(req.params.videoId);
          data.save(function(err, data) {
            if (err) {
              debug('error when save favorite video '+ err.message);
              return res.status(500).json({
                error: err,
                success: false
              })
            }

            debug('save favorite done');
            res.json(data)
          })
        }
      });

    },
    /**
     * Delete favorite video
     */
    deleteFavorite: function(req, res) {
      console.log(req.params);
      Metadata.findOne({user: req.user}, function(err, metadata) {
        if (err) {
          debug('cannot find user metadata');
          return res.status(500).json({
            error: err,
            success: false
          })
        }

        metadata.favorite.pull(req.params.videoId);
        metadata.save(function(err, data) {
          if (err) {
            debug('error when save favorite video '+ err.message);
            return res.status(500).json({
              error: err,
              success: false
            })
          }

          debug('save favorite done ');
          res.json({
            success: true,
            data: data
          })
        })
      })

    }
  }
};