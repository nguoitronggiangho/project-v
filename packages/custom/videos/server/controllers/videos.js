'use strict';

/**
 * Module dependencies
 */
require('../../../ai_helper/server/models/ai_helper');
require('../../../captions/server/models/contribute');
require('../../../channel/server/models/channel');
require('../models/traking-event-viewed');

var debug = require('debug')('app:videos:controller' + process.pid);
var mongoose = require('mongoose'),
    Video = mongoose.model('Video'),
    Category = mongoose.model('Category'),
    Tag = mongoose.model('Tag'),
    Keyword = mongoose.model('Keyword'),
    Contribute = mongoose.model('Contribute'),
    Channel = mongoose.model('Channel'),
    TrackingEventViewed = mongoose.model('TrackingEventViewed'),
    config = require('../../../../../core').loadConfig(),
    _ = require('lodash'),
    multiparty = require('multiparty'),
    fs = require('fs'),
    Q = require('q'),
    moment = require('moment'),
    exec = require('child_process').exec;

var redis = require('redis');
var sub = redis.createClient();
var pub = redis.createClient();

function findVideo(channel) {
  var deferred = Q.defer();
  Video.find({channel: channel})
      .sort('-created_at')
      .limit(12)
      .exec(function(err, videos) {
        if (err) {
          debug('Error while find video in step3'.red.inverse, err);
          deferred.reject(err)
        }
        deferred.resolve(videos)
      });
  return deferred.promise
}

function map(channels) {
  var promiseArray = channels.map(function(channel) {
    return findVideo(channel)
        .then(function(videos) {
          return {
            channel: channel,
            videos: videos
          }
        })
  });
  return Q.all(promiseArray)
}
function findViewedVideos(user) {
  var deferred = Q.defer();
  TrackingEventViewed.findOne({user: user}, function(err, viewedVideos) {
    if (err) {
      debug('Error while find viewed data for request user '.red.inverse, err);
      deferred.reject(err)
    } else if (!viewedVideos) {
      deferred.resolve([])
    } else {
      deferred.resolve(viewedVideos.video)
    }
  });
  return deferred.promise
}

module.exports = function (Videos) {

  return {
    /**
     * Find Video by Id
     */
    video: function (req, res, next, id) {
      Video.load(id, function (err, video) {
        if (err) return next(err);
        if (!video) return next(new Error('Failed to load video'));
        req.video = video;
        next();
      })
    },
    /**
     * Init category
     */
    createCategory: function (req, res) {
      var count = 0;
      var categories = ['Game show', 'Kinh doanh Giáo dục', 'Khoa học công nghệ', 'Giải trí'];
      for (var i = 0; i<categories.length;i++) {
        var cate = new Category();
        cate.name = categories[i];
        cate.save(function (err, cate) {
          count++;
          if (count === categories.length) {
            res.json(cate)
          }
        });

      }

    },
    getAllCategories: function (req, res) {
      Category.find({}, function (err, categories) {
        if (err) {
          return res.status(500).json({
            error: 'cannot get categories'
          })
        }
        
        res.json(categories)
      })
    },
    /**
     * Save video with youtube link
     */
    youtube: function (req, res) {
      console.log(req.body);
      // debugger;
      var reqTags = req.body.tags;
      var reqCate = req.body.categories;

      var video = new Video();
      video.user = req.user;
      video.url = 'https://www.youtube.com/watch?v='+req.body.youtubeId;
      video.title = req.body.title;
      video.youtubeId = req.body.youtubeId;
      video.thumbnail = 'http://i.ytimg.com/vi/'+req.body.youtubeId+'/mqdefault.jpg';
      video.description = req.body.description;
      video.channel = req.body.channel;
      reqCate.forEach(function (cate) {
        video.categories.push(cate)
      });

      var arrayFoundTagPromise = [];
      var arrayNewPromise = [];

      function findOrCreateTag(tagName){
        var deferred = Q.defer();

        Tag.findOne({name: tagName}, function (err, tag) {
          if (err) {
            console.log("have an error in find tag name: " + tagName);
            deferred.reject(new Error(err));
          } else {
            if(tag){
              video.tags.push(tag);
              deferred.resolve(tag);
            }else{
              var newTag = new Tag();
              newTag.name = tagName;
              video.tags.push(newTag);
              arrayNewPromise.push(Q.ninvoke(newTag, "save"));
              deferred.resolve(newTag);
            }
          }
        });

        return deferred.promise;
      }

      reqTags.forEach(function (tagName) {
        arrayFoundTagPromise.push(findOrCreateTag(tagName));
      });

      Q.all(arrayFoundTagPromise)
          .then(Q.all(arrayNewPromise))
          .then(function () {
            return Q.ninvoke(video, 'save')
          })
          .then(function() {
            res.json(video);

            var searchKey = video.title.toLowerCase();

            Keyword.findOrCreate({keyWord: searchKey}, function(err, click, created) {
              if (req.user) {
                click.update({ $addToSet: { searchPeople: req.user } },
                    function(err, result) {
                      if (err) console.log(err);
                    })
              }
            });

            // in the case add, id has just created and unique, so, using push is good enough
            Contribute.update(
              {user: req.user._id},
              {$push: {video:  video._id}, $inc: {videoUploaded: 1, contributed: 1}},
              {upsert: true, new: true},
              function(error, contributeInfo){
                if(error){
                  console.log("Update contribute video failed, error: " + error);
                }else{
                  console.log("Update contribute video success, contributeInfo: ");
                  console.log(contributeInfo);
                }
              }
            );
          })
          .catch(function (err) {
            // console.log(err);
            var modelErrors = [];

            if (err.errors) {

              for (var x in err.errors) {
                modelErrors.push({
                  param: x,
                  msg: err.errors[x].message,
                  value: err.errors[x].value
                });
              }

              return res.status(400).json(modelErrors);
            }
            res.status(500).json({
              error: 'cannot save video'
            });
          })
          .done();
    },
    /**
     * Create Video with file
     */
    upload: function (req, res) {
      var form = new multiparty.Form();
      form.parse(req, function(err, fields, files) {
        console.log(fields);
        //res.writeHead(200, {'content-type': 'text/plain'});
        //res.end(util.inspect({fields: fields, files: files}));
        var file = files.file[0];
        var contentType = file.headers['content-type'];
        var tmpPath = file.path;
        //var extIndex = tmpPath.lastIndexOf('.');
        //var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
        // uuid is for generating unique filenames.
        //var fileName = uuid.v4() + extension;
        var fileName = file.originalFilename;
        var destPath = 'packages/custom/videos/public/assets/video/' + fileName;
        var showPath = '/videos/assets/video/' + fileName;

        // Server side file type checker.
        if (contentType !== 'video/mp4') {
          fs.unlink(tmpPath);
          return res.status(400).send({
            message: 'Unsupported file type'
          });
        }

        fs.rename(tmpPath, destPath, function(err) {
          if (err) {
            return res.status(400).send({
              message: 'Video is not saved'
            });
          }
          fs.unlink(tmpPath, function() {
            if (err) {
              return res.status(400).send({
                message: 'Impossible to delete temp file'
              });
            }
          });

          exec('ffmpeg -i ' + destPath  + ' -ss 00:30 -r 1 -an -vframes 1 -f mjpeg packages/custom/videos/public/assets/video/thumbnail/' + fileName  + '.jpg', function(err){
            if (err) {
              console.log(err);
              console.log('cannot create thumbnail')
            }
          });

          var video = new Video();
          video.user = req.user;
          video.url = showPath;
          video.title = fields.title;
          video.thumbnail = '/videos/assets/video/thumbnail/' + fileName + '.jpg';

          video.save(function (err) {
            if (err) {
              return res.status(500).json({
                error: 'Cannot save video'
              })
            }

            res.json(video);
          })

        });
      })
    },
    /**
     * Create a video
     */
    create: function (req, res) {
      console.log(req.body);
      var video = new Video(req.body);
      video.user = req.user;

      video.save(function (err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot save video'
          })
        }

        res.json(video);
      })
    },
    /**
     * Update a video
     */
    update: function (req, res) {
      var video = req.video;

      video = _.extend(video, req.body);

      video.save(function (err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot update the video'
          })
        }

        res.json(video)
      })
    },
    /**
     * Delete a video
     */
    destroy: function (req, res) {
      Video.findByIdAndRemove(req.params.videoId, function(error){
        if (err) {
          return res.status(500).json({
            error: 'Cannot delete video'
          })
        }

        res.json({message: true});
      });
    },
    /**
     * Show a video
     */
    show: function (req, res) {
      Video.findByIdAndUpdate(req.params.videoId, {$inc: { view: 1 }})
          .populate('lockedLanguage')
          .populate('tags')
          .populate('categories')
          .populate('channel')
          .exec( function (err, video) {
            if (err) {
              debug('Error while find video '.red.inverse, err);
              res.status(500).json({message: false});
            }
            video = video.toJSON();
            if (req.user) {
              var subscribeState = video.channel.subscriber.some(function (user) {
                return user.equals(req.user._id);
              });
              video.channel.subscribeState = subscribeState;
              delete video.channel.subscriber;

              var viewed = new TrackingEventViewed();
              viewed.user = req.user;
              viewed.video.addToSet(req.params.videoId);
              viewed.save(function(err) {
                if (err) {
                  debug('Error while save viewed data '.red.inverse, err);
                  return res.status(500).json({
                    error: 'Error while save viewed data'
                  })
                }

                debug('Save viewed video data done'.green.inverse)
              })
            }

            debug('Get video success\n'.green.inverse);
            res.json(video);
      });
    },
    /**
    * List Video
    */
    all: function (req, res) {
      //console.log(req.acl);
      //var query = req.acl.query(Video);
      // console.log(req.params);
      console.log(req.query);
      var query = {};
      var re = new RegExp('([^\s])');

      if (req.query.query) {
        query = JSON.parse(req.query.query);
      }
      if (req.query.title) {
        query.title = new RegExp(req.query.title, 'i')
      }
      var limit = req.query.limit || 50;
      var order = req.query.order || '-created';
      var cate = req.query.cate;
      var skip = req.query.skip || 0;

      if (cate) {
        query.categories = cate
      }

      console.log(query);
      Video.find(query)
          .sort('-' + order)
          .skip(skip)
          .limit(limit)
          .populate('user', 'user username name')
          .populate('tags')
          .populate('categories')
          .populate('channel', 'name avatar')
          .lean()
          .exec(function (err, videos) {
            if (err) {
              console.log(err);
              return res.status(500).json({
                error: 'Cannot list the videos'
              })
            }

        res.json(videos)
      })
    },
    /**
     * Get video by channel for Videos - Home page
     */
    getVideoByChannel: function(req, res) {
      function getPopularVideo() {
        var deferred = Q.defer();
        Video.find({})
            .where('created_at').gt(moment().add(-30, 'days')).lt(Date.now())
            .sort('-view')
            .limit(12)
            .populate('user', 'name')
            .populate('channel', 'name')
            .exec(function(err, popular) {
              if (err) {
                debug('Error when get popular video '.red.inverse, err);
                deferred.reject(err);
              }

              deferred.resolve(popular)
            });
        return deferred.promise
      }
      function getLatestVideo() {
        var deferred = Q.defer();
        Video.find({})
            .sort('-created_at')
            .limit(12)
            .populate('user', 'name')
            .populate('channel', 'name')
            .exec(function(err, latest) {
              if (err) {
                debug('Error when get latest videos '.red.inverse, err);
                deferred.reject(err);
              }

              deferred.resolve(latest)
            });
        return deferred.promise
      }
      function getTopChannelVideo() {
        var deffered = Q.defer();
        Channel.find({feature: true})
            .limit(4)
            .exec(function(err, channels) {
              if (err) {
                deffered.reject(err)
              }
              deffered.resolve(channels)
            });
        return deffered.promise
      }
      function findVideo(channel) {
        var deferred = Q.defer();
        Video.find({channel: channel})
            .sort('-created_at')
            .limit(12)
            .exec(function(err, videos) {
              if (err) {
                debug('Error while find video in step3'.red.inverse, err);
                deferred.reject(err)
              }
              deferred.resolve(videos)
            });
        return deferred.promise
      }

      function map(channels) {
        var promiseArray = channels.map(function(channel) {
          return findVideo(channel)
              .then(function(videos) {
                return {
                  channel: channel,
                  videos: videos
                }
              })
        });
        return Q.all(promiseArray)
      }
      function findViewedVideos() {
        var deferred = Q.defer();
        TrackingEventViewed.findOne({user: req.user}, function(err, viewedVideos) {
          if (err) {
            debug('Error while find viewed data for request user '.red.inverse, err);
            deferred.reject(err)
          } else if (!viewedVideos) {
            deferred.resolve([])
          } else {
            deferred.resolve(viewedVideos.video)
          }
        });
        return deferred.promise
      }

      getTopChannelVideo()
          .then(function(channels) {
            return map(channels)
          })
          .then(function(result) {
            return getLatestVideo()
                .then(function(latestVideos) {
                  return {
                    topChannel: result,
                    latestVideos: latestVideos
                  }
                });
          })
          .then(function(result) {
            return getPopularVideo()
                .then(function(popularVideos) {
                  result.popularVideos = popularVideos;
                  return result
                })
          })
          .then(function(data) {
            if (!req.user) {
              debug('Find videos for Video - Home page success'.green.inverse);
              res.json(data)
            } else {
              findViewedVideos()
                  .then(function(viewedVideos) {
                    data.topChannel = data.topChannel.map(function(item) {
                      return {
                        channel: item.channel,
                        videos: item.videos.filter(function(video) {
                          return viewedVideos.indexOf(String(video._id)) === -1
                        })
                      }
                    });
                    data.latestVideos =  data.latestVideos.filter(function(video) {
                      return viewedVideos.indexOf(String(video._id)) === -1
                    });
                    data.popularVideos = data.popularVideos.filter(function(video) {
                      return viewedVideos.indexOf(String(video._id)) === -1
                    });

                    debug(`Get Home page videos for user ${req.user.name} success`.green);
                    res.json(data)
                  })
            }
          })
          .catch(function(err) {
            Debug('Error when get video for Videos-home page '.red.inverse, err);
            res.status(500).json({
              error: 'Error when get video for Videos-home page'
            })
          })
    },
    loadMore: function(req, res) {
      debug(req.body);
      function getMoreChannel() {
        var deferred = Q.defer();
        Channel.find({})
            .where('_id').nin(req.body.channel)
            .limit(8)
            .exec(function(err, channels) {
              if (err) {
                deferred.reject(err)
              } else {
                deferred.resolve(channels)
              }
            });
        return deferred.promise
      }
      getMoreChannel()
          .then(function(channels) {
            return map(channels)
          })
          .then(function(result) {
            if (!req.user) {
              debug('Loadmore for anonymous user success'.green.inverse);
              res.json(result)
            } else {
              findViewedVideos(req.user)
                  .then(function(viewedVideos) {
                    result.map(function(item) {
                      return {
                        channel: item.channel,
                        videos: item.videos.filter(function(video) {
                          return viewedVideos.indexOf(String(video._id)) === -1
                        })
                      }
                    });

                    debug(`Loadmore for user ${req.user.name} success`.green.inverse);
                    res.json(result)
                  })
            }

          })
          .catch(function(err) {
            debug(`Error when loadmore item`.red.inverse, err);
            return res.status(500).json({
              error: `Error when loadmore item`
            })
          })
    },
    /**
     * find video created by user
     */
    findUserVideos: function (req, res) {
      Video.find({user: req.user}, function (err, videos) {
        if (err) {
          return res.status(500).json({
            error: 'cannot list your videos'
          })
        }

        res.json(videos)
      })
    },
    /**
     * find 10 trending video
     */
    trending: function (req, res) {
      Video.find({}).sort('-view').populate('user', 'user username').limit(10).exec(function (err, videos) {
        if (err) {
          console.log(err);
          return res.status(500).json({
            error: 'cannot get list trending'
          })
        }

        res.json(videos);
      })
    },
    vote: function (req, res) {
      Video.update({ _id: req.body.videoId },
          { $addToSet: { voted: req.user } },
          function(err, result) {
            res.json(result)
          }
      );
    },
    getTrendingForMainPage: function(req, res) {
      Video.find({})
          .where('created_at').gt(moment().add(-30, 'days')).lt(Date.now())
          .sort('-view')
          .limit(50)
          .populate('user', 'name')
          .populate('channel', 'name')
          .exec(function(err, videos) {
            if (err) {
              debug('Error while get trending video '.red.inverse, err);
              return res.status(500).json({
                error: 'Error while get trending video'
              })
            }

            res.json(videos)
          })
    },
    getSubscriptionsVideos: function(req, res) {
      function findSubscriptionData() {
        var deferred = Q.defer();
        Channel.find({subscriber: req.user})
            .sort('-created_at')
            .limit(5)
            .exec(function(err, channels) {
              if (err) {
                debug('Error while find user channel subscriptions data '.red.inverse, err);
                deferred.reject(err)
              }
              deferred.resolve(channels)
            });
        return deferred.promise
      }
      function findViewedVideos() {
        var deferred = Q.defer();
        TrackingEventViewed.findOne({user: req.user}, function(err, viewedVideos) {
          if (err) {
            debug('Error while find viewed data for request user '.red.inverse, err);
            deferred.reject(err)
          } else if (!viewedVideos) {
            deferred.resolve([])
          } else {
            deferred.resolve(viewedVideos.video)
          }
        });
        return deferred.promise
      }

      function findVideo(channel) {
        var deferred = Q.defer();
        Video.find({channel: channel})
            .sort('-created_at')
            .limit(12)
            .exec(function(err, videos) {
              if (err) {
                debug('Error while find video in step3'.red.inverse, err);
                deferred.reject(err)
              }
              deferred.resolve(videos)
            });
        return deferred.promise
      }

      function map(channels) {
        var promiseArray = channels.map(function(channel) {
          return findVideo(channel)
              .then(function(videos) {
                return {
                  channel: channel,
                  videos: videos
                }
              })
        });
        return Q.all(promiseArray)
      }

      debug(`Find subscriptions data for subscriptions page`.magenta.inverse);
      debug('Step1: find subscription channel'.blue.inverse);
      findSubscriptionData()
          .then(function(channels) {
            debug(`User had Subscribed ${channels.length} channels`.green);
            debug('Step2: Find videos by channel'.blue.inverse);
            return map(channels);
          })
          .then(function(result) {
            debug('Step3: find viewed video'.blue.inverse);
            // console.log(result);
            return findViewedVideos()
                .then(function(viewedVideos) {
                  debug(`User had viewed ${viewedVideos.length} videos`.green);
                  return {
                    viewedVideos: viewedVideos,
                    data: result
                  }
                });
          })
          .then(function(result) {
            debug('Step4: remove viewed videos'.blue.inverse);
            var data = result.data;
            data = data.map(function(data) {
              return {
                channel: data.channel,
                videos: data.videos.filter(function(video) {
                  return result.viewedVideos.indexOf(String(video._id)) === -1
                })
              }
            });
            res.json(data)
          })
          .catch(function(err) {
            debug('Error when find user subscriptions video '.red.inverse, err);
            res.status(500).json({
              error: err,
              message: 'Error when find user subscriptions video'
            })
          })

    },
    getRandom: function(req, res) {
      console.log(req.query);
      let limit = req.query.limit;
      Video.findRandom({}, {}, {limit: limit, populate: 'channel'}, function(err, results) {
        if (err) {
          debug('Error when get random video '.red.inverse, err);
          return res.status(500).json({
            error: 'Error when get random video'
          })
        }

        res.json(results)
      });
    }
  }
};
