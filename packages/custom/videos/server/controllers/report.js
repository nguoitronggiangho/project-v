module.exports = (function() {
  'use strict';

  const debug = require('debug')('app:report:controller:' + process.pid);
  const mongoose = require('mongoose');
  const Report = mongoose.model('Report');

  class Reports {

    static create(req, res) {
      let report = new Report;
      report.user = req.user;
      report.video = req.body.video;
      report.content = req.body.content;

      report.save(function(err) {
        if (err) {
          debug('Error while save report '.red.inverse, err);
          return res.status(500).json({
            error: err
          })
        }

        debug('Crated report data'.green.inverse);
        res.json({
          message: 'Success'
        })
      })
    }

  }

  return Reports

})();