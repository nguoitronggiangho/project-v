'use strict';

var debug = require('debug')('app:video:route' + process.pid);

// Video authorization helpers
var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin && !req.video.user._id.equals(req.user._id)) {
    debug('Error when checking `hasAuthorization`'.red);
    console.log(req.user);
    return res.status(401).send('User is not authorized');
  }
  next();
};

//var hasPermissions = function(req, res, next) {
//
//  req.body.permissions = req.body.permissions || ['authenticated'];
//
//  for (var i = 0; i < req.body.permissions.length; i++) {
//    var permission = req.body.permissions[i];
//    if (req.acl.user.allowed.indexOf(permission) === -1) {
//      return res.status(401).send('User not allowed to assign ' + permission + ' permission.');
//    }
//  }
//
//  next();
//};

module.exports = function(Videos, app, auth) {

  var videos = require('../controllers/videos')(Videos);
  var search = require('../controllers/search')(Videos);
  var userMetadata = require('../controllers/usermetadata')(Videos);
  var report = require('../controllers/report');

  app.route('/api/videos')
      .get(videos.all)
      .post(auth.requiresLogin,  videos.create);
  app.route('/api/videos/:videoId')
      .get(auth.isMongoId, videos.show)
      .put(auth.isMongoId, auth.requiresLogin, hasAuthorization, videos.update)
      .delete(auth.isMongoId, auth.requiresLogin, hasAuthorization, videos.destroy);
  app.route('/api/videos/view/random').get(videos.getRandom);
  app.route('/api/videos/upload')
      .post(videos.upload);
  app.route('/api/videos/create/youtube')
      .post(auth.requiresLogin, videos.youtube);
  app.route('/api/videos/find-user-videos/get')
      .get(videos.findUserVideos);

  app.route('/api/videos/trending/get')
      .get(videos.trending);

  app.route('/api/videos/category/create')
      .get(videos.createCategory);
  app.route('/api/category/get')
      .get(videos.getAllCategories);
  app.route('/api/category')
      .get(videos.getAllCategories);
  
  app.route('/api/videos/vote/post')
      .post(auth.requiresLogin, videos.vote);
  app.route('/api/videos/report/post')
      .post(auth.requiresLogin, report.create);

  app.route('/api/search/:keyWord')
      .get(search.search);
  app.route('/api/keyword/:keyWord')
      .get(search.find);
  
  app.route('/api/trending/mainpage')
      .get(videos.getTrendingForMainPage);
  app.route('/api/feed/subscriptions')
      .get(videos.getSubscriptionsVideos);
  app.route('/api/feed/home').get(videos.getVideoByChannel);
  app.route('/api/feed/loadmorechannel').post(videos.loadMore);


  app.route('/api/user-metadata/favorite/:videoId')
      .get(auth.isMongoId, auth.requiresLogin, userMetadata.addFavorite)
      .delete(auth.isMongoId, auth.requiresLogin, hasAuthorization, userMetadata.deleteFavorite);

  app.route('/api/videos/top-by-category/get')
      .get(function (req, res) {
        var  mongoose = require('mongoose'),
              Video = mongoose.model('Video'),
              Category = mongoose.model('Category');
        console.log('start');
          var data = [];
          var count = 0;
        Category.find({}, function (err, cate) {
          // console.log(cate);
          cate.forEach(function (c) {
            Video.find({categories: c}).sort('-view').limit(4).exec(function (err, vid) {
              if (err) {console.log(err.message)}
              if (vid.length > 0) {
                data.push({
                  category: c,
                  videos: vid
                });
              }
              // console.log(data);
              count++;
              if (count === cate.length) {
                res.json(data)
              }
            })
          })
        });

      });

  // Finish with setting up the videoId param
  app.param('videoId', videos.video);
};
