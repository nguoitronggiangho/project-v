var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin && !req.video.user._id.equals(req.user._id)) {
    return res.status(401).send('User is not authorized');
  }
  next();
};

module.exports = function(Videos, app, auth) {
  var videosv2 = require('../controllers/videosv2')(Videos);

  app.route('/api/v2/videos')
      .get(videosv2.all)
  //     .post(auth.requiresLogin, hasAuthorization, videosv2.create);
  app.route('/api/v2/videos/:videoId')
      .get(auth.isMongoId, videosv2.show)
      .put(auth.isMongoId, auth.requiresLogin, hasAuthorization, videosv2.update)
      .delete(auth.isMongoId, auth.requiresLogin, hasAuthorization, videosv2.destroy);

  // app.param('videoId', videos.video);
};