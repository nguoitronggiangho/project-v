'use strict';

/*
 * Defining the Package
 */
var Module = require('../../../core').Module;

var Videos = new Module('videos');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Videos.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Videos.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  // Videos.menus.add({
  //   title: 'Upload videos',
  //   link: 'create video',
  //   roles: ['authenticated'],
  //   menu: 'main'
  // });
  Videos.menus.add({
    'roles': ['all'],
    'title': 'ALL VIDEOS',
    'link': 'all videos'
  });

  Videos.angularDependencies([
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.poster',
    'com.2fdevs.videogular.plugins.buffering',
    'videogular.texttrack',
    'info.vietnamcode.nampnq.videogular.plugins.youtube',
    'angularFileUpload',
    'ngFileUpload',
    'djds4rce.angular-socialshare',
    'angular-loading-bar',
    'angularMoment',
    'angulartics.google.analytics',
    'angular.filter',
    'textAngular'
  ]);

  
  Videos.aggregateAsset('css', 'list.css');
  Videos.aggregateAsset('css', 'videos.css');
  Videos.aggregateAsset('css', 'view.css');
  Videos.aggregateAsset('css', 'edit.css');
  Videos.aggregateAsset('css', 'upload.css');
  Videos.aggregateAsset('css', 'home.css');
  Videos.aggregateAsset('css', 'trending.css');
  Videos.aggregateAsset('css', '../lib/video.js/dist/video-js.css');
  Videos.aggregateAsset('css', '../lib/angular-loading-bar/build/loading-bar.css');
  Videos.aggregateAsset('css', '../lib/angular-socialshare/angular-socialshare.min.css');
  Videos.aggregateAsset('js', 'videogular-subtitle.js');
  Videos.aggregateAsset('js', '../lib/video.js/dist/video.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/videogular/videogular.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/videogular-controls/vg-controls.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/videogular-overlay-play/vg-overlay-play.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/videogular-poster/vg-poster.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/videogular-buffering/vg-buffering.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/videogular-angulartics/vg-analytics.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/bower-videogular-youtube/youtube.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/angular-file-upload/dist/angular-file-upload.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/ng-file-upload/ng-file-upload.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/angular-socialshare/angular-socialshare.min.js', {global:true, weight:4, absolute:false});
  Videos.aggregateAsset('js', '../lib/angular-loading-bar/build/loading-bar.js', {global:true, weight:1, absolute:false});

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Videos.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Videos.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Videos.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Videos;
});
