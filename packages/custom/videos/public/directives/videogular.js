'use strict';

angular.module('mean.videos').directive('myReplayPlugin',
    [function() {
      return {
        restrict: 'E',
        require: '^videogular',
        //replace: true,
        scope: {
          voted: '=',
          yes: '&',
          no: '&'
        },
        template: "<div class='end-video-overlay' ng-if=\"API.isCompleted && API.currentState == 'stop'\">" +
                    "<div><span>" +
                      "<p>are you satisfied with the translation?</p>" +
                      "<p ng-hide='voted'><button class='btn btn-info' ng-click='yes()'>YES</button><button ng-click='no()' class='btn btn-default'>NO</button></p>" +
                      "<p ng-show='voted'>Thank you for your contribution!</p>" +
                      "<p ng-click=\"onClickReplay()\"><i class='fa fa-repeat'></i></p>" +
                    "</span></div>" +
                  "</div>",
        link: function(scope, elem, attrs, API) {
          scope.API = API;

          scope.onClickReplay = function() {
            API.play();
          };
        }
      }
    }
    ]);

angular.module('mean.videos').directive('myStopButton',
    function() {
      return {
        restrict: 'E',
        require: '^videogular',
        template: '<div class="iconButton" ng-click="test()" style="outline: none;border: none;cursor: default;"></div>',
        link: function(scope, elem, attrs, API) {
          scope.API = API;

          scope.test = function() {
            $('.caption-area').toggle(300)
          };
        }
      }
    }
);

angular.module('mean.videos').directive('mainCaption',
    function () {
      return {
        restrict: 'E',
        require: '^videogular',
        template: '<div></div>'
      }
});

angular.module('mean.videos').directive('topCaption',
    function () {
      return {
        restrict: 'E',
        require: '^videogular',
        replace: true,
        scope: {
          fuck: '&',
          captions: '=topCaption',
          display: '@',
          display2: '@',
          plus: '&',
          minus: '&'
        },
        templateUrl: '/videos/directives/top-area.html',
        link: function (scope, elem, attrs, API) {
          scope.dropdownClick = function () {
            API.pause();
            scope.custom = null;
          };

          scope.fuck = function (text) {
            //console.log($('#videoElm').get(0).currentTime);
          };

          scope.showTopCaption = function () {

          };
          var tourWrapper = $('.cd-tour-wrapper'),
              tourSteps = tourWrapper.children('li'),
              stepsNumber = tourSteps.length,
              coverLayer = $('.cd-cover-layer'),
              tourStepInfo = $('.cd-more-info'),
              tourTrigger = $('.cd-tour-trigger');

          scope.tutClick = function() {
            
            if(!tourWrapper.hasClass('active')) {
              //in that case, the tour has not been started yet
              tourWrapper.addClass('active');
              showStep(tourSteps.eq(0), coverLayer);
            }
          };

          function showStep(step, layer) {
            step.addClass('is-selected').removeClass('move-left');
            smoothScroll(step.children('.cd-more-info'));
            showLayer(layer);
          }

          function smoothScroll(element) {
            (element.offset().top < $(window).scrollTop()) && $('body,html').animate({'scrollTop': element.offset().top}, 100);
            (element.offset().top + element.height() > $(window).scrollTop() + $(window).height() ) && $('body,html').animate({'scrollTop': element.offset().top + element.height() - $(window).height()}, 100);
          }

          function showLayer(layer) {
            layer.addClass('is-visible').on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
              layer.removeClass('is-visible');
            });
          }

          function changeStep(steps, layer, bool) {
            var visibleStep = steps.filter('.is-selected'),
                delay = (viewportSize() == 'desktop') ? 300: 0;
            visibleStep.removeClass('is-selected');

            (bool == 'next') && visibleStep.addClass('move-left');

            setTimeout(function(){
              ( bool == 'next' )
                  ? showStep(visibleStep.next(), layer)
                  : showStep(visibleStep.prev(), layer);
            }, delay);
          }

          function closeTour(steps, wrapper, layer) {
            steps.removeClass('is-selected move-left');
            wrapper.removeClass('active');
            layer.removeClass('is-visible');
          }

          function viewportSize() {
            /* retrieve the content value of .cd-main::before to check the actua mq */
            return window.getComputedStyle(document.querySelector('.cd-tour-wrapper'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
          }
          //scope.plusVote = function (index) {
          //  console.log('vote +' + index)
          //};
          //
          //scope.minusVote = function (index) {
          //  console.log('vote -' + index)
          //}

        }
      }
    }
);

angular.module('mean.videos').directive('captionCtrl',
    function() {
      return {
        restrict: 'E',
        require: '^videogular',
        scope: {
          captions: '=captionsList',
          selected: '&'
        },
        template: '<span class="glyphicon glyphicon-subtitles" style="font-size: 14px;"></span>' + // Still using Glyphicons Bootstrap icon
        '<ul ng-show="selectorVisibility" style="max-height: 250px;overflow-y:scroll;">' +
        '<li ng-repeat="track in captions" ng-click="selected({track:track});selectCaption()">{{track.language.language_description | capitalize}}</li>' +
        '<li ng-click="offCaption()">Off</li>' +
        '</ul>',

        controller: function ($scope) {
          this.test = 'test';
        },

        link: function(scope, elem, attrs, API) {
          scope.trackTag = angular.element(API.videoElement).find('track');

          scope.changeCaption = function (track) {
            if (track) {
              $('video').empty().append('<track kind="'+track.kind+'" src="'+track.src+'" srclang="'+track.srclang+'" default>')
            }
          };

          scope.offCaption = function () {
            $('.main-caption').hide();
          };

          scope.selectCaption = function() {
            $('.main-caption').show();
          };

          function onMouseOverCaptions() {
            scope.selectorVisibility = true;
            scope.$apply();
          }

          function onMouseLeaveCaptions() {
            scope.selectorVisibility = false;
            scope.$apply();
          }

          function onCaptionSourceChange(newValue, oldValue) {

            if (!oldValue || newValue !== oldValue) {
              elem.css('display', (newValue ? 'table-cell' : 'none'));
              scope.changeCaption();
            }
          }

          elem.bind('mouseover', onMouseOverCaptions);
          elem.bind('mouseleave', onMouseLeaveCaptions);
          scope.$watch('captions', onCaptionSourceChange);
        }
      }
    }
);


angular.module('mean.videos').directive('combineHorizontalScrolls', [function(){
  var scrollTop = 0;
  function combine(elements){
    elements.on('scroll', function(e){
      if(e.isTrigger){
        e.target.scrollTop = scrollTop;
      }else {
        scrollTop = e.target.scrollTop;
        elements.each(function (element) {
          if( !this.isSameNode(e.target) ){
            $(this).trigger('scroll');
          }
        });
      }
    });
  }

  return {
    restrict: 'A',
    replace: false,
    compile: function(element, attrs){
      combine(element.find('.'+attrs.combineHorizontalScrolls));
    }
  };
}]);

angular.module('mean.videos').directive('fileModel', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function(){
        scope.$apply(function(){
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);

angular.module('mean.videos').directive('vgSetting', [function () {
  return {
    restrict: 'E',
      template: '<span uib-popover-template="\'/videos/directives/setting.html\'" type="button" popover-elem><i class="fa fa-cog"></i></span>',

    link: function (scope, elem, attrs, API) {

    }
  }
}]);

angular.module('mean.videos').directive('popoverClose', function($timeout){
  return{
    scope: {
      excludeClass: '@'
    },
    link: function(scope, element, attrs) {
      var trigger = document.getElementsByClassName('trigger');

      function closeTrigger(i) {
        $timeout(function(){
          angular.element(trigger[0]).triggerHandler('click').removeClass('trigger');
        });
      }

      element.on('click', function(event){
        var etarget = angular.element(event.target);
        var tlength = trigger.length;
        if(!etarget.hasClass('trigger') && !etarget.hasClass(scope.excludeClass)) {
          for(var i=0; i<tlength; i++) {
            closeTrigger(i)
          }
        }
      });
    }
  };
});

angular.module('mean.videos').directive('popoverElem', function(){
  return{
    link: function(scope, element, attrs) {
      element.on('click', function(){
        element.addClass('trigger');
      });
    }
  };
});

angular.module('mean.videos').directive('myEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
          scope.$eval(attrs.myEnter);
        });

        event.preventDefault();
      }
    });
  };
});

angular.module('mean.videos').directive('loading',   ['$http' ,function ($http)
{
  return {
    restrict: 'A',
    link: function (scope, elm, attrs)
    {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, function (v)
      {
        if(v){
          elm.show();
        }else{
          elm.hide();
        }
      });
    }
  };

}]);

angular.module('mean.videos').directive('clickOut', ['$window', '$parse', function ($window, $parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var clickOutHandler = $parse(attrs.clickOut);

      angular.element($window).on('click', function (event) {
        if (element[0].contains(event.target)) return;
        clickOutHandler(scope, {$event: event});
        scope.$apply();
      });
    }
  };
}]);

angular.module('mean.videos').directive('setClassWhenAtTop', function ($window) {
  var $win = angular.element($window); // wrap window object as jQuery object

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
          offsetTop = element.offset().top; // get element's top relative to the document

      $win.on('scroll', function (e) {
        if ($win.scrollTop() >= offsetTop) {
          element.addClass(topClass);
        } else {
          element.removeClass(topClass);
        }
      });
    }
  };
})