angular
    .module('mean.videos')
    .directive('vgSetting2', vgSetting2);

function vgSetting2() {
  var directive = {
    restrict: 'EA',
    templateUrl: '/videos/directives/video-setting.html',
    scope: {
      max: '='
    },
    link: linkFunc,
    controller: VideoSettingController,
    // note: This would be 'VideoSettingController' (the exported controller name, as string)
    // if referring to a defined controller in its separate file.
    controllerAs: 'vm',
    bindToController: true // because the scope is isolated
  };

  return directive;

  function linkFunc(scope, el, attr, ctrl) {
    // console.log('LINK: scope.min = %s *** should be undefined', scope.min);
    // console.log('LINK: scope.max = %s *** should be undefined', scope.max);
    // console.log('LINK: scope.vm.min = %s', scope.vm.min);
    // console.log('LINK: scope.vm.max = %s', scope.vm.max);
  }
}

VideoSettingController.$inject = ['$scope', '$rootScope'];

function VideoSettingController($scope, $rootScope) {
  // Injecting $scope just for comparison
  var vm = this;

  vm.min = 3;
  vm.main = false;
  vm.qualitySetting = false;
  vm.availableQuality = $rootScope.quality;

  vm.toggleSettingPopup = function() {
    vm.main = !vm.main;
    vm.qualitySetting = false;
  };

  vm.clickOutside = function() {
    vm.main = false;
  };

  vm.selectSettingOption = function(e) {
    var id = $(e.target).data('id');
    console.log(e);
    vm.main = false;
    vm[id] = true;
  };

  vm.selectBack = function(e) {
    var id = $(e.target).data('id');
    vm.main = true;
    vm[id] = false;
  };

  vm.selectedQuality = function(quality) {
    $rootScope.newQuality = quality;
    // console.log(quality)
  };

  $rootScope.$watch('currentQuality', function(val) {
    vm.currentQuality = val;
  });
  // console.log('CTRL: $scope.vm.min = %s', $scope.vm.min);
  // console.log('CTRL: $scope.vm.max = %s', $scope.vm.max);
  // console.log('CTRL: vm.min = %s', vm.min);
  // console.log('CTRL: vm.max = %s', vm.max);
}