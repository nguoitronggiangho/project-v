(function() {
  'use strict';

  angular
      .module('mean.videos')
      .directive('videoList', videoList);

  function videoList() {
    var directive = {
      templateUrl: '/videos/directives/video-list.html',
      bindToController: true,
      controller: VideoListDirectiveController,
      controllerAs: 'vm',
      link: link,
      restrict: 'EA',
      scope: {
        videos: '='
      }
    };
    return directive;

    function link(scope, element, attrs) {
      
    }
  }

  VideoListDirectiveController.$inject = ['$scope'];

  /* @ngInject */
  function VideoListDirectiveController($scope) {
    var vm = this;
  }

})();

