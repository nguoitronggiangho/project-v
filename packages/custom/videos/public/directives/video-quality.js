angular.module('mean.videos').directive('vgQuality',
    function() {
      return {
        restrict: 'E',
        require: '^videogular',
        scope: {
          quality: '=qualityList',
          selected: '&',
          currentQuality: '='
        },
        templateUrl: '/videos/directives/video-quality.html',

        controller: function ($scope) {
          this.test = 'test';
        },

        link: function(scope, elem, attrs, API) {
          scope.trackTag = angular.element(API.videoElement).find('track');

          scope.changeCaption = function (track) {
            if (track) {
              $('video').empty().append('<track kind="'+track.kind+'" src="'+track.src+'" srclang="'+track.srclang+'" default>')
            }
          };

          scope.offCaption = function () {
            $('video').empty();
          };

          function onMouseOverCaptions() {
            scope.selectorVisibility = true;
            scope.$apply();
          }

          function onMouseLeaveCaptions() {
            scope.selectorVisibility = false;
            scope.$apply();
          }

          function onCaptionSourceChange(newValue, oldValue) {

            if (!oldValue || newValue !== oldValue) {
              elem.css('display', (newValue ? 'table-cell' : 'none'));
              scope.changeCaption();
            }
          }

          elem.bind('mouseover', onMouseOverCaptions);
          elem.bind('mouseleave', onMouseLeaveCaptions);
          scope.$watch('captions', onCaptionSourceChange);
        }
      }
    }
);
