'use strict';

/* jshint -W098 */
angular.module('mean.videos').config(['$viewPathProvider', 'cfpLoadingBarProvider',
  function($viewPathProvider, cfpLoadingBarProvider) {
    $viewPathProvider.override('system/views/index.html', 'videos/views/test.html');
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = false;
  }
])
//TODO: create redirect back after login (better login popup)
  .run(function ($location, $rootScope, $state, amMoment) {

    amMoment.changeLocale('vi');
  var postLogInRoute;

  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options) {

    //if login required and you're logged out, capture the current path
    //if (nextRoute.loginRequired && Account.loggedOut()) {
    //  postLogInRoute = $location.path();
    //  $location.path('/login').replace();
    //} else if (postLogInRoute && Account.loggedIn()) {
    //  //once logged in, redirect to the last route and reset it
    //  $location.path(postLogInRoute).replace();
    //  postLogInRoute = null;
    //}
    //console.log(toState);
    //console.log(toParams);
    //console.log(fromState);
    //alert('test');
    //$state.go(fromState.name, fromParams)
  });
});