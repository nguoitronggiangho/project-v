(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('ViewVideoController', ViewVideoController);

  ViewVideoController.$inject = ['$scope', 'Videos', '$mdToast'];

  /* @ngInject */
  function ViewVideoController($scope, Videos, $mdToast) {
    var vm = this;
    vm.title = 'ViewVideoController';

    activate();

    ////////////////

    function activate() {
      
    }

    this.getRandom = function() {
      var query = {
        limit: 20
      };
      Videos.getRandom(query, function(response) {
        vm.videos = response;
      })
    };
    
    this.addFavorite = function(videoId) {
      Videos.addFavorite({videoId: videoId}, function(response) {
        $mdToast.show(
            $mdToast.simple()
                .textContent('added to favorites')
                .position('top right')
                .hideDelay(3000)
        )
      })
    }
  }

})();

