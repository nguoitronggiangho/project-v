(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('TrendingController', TrendingController);

  TrendingController.$inject = ['$scope', '$mdMedia', 'Trending'];

  /* @ngInject */
  function TrendingController($scope, $mdMedia, Trending) {
    var vm = this;
    vm.title = 'TrendingController';

    activate();

    ////////////////

    function activate() {
      getTrendingVideo()
    }

    function getTrendingVideo() {
      Trending.mainPage()
          .then(function(response) {
            // console.log(response);
            vm.videos = response
          })
    }

    $scope.$watch(function() {
      if ($mdMedia('(min-width: 1563px)')) {
        $('html').addClass('content-snap-width-3');
        // $('.container').width(1262)
      } else if ($mdMedia('(min-width: 1357px) and (max-width: 1562px)')) {
        $('html').addClass('content-snap-width-2');
        // $('.container').width(1056)
      } else if ($mdMedia('(min-width: 911px) and (max-width: 1356px)')) {
        $('html').addClass('content-snap-width-1');
        // $('.container').width(850)
      } else if ($mdMedia('(min-width: 705px) and (max-width: 910px)')) {
        $('.container').width(644)
      }
      return $mdMedia('(min-width: 1563px)');
    }, function(big) {
      // console.log(big);
      $scope.bigScreen = big;
    });
  }

})();

