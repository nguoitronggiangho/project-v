'use strict';

/* jshint -W098 */
angular.module('mean.videos').controller('VideosController', ['$scope', 'Global', 'Videos', '$stateParams', '$location', 'MeanUser', 'Circles', 'Captions', '$sce', 'FileUploader', 'Upload', '$timeout', '$mdConstant', '$rootScope', '$mdToast', 'Search', 'Channel', '$cookies',
  function ($scope, Global, Videos, $stateParams, $location, MeanUser, Circles, Captions, $sce, FileUploader, Upload, $timeout, $mdConstant, $rootScope, $mdToast, Search, Channel, $cookies) {
    $scope.global = Global;




    $scope.initTour = function () {
      var tourWrapper = $('.cd-tour-wrapper'),
          tourSteps = tourWrapper.children('li'),
          stepsNumber = tourSteps.length,
          coverLayer = $('.cd-cover-layer'),
          tourStepInfo = $('.cd-more-info'),
          tourTrigger = $('.cd-tour-trigger');
      var showTour = $cookies.get('showVideoPlayerTour');

      //create the navigation for each step of the tour
      createNavigation(tourSteps, stepsNumber);

      if (!showTour || showTour === true) {
        if(!tourWrapper.hasClass('active')) {
          //in that case, the tour has not been started yet
          tourWrapper.addClass('active');
          showStep(tourSteps.eq(0), coverLayer);
        }
      }

      tourTrigger.on('click', function(){
        //start tour
        if(!tourWrapper.hasClass('active')) {
          //in that case, the tour has not been started yet
          tourWrapper.addClass('active');
          showStep(tourSteps.eq(0), coverLayer);
        }
      });

      //change visible step
      tourStepInfo.on('click', '.cd-prev', function(event){
        //go to prev step - if available
        ( !$(event.target).hasClass('inactive') ) && changeStep(tourSteps, coverLayer, 'prev');
      });
      tourStepInfo.on('click', '.cd-next', function(event){
        //go to next step - if available
        ( !$(event.target).hasClass('inactive') ) && changeStep(tourSteps, coverLayer, 'next');
      });

      //close tour
      tourStepInfo.on('click', '.cd-close', function(event){
        closeTour(tourSteps, tourWrapper, coverLayer);
        $cookies.put('showVideoPlayerTour', false)
      });

      //detect swipe event on mobile - change visible step
      tourStepInfo.on('swiperight', function(event){
        //go to prev step - if available
        if( !$(this).find('.cd-prev').hasClass('inactive') && viewportSize() == 'mobile' ) changeStep(tourSteps, coverLayer, 'prev');
      });
      tourStepInfo.on('swipeleft', function(event){
        //go to next step - if available
        if( !$(this).find('.cd-next').hasClass('inactive') && viewportSize() == 'mobile' ) changeStep(tourSteps, coverLayer, 'next');
      });

      //keyboard navigation
      $(document).keyup(function(event){
        if( event.which=='37' && !tourSteps.filter('.is-selected').find('.cd-prev').hasClass('inactive') ) {
          changeStep(tourSteps, coverLayer, 'prev');
        } else if( event.which=='39' && !tourSteps.filter('.is-selected').find('.cd-next').hasClass('inactive') ) {
          changeStep(tourSteps, coverLayer, 'next');
        } else if( event.which=='27' ) {
          closeTour(tourSteps, tourWrapper, coverLayer);
        }
      });
    }

    function createNavigation(steps, n) {
      var tourNavigationHtml = '<div class="cd-nav"><span><b class="cd-actual-step">1</b> of '+n+'</span><ul class="cd-tour-nav"><li><a href="#0" class="cd-prev">&#171; Previous</a></li><li><a href="#0" class="cd-next">Next &#187;</a></li></ul></div><a href="#0" class="cd-close">Close</a>';

      steps.each(function(index){
        var step = $(this),
            stepNumber = index + 1,
            nextClass = ( stepNumber < n ) ? '' : 'inactive',
            prevClass = ( stepNumber == 1 ) ? 'inactive' : '';
        var nav = $(tourNavigationHtml).find('.cd-next').addClass(nextClass).end().find('.cd-prev').addClass(prevClass).end().find('.cd-actual-step').html(stepNumber).end().appendTo(step.children('.cd-more-info'));
      });
    }

    function showStep(step, layer) {
      step.addClass('is-selected').removeClass('move-left');
      smoothScroll(step.children('.cd-more-info'));
      showLayer(layer);
    }

    function smoothScroll(element) {
      (element.offset().top < $(window).scrollTop()) && $('body,html').animate({'scrollTop': element.offset().top}, 100);
      (element.offset().top + element.height() > $(window).scrollTop() + $(window).height() ) && $('body,html').animate({'scrollTop': element.offset().top + element.height() - $(window).height()}, 100);
    }

    function showLayer(layer) {
      layer.addClass('is-visible').on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
        layer.removeClass('is-visible');
      });
    }

    function changeStep(steps, layer, bool) {
      var visibleStep = steps.filter('.is-selected'),
          delay = (viewportSize() == 'desktop') ? 300: 0;
      visibleStep.removeClass('is-selected');

      (bool == 'next') && visibleStep.addClass('move-left');

      setTimeout(function(){
        ( bool == 'next' )
            ? showStep(visibleStep.next(), layer)
            : showStep(visibleStep.prev(), layer);
      }, delay);
    }

    function closeTour(steps, wrapper, layer) {
      steps.removeClass('is-selected move-left');
      wrapper.removeClass('active');
      layer.removeClass('is-visible');
    }

    function viewportSize() {
      /* retrieve the content value of .cd-main::before to check the actua mq */
      return window.getComputedStyle(document.querySelector('.cd-tour-wrapper'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
    }





    
    

    $rootScope.$watch('quality', function(old, newVal) {
      // console.log(old);
      // console.log(newVal);
      $scope.avaliableQuality = newVal;
      // console.log($scope.avaliableQuality)
    });

    $scope.selectedQuality = function(quality) {
      $rootScope.newQuality = quality;
      // console.log(quality)
    };

    $scope.hasAuthorization = function(video) {
      if (!video || !video.user) return false;
      console.log(MeanUser.isAdmin);
      console.log('assadsad');
      return MeanUser.isAdmin || video.user._id === MeanUser.user._id;
    };

    $scope.isLogedin = function () {
      if (!MeanUser.user._id) {
        alert('not logged in')
        return false
      }
      console.log(MeanUser.user);
      console.log('already in')
    };

    $scope.samples = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,20];

    $scope.uploader = new FileUploader();
    $scope.uploadFile = function () {
      var file = $scope.myFile;
      console.log(file);
      //console.dir(file);
    };
    $scope.uploadChange = function () {
      $scope.selectedFile = true
    };
    var self = this;
    this.currentTime = 0;
    this.totalTime = 0;
    this.state = null;
    this.volume = 1;
    this.isCompleted = false;
    this.API = null;
    this.seeking = {
      currentTime: 0,
      duration: 0
    };
    this.seeked = {
      currentTime: 0,
      duration: 0
    };

    this.onPlayerReady = function (API) {
      this.API = API;
      // $scope.logFull = function() {
      //   console.log(API);
      // }
    };

    //this.onError = function (event) {
    //  console.log('VIDEOGULAR ERROR EVENT');
    //  console.log(event);
    //};

    this.onCompleteVideo = function () {
      this.isCompleted = true;
    };

    this.onUpdateState = function (state) {
      this.state = state;
    };

    this.onUpdateTime = function (currentTime, totalTime) {
      this.currentTime = currentTime;
      this.totalTime = totalTime;
      $scope.isFullScreen = this.API.isFullScreen;
    };

    $scope.$watch('isFullScreen', function(val) {
      // console.log(val);
    });

    this.onSeeking = function (currentTime, duration) {
      this.seeking.currentTime = currentTime;
      this.seeking.duration = duration;
    };

    this.onSeeked = function (currentTime, duration) {
      this.seeked.currentTime = currentTime;
      this.seeked.duration = duration;
    };

    this.onUpdateVolume = function (newVol) {
      this.volume = newVol;
    };

    this.onUpdatePlayback = function (newSpeed) {
      this.API.playback = newSpeed;
    };

    this.onEnterAnotationCuePoint = function onEnterAnotationCuePoint(currentTime, timeLapse, params) {
      console.log("entering in anotation!");
      // params.prop[params.value] = "0" + params.units;
      // params.show = true;
    };

    this.onLeaveAnotationCuePoint = function onLeaveAnotationCuePoint(currentTime, timeLapse, params) {
      // params.prop[params.value] = "0" + params.units;
      // params.show = false;
    };

    this.onUpdateAnotationCuePoint = function onUpdateAnotationCuePoint(currentTime, timeLapse, params) {
      var percent = (currentTime - timeLapse.start) * 100 / (timeLapse.end - timeLapse.start);
      var value = params.final * percent / 100;
      params.show = true;

      // params.prop[params.value] = value + params.units;
    };

    this.onCompleteAnotationCuePoint = function onCompleteAnotationCuePoint(currentTime, timeLapse, params) {
      // params.prop[params.value] = params.final + params.units;
      params.show = false;
    };


    this.media = [
      {
        sources: [

          {
            src: $sce.trustAsResourceUrl('/videos/assets/video/oceans.mp4'),
            type: 'video/mp4'
          },
          {
            src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogular.ogg'),
            type: 'video/ogg'
          },
          {
            src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogular.webm'),
            type: 'video/webm'
          }
        ]
      },
      {
        sources: [
          {
            src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov'),
            type: 'video/mp4'
          },
          {
            src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/big_buck_bunny_720p_stereo.ogg'),
            type: 'video/ogg'
          }
        ]
      }
    ];

    this.config = {
      width:100,
      responsive:false,
      playsInline: false,
      nativeFullscreen: true,
      autoHide: true,
      autoHideTime: 3000,
      autoPlay: false,
      sources: this.media[0].sources,
      tracks: this.media[0].tracks,
      loop: false,
      preload: 'auto',
      controls: false,
      // theme: {
      //   url: '/videos/assets/lib/videogular-themes-default/videogular.css'
      // },
      theme: {
        url: '/videos/assets/css/videogular.css'
      },
      cuePoints: {
        anotation: [
          // {
          //   timeLapse: {
          //     start: 3,
          //     end: 8
          //   },
          //   onEnter: this.onEnterAnotationCuePoint.bind(this),
          //   onLeave: this.onLeaveAnotationCuePoint.bind(this),
          //   onUpdate: this.onUpdateAnotationCuePoint.bind(this),
          //   onComplete: this.onCompleteAnotationCuePoint.bind(this),
          //   params: {
          //     final: 500,
          //     // prop: this.barChartStyle,
          //     value: "width",
          //     units: "px",
          //     text: 'anotation 1',
          //     show: false
          //   }
          // },
          // {
          //   timeLapse: {
          //     start: 12,
          //     end: 17
          //   },
          //   onEnter: this.onEnterAnotationCuePoint.bind(this),
          //   onLeave: this.onLeaveAnotationCuePoint.bind(this),
          //   onUpdate: this.onUpdateAnotationCuePoint.bind(this),
          //   onComplete: this.onCompleteAnotationCuePoint.bind(this),
          //   params: {
          //     final: 1,
          //     // prop: this.textStyle,
          //     value: "opacity",
          //     units: "",
          //     text: 'anotation 2',
          //     show: false
          //   }
          // }
        ]
      },
      plugins: {
        poster: {
          url: '/videos/assets/img/Payphone-Maroon-5.jpg'
        },
        ads: {
          companion: 'companionAd',
          companionSize: [728, 90],
          network: '6062',
          unitPath: 'iab_vast_samples',
          adTagUrl: 'http://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=%2F3510761%2FadRulesSampleTags&ciu_szs=160x600%2C300x250%2C728x90&cust_params=adrule%3Dpremidpostpodandbumpers&impl=s&gdfp_req=1&env=vp&ad_rule=1&vid=47570401&cmsid=481&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]',
          skipButton: '<div class="skipButton">skip ad</div>'
        },
        analytics: {
          category: 'Videogular',
          label: 'Main',
          events: {
            ready: true,
            play: true,
            pause: true,
            stop: true,
            complete: true,
            progress: 10
          }
        }
      }
    };

    this.changeSource = function () {
      this.config.sources = this.media[1].sources;
      this.config.tracks = undefined;
      this.config.loop = false;
      this.config.preload = false;
    };

    this.changeTrack = function () {
      this.media[0].tracks[0].default = null;
      this.media[0].tracks[1].default = 'default';
    };

    this.wrongSource = function () {
      this.config.sources = [
        {
          src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogula.mp4'),
          type: 'video/mp4'
        },
        {
          src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogula.webm'),
          type: 'video/webm'
        },
        {
          src: $sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogula.ogg'),
          type: 'video/ogg'
        }
      ];
      this.config.tracks = undefined;
      this.config.loop = false;
      this.config.preload = true;
    };

    this.changeTrackInfo = function () {
      this.config.plugins.analytics.category = 'VG';
      this.config.plugins.analytics.label = 'main.html';
    };


    $scope.hasAuthorization = function (video) {
      if (!video || !video.user) return false;
      return MeanUser.isAdmin || video.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];

    Circles.mine(function (acl) {
      $scope.availableCircles = acl.allowed;
      $scope.allDescendants = acl.descendants;
    });

    $scope.selectVideo = function($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
      //console.log($newFiles);
      $scope.videoSelected = true;
      if ($newFiles) {
        $scope.videoInfo = $newFiles[0];
        $scope.title = $newFiles[0].name;
      }
    };

    $scope.getAllCategories = function () {
      Videos.getAllCategories(function (categories) {
        $scope.categories = categories
      })
    };
    
    $scope.getChannelByUser = function() {
      Channel.getChannelByUser().then(function(channels) {
        // console.log(channels);
        return $scope.channels = channels
      })
    };

    //upload video form (step1)
    self.keys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA];
    self.tags = [];
    self.selectedCategories = [];
    self.description = '';
    self.selectedChannel = null;
    $scope.uploadPic = function(file) {

      if (file && $scope.title) {
        $scope.submit = true;
        $scope.fileSize = file.size/(1024*1024);
        file.upload = Upload.upload({
          url: '/api/videos/upload',
          data: {file: file, title: $scope.title},
          method: 'POST',
          file: file
        });

        file.upload.then(function (response) {
          if (response.status === 200) {
            $scope.uploadSuccess = true;
            $scope.video = response.data;
            $scope.videoId = response.data._id;
          }
          $timeout(function () {
            file.result = response.data;
          });
        }, function (response) {
          if (response.status > 0)
            $scope.errorMsg = response.status + ': ' + response.data.message;
        }, function (evt) {
          // Math.min is to fix IE which reports 200% sometimes
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      } else if ($scope.youtube && $scope.title && $scope.youtube.length > 32) {
        var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
        var videoId = $scope.youtube.match(myregexp)[1];
        if (!videoId) {
          alet('are you dump');
        }
        // var url = 'https://www.youtube.com/watch?v='+videoId;
        // if (videoId) {
        //   $scope.thumbnail = 'http://i.ytimg.com/vi/'+videoId+'/mqdefault.jpg'
        // }

        // Videos.youtube({
        //   title: $scope.title,
        //   youtubeId: videoId,
        //   // url: url,
        //   // thumbnail: $scope.thumbnail,
        //   tags: self.tags,
        //   categories: self.selectedCategories,
        //   description: self.description
        // }, function (response) {
        //   console.log('done');
        //   console.log(response);
        //   self.createErrors = response;
        //   $scope.uploadSuccess = true;
        //   $scope.video = response;
        //   $scope.videoId = response._id;
        // });
        Search.createYoutube({
            title: $scope.title,
            youtubeId: videoId,
            // url: url,
            // thumbnail: $scope.thumbnail,
            tags: self.tags,
            categories: self.selectedCategories,
            description: self.description,
            channel: self.selectedChannel
          }).then(function(response) {
          if (response.status === 200) {
            // console.log(response.data);
            $scope.uploadSuccess = true;
            $scope.video = response.data;
            $scope.videoId = response.data._id;
          } else {
            self.createErrors = response.data;
          }
        });

      } else {
        alert('oop you forgot select a file or enter the youtube link, don\'t forget enter the title too')
      }
    };

    /*
     |--------------------------------------------------------------------------
     | $Subscribe button
     |--------------------------------------------------------------------------
     */
    $scope.handleSubscribe = function() {
      var data = $scope.video.channel;
      data.subscribe = 'subscribe';
      // data.moderator = null;
      var channelId = data._id;
      // console.log(data);
      Channel.channelResource.update({channelId: channelId}, data).$promise
          .then(function(response) {
            // console.log(response)
          }, function(err) {
            console.log(err)
          });
    };

    var subButton = document.querySelector('.subscribe-button');
    var subbedClass = 'subbed';

    $(subButton).on('click', function(e) {
      toggleSubbed();
      e.preventDefault();
    });

    function toggleSubbed() {
      var text;
      var count = subButton.dataset.count;
      // console.log(count);

      if (subButton.classList.contains(subbedClass)) {
        subButton.classList.remove(subbedClass);
        text = 'Subscribe';
        count--;
      } else {
        subButton.classList.add(subbedClass);
        text = 'Subscribed';
        count++;
      }

      subButton.querySelector('.subscribe-text').innerHTML = text;
      subButton.dataset.count = count;
    }

    if ('alert' == '') {
      window.setInterval(toggleSubbed, 1000);
    }

    /*
     |--------------------------------------------------------------------------
     | Report form submit
     |--------------------------------------------------------------------------
     */
    $scope.reportSubmit = function(isValid) {
      if (isValid) {
        var data = {};
        data.video = $scope.video;
        data.content = self.report.content;

        Videos.report(data, function(response) {
          $mdToast.show(
              $mdToast.simple()
                  .textContent('Gửi báo cáo thành công')
                  .position('top right')
                  .hideDelay(3000)
          );
        }, function(err) {
          $mdToast.show(
              $mdToast.simple()
                  .textContent('Gửi báo cáo thất bại, vui lòng thử lại')
                  .position('top right')
                  .hideDelay(3000)
          );
        })
        self.report.content = '';
      }
    };

    $scope.create = function (isValid) {
      console.log($scope.video);
      console.log(isValid);
      if (isValid) {
        //$scope.video.file = $scope.myFile;
        var video = new Videos($scope.video);

        video.$save(function (response) {
          $location.path('videos' / +response._id);
        });

        $scope.video = {};
      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function (video) {
      if (video) {
        video.$remove(function (response) {
          for (var i in $scope.videos) {
            if ($scope.videos[i] == video) {
              $scope.videos.splice(i, 1)
            }
          }
          $location.path('videos');
        });
      } else {
        $scope.video.$remove(function (response) {
          $location.path('videos')
        })
      }
    };

    $scope.update = function (isValid) {
      if (isValid) {
        var video = $scope.video;
        if (!video.updated) {
          video.updated = [];
        }
        video.updated.push(new Date().getTime());

        video.$update(function () {
          $location.path('videos/' + video._id)
        })
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function () {
      Videos.query({limit: 20}, function (videos) {
        $scope.videos = videos;
      })
    };

    $scope.findOne = function () {
      Videos.get({
        videoId: $stateParams.videoId
      }, function (video) {
        // console.log(video);
        if (video.channel.subscribeState === true) {
          var subButton = document.querySelector('.subscribe-button');
          var subbedClass = 'subbed';
          subButton.classList.add(subbedClass);
          subButton.querySelector('.subscribe-text').innerHTML = 'Subscibed';
        }

        $scope.video = video;
        $scope.source = [{
          src: video.url,
          type: 'video/mp4'
        }]
      })
    };

    $scope.findVideoCaption = function () {
      Captions.findVideoCaption({
        videoId: $stateParams.videoId
      }, function (captionCollection) {
        $scope.captionCollection = captionCollection;
        // console.log(captionCollection);
      })
    };

    $scope.getAllLanguage = function () {
      Captions.findLanguage(function (language) {
        $scope.languages = language;
        //for (var i=0; i<$scope.video.lockedLanguage.length; i++){
        //  console.log($scope.video.lockedLanguage[i]);
        //  if ($scope.video.lockedLanguage[i]) {
        //
        //  }
        //}
      })
    };

    $scope.selectedTrack = function (track) {
      console.log(track);
    };

    $scope.currentURL = $location.absUrl().split('?')[0];
    $('.button-share').click(function() {
      $('.share-area').slideToggle()
    });


    $scope.editLink ='/videos/' + $stateParams.videoId + '/edit';

    $scope.translator = function () {
      $scope.startTranslate = true;
      var data = {};
      data.videoId = $scope.videoId;
      data.captionId = $('#captionId').val();

      Captions.translate(data,function (response) {
        console.log(response);
        $scope.startTranslate = false;
        if (response.videoId) {
          $scope.translateDone = true;
          setInterval(function() {
            $location.path('/videos/'+ $scope.videoId + '/edit');
          }, 1000);
        }
      });
    };

    $scope.upload = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
      console.log($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event);
      $scope.fileSelected = true;
    };

    $scope.voteVideo = function (id) {
      var data = {
        videoId: id
      };
      Videos.vote(data, function (response) {
        // console.log(response);
        if (response.nModified === 0) {
          // alert('you already like this video')
          $mdToast.show(
              $mdToast.simple()
                  .textContent('you already like this video!')
                  .position('top right')
                  .hideDelay(3000)
          );
        } else {
          $scope.video.voteCount += 1;
        }
      })
    };

    function getVideoCaptions (videoId) {
      return Captions.getCaptionByLang({
        videoId: videoId
      }, function (data) {
        return data;
      });
    }

    this.viewInit = function () {
      var videoId = $stateParams.videoId;
      var captions = getVideoCaptions(videoId);
      $scope.captionSrc = captions;
      // console.log(captions);

      $scope.selectedTrack = function (track) {
        // console.log(track);
        var captionsArray = track.display;
        //$scope.selectedCaption = track;
        //return track;

        //save sentence created in the player
        $scope.customSave = function (text, time) {
          var second = self.currentTime;
          var index = FindCaptionIndex(second, track.display);
          var data = {};
          data.language = track.language;
          data.order = index + 1;
          data.text = text;
          data.captionId = track._id;
          data.videoId = $stateParams.videoId;
          data.timeStart = track.display[index].timeStart;
          data.timeEnd = track.display[index].timeEnd;
          // console.log(data);
          $('#player-input').val('');

          Captions.checkout(data, function (res) {
            // console.log(data);
            // console.log(res);
            if (res.message){
              $scope.message = res.message;
              // console.log(res.status);
              // console.log(res.message);
              $mdToast.show(
                  $mdToast.simple()
                      .textContent('Tạo thành công!')
                      .position('top right')
                      .hideDelay(3000)
              );
            } else {
              $scope.message = 'cannot save';
              $mdToast.show(
                  $mdToast.simple()
                      .textContent('Some thing wrong, cannot save!')
                      .position('top right')
                      .hideDelay(3000)
              );
            }
          });
        };

        $scope.plusVote = function (id, trackId) {
          console.log('voted sentence '+id);
          var data = {};
          data.captionId = trackId;
          data.sentenceId = id;
          Captions.vote(data, function (response) {
            console.log(response);
            $mdToast.show(
                $mdToast.simple()
                    .textContent(response.message)
                    .position('top right')
                    .hideDelay(3000)
            );
          })
        };

        $scope.noVoteEndOfVideo = function () {
          console.log('click no');
          $scope.endVideoVote = true;
        };

        $scope.updateTime = function(currentTime, totalTime) {

          if (autoPauseAtTime >= 0 && currentTime >= autoPauseAtTime) {
            autoPauseAtTime = -1;
            self.API.pause();
            return;
          }

          var captionsEndTime = existingCaptionsEndTime();
          //console.log(captionsEndTime);

          if (currentTime < captionsEndTime) {
            DisplayExistingCaption(currentTime);
            displayOtherCaption(currentTime);
          } else {
            $('.main-caption').text('');
            //$('.subcaption-first').text('');
            $('.dropdown-caption-area').text('');

            $('.subcaption-first').html('');
            $('.fa-eye').removeClass('eye-warning');
            $('#second-like').addClass('hide');

            if (captionBeingDisplayed != -1) {
              $('#textCaptionEntry').val('');
              captionBeingDisplayed = -1;
            }
          }
        };

        function DisplayExistingCaption(seconds) {
          var ci = FindCaptionIndex(seconds, captionsArray);
          if (ci != captionBeingDisplayed) {
            captionBeingDisplayed = ci;
            if (ci != -1) {
              var mainCaption = captionsArray[ci];
              $('.main-caption').html('<span>'+mainCaption.text+'</span>');
            } else {
              $('#captionTitle').html('&nbsp;');
              $('#textCaptionEntry').val('');
            }
          }
        }

        Object.size = function(obj) {
          var size = 0, key;
          for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
          }
          return size;
        };

        function displayOtherCaption (seconds) {
          //var index = [];
          $('.dropdown-caption-area').html('');
          $('.second-caption-area').html('');
          // $('.subcaption-first').html('');
          // $('.sencond-caption-vote').hide();
          // $('#second-rate').hide();
          $('.fa-eye').removeClass('eye-warning');

            var ci = FindCaptionIndex(seconds, captionsArray);

            if (ci != -1) {
              for (var i = 1; i<Object.size(track.sentence[ci+1]); i++) {
                var theCaption = track.sentence[ci+1][i];
                var template = '<span>'+theCaption.text+'</span>'+'<i ng-click="plus({voted:1})" data-sentenceid="'+theCaption._id+'" class="fa fa-thumbs-up" style="margin-left:10px;cursor: pointer"></i></span> <span class="third-rate" style="margin-left:5px;cursor: pointer">'+theCaption.rate+'</span>';
                $('.dropdown-caption-area').append("<p>"+template+"</p>");

                // $('.subcaption-first').html(theCaption.text);
                // $('.sencond-caption-vote').show();
                // $('#second-rate').show();
                $('.fa-eye').addClass('eye-warning');
                $('#second-like').removeClass('hide');
                $('#second-rate').text(theCaption.rate)
              }
              if (Object.size(track.sentence[ci+1]) > 1) {
                var secondCaption = track.sentence[ci+1][1];
                var template2 = '<span>'+secondCaption.text+'</span>'+'<i ng-click="plus({voted:1})" data-sentenceid="'+secondCaption._id+'" class="fa fa-thumbs-up" style="margin-left:10px;cursor: pointer"></i></span> <span class="third-rate" style="margin-left:5px;cursor: pointer">'+secondCaption.rate+'</span>';
                $('.second-caption-area').html("<span>"+template2+"</span>");
                if ($scope.isFullScreen) {
                  $('.second-caption-area').addClass('full-screen')
                } else {
                  $('.second-caption-area').removeClass('full-screen')
                }
                // $('.sencond-caption-vote').show();
                // $('#second-rate').show();
              }

            }
        }

        var captionBeingDisplayed = -1;

        function FindCaptionIndex(seconds, captionArr) {
          var below = -1;
          var above = captionArr.length;
          var i = Math.floor((below + above) / 2);

          while (below < i && i < above) {

            if (captionArr[i].timeStart <= seconds && seconds < captionArr[i].timeEnd)
              return i;

            if (seconds < captionArr[i].timeStart) {
              above = i;
            } else {
              below = i;
            }

            i = Math.floor((below + above) / 2);
          }

          return -1;
        }

        function existingCaptionsEndTime() {
          return captionsArray.length > 0 ? captionsArray[captionsArray.length - 1].timeEnd : 0;
        }

        $(document).on('click','.fa-thumbs-up', function () {
          var msg = $(this).data("sentenceid");
          //alert('click' + msg);
          $scope.plusVote(msg, track._id);
        });

      };

    };

    $scope.viewStart = function () {

      Captions.getCaptionByLang({
        videoId: $stateParams.videoId
      }, function (data) {
        $scope.captionSrc = data.captionSrc;
        //$scope.subCaption = data.subCaption;
        //console.log($scope.subCaption['VI'])
        $scope.selectedTrack = function (track) {
          $scope.subCaption = [];

          if (!jQuery.isEmptyObject(data.subCaption)) {
            //console.log(data.subCaption[track.srclang]);
            $scope.subCaption = data.subCaption[track.srclang];
          }
          $scope.mainCaption = track.sentence;

          //Create new verion when edit from player
          $scope.customSave = function (text, time) {
            var second = self.currentTime;
            var index = FindCaptionIndex(second, track.sentence);
            console.log(index);
            var sentence = track.sentence;
            sentence[index].text = text;
            var data = {};
            data.sentence = sentence;
            data.videoId = $stateParams.videoId;
            data.language = track.language;
            data.rate = track.caption.rate;

            Captions.checkout(data, function (res) {
              console.log(data);
              console.log(res);
              if (res.message){
                $scope.message = res.message;
                // alert('created')
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Created!')
                        .position('top right')
                        .hideDelay(3000)
                );
              } else {
                $scope.message = 'cannot save';
                // alert('cannot save')
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Something wrong, cannot save!')
                        .position('top right')
                        .hideDelay(3000)
                );
              }
            });
          };

          $scope.plusVote = function (voted) {
            console.log(voted);
            //$scope.isLogedin();
            var caption = $scope.subCaption[voted];
            caption.rate +=1;
            var id = caption._id;
            var a = Captions.get({
              captionId: id
            });
            if (!caption.updated) {
              caption.updated = [];
            }
            caption.updated.push(new Date().getTime());
            console.log(caption);
            Captions.update({captionId: id}, caption);
          };

          $scope.minusVote = function (voted) {
            console.log(voted);
          };

          $scope.plusVoteEndOfVideo = function () {
            console.log(track);
            console.log('click');
            var caption = track.caption;
            caption.rate += 1;
            var id = caption._id;
            if (!caption.updated) {
              caption.updated = [];
            }
            caption.updated.push(new Date().getTime());
            console.log(caption);
            Captions.update({captionId: id}, caption);
            $scope.endVideoVote = true;
          };

          $scope.noVoteEndOfVideo = function () {
            console.log('click no');
            $scope.endVideoVote = true;
          };

          var captionsArray = $scope.mainCaption;
          var captionsArray1 = [];
          var captionsArray2 = [];
          //console.log(track);
          //console.log($scope.subCaption);
          if ($scope.subCaption && $scope.subCaption.length >= 1) {
            //captionsArray1 = $scope.subCaption[0].sentence;
            var arr1 = $scope.subCaption[0].sentence;
            for (var i=0;i<arr1.length;i++) {
                if (arr1[i].text !== $scope.mainCaption[i].text) {
                  captionsArray1.push(arr1[i]);
                }
            }
          }
          if ($scope.subCaption && $scope.subCaption.length >= 2) {
            captionsArray2 = $scope.subCaption[1].sentence;
          }

          //var captionsArray = caption.captionArr;
          function FindCaptionIndex(seconds, captionArr) {
            var below = -1;
            var above = captionArr.length;
            var i = Math.floor((below + above) / 2);

            while (below < i && i < above) {

              if (captionArr[i].timeStart <= seconds && seconds < captionArr[i].timeEnd)
                return i;

              if (seconds < captionArr[i].timeStart) {
                above = i;
              } else {
                below = i;
              }

              i = Math.floor((below + above) / 2);
            }

            return -1;
          }

          var captionBeingDisplayed = -1;

          $(document).on('click','.fa-thumbs-up', function () {
            var msg = $(this).data("order");
            //alert('click' + msg);
            $scope.plusVote(msg);
          });

          function DisplayExistingCaption(seconds) {
            var ci = FindCaptionIndex(seconds, captionsArray);
            if (ci != captionBeingDisplayed) {
              captionBeingDisplayed = ci;
              if (ci != -1) {
                var mainCaption = captionsArray[ci];
                //var theCaption = captionsArray1[ci];
                //var theCaption2 = captionsArray2[ci];

                $('.main-caption').html(mainCaption.text);

                //if(captionsArray1.length > 0) {
                //  //$scope.display = theCaption.text;
                //  $('.subcaption-first').html(theCaption.text);
                //  $('.fa-eye').addClass('eye-warning');
                //}
                //if(captionsArray2.length >0){
                //  $scope.display2 = theCaption2.text;
                //}
              } else {
                $('#captionTitle').html('&nbsp;');
                $('#textCaptionEntry').val('');
              }
            }
          }

          function DisplaySecondCaption (seconds) {
            var ci = FindCaptionIndex(seconds, captionsArray1);

              if (ci != -1) {
                var theCaption = captionsArray1[ci];

                if(captionsArray1.length > 0) {
                  //$scope.display = theCaption.text;
                  $('.subcaption-first').html(theCaption.text);
                  $('.fa-eye').addClass('eye-warning');
                  $('#second-like').removeClass('hide');
                  $('#second-rate').text($scope.subCaption[0].rate)
                }
              }
          }

          function displayOtherCaption (seconds) {
            //var index = [];
            $('.dropdown-caption-area').html('');
            for (var i=1;i<$scope.subCaption.length;i++) {
              var ci = FindCaptionIndex(seconds, $scope.subCaption[i].sentence);

              if (ci != -1) {
                var theCaption = $scope.subCaption[i].sentence[ci];
                var template = '<span>'+theCaption.text+'</span>'+'<i ng-click="plus({voted:1})" data-order="'+i+'" class="fa fa-thumbs-up" style="margin-left:10px;cursor: pointer"></i></span> <span class="third-rate" style="margin-left:5px;cursor: pointer">'+$scope.subCaption[i].rate+'</span>'
                    ;
                $('.dropdown-caption-area').append("<p>"+template+"</p>")
              }
            }
          }

          $scope.updateTime = function(currentTime, totalTime) {
            //console.log(currentTime);
            //var playTime = $('#videoElm').prop('currentTime');

            if (autoPauseAtTime >= 0 && currentTime >= autoPauseAtTime) {
              autoPauseAtTime = -1;
              self.API.pause();
              return;
            }

            var captionsEndTime = existingCaptionsEndTime();
            var secondCaptionsEndTime = existingSecondCaptionsEndTime();

            if (currentTime < captionsEndTime) {
              DisplayExistingCaption(currentTime);
              displayOtherCaption(currentTime);
            } else {
              $('.main-caption').text('');
              //$('.subcaption-first').text('');
              $('.dropdown-caption-area').text('');
              $('#captionTitle').text('Pause to enter caption for segment from ' + FormatTime(captionsEndTime) + ' to ' + FormatTime(currentTime) + ':');
              if (captionBeingDisplayed != -1) {
                $('#textCaptionEntry').val('');
                captionBeingDisplayed = -1;
              }
            }

            if (currentTime < secondCaptionsEndTime) {
              DisplaySecondCaption(currentTime);
            } else {
              $('.subcaption-first').html('');
              $('.fa-eye').removeClass('eye-warning');
              $('#second-like').addClass('hide');
            }
          };

          function existingCaptionsEndTime() {
            return captionsArray.length > 0 ? captionsArray[captionsArray.length - 1].timeEnd : 0;
          }

          function existingSecondCaptionsEndTime() {
            return captionsArray1.length > 0 ? captionsArray1[captionsArray1.length - 1].timeEnd : 0;
          }

          $('#videoElm').bind({
            //play: videoPlayEventHandler,
            //timeupdate: videoTimeUpdateEventHandler,
            //pause: videoPauseEventHandler,
            //canplay: EnableDemoAfterLoadVideo,
            //loadeddata: EnableDemoAfterLoadVideo    // opera doesn't appear to fire canplay but does fire loadeddata
          });
        };
      });

    };

    $scope.init = function () {
      Videos.get({
        videoId: $stateParams.videoId
      }, function (video) {
        $scope.video = video;

        //Captions.video({
        //  videoId: $stateParams.videoId
        //}, function (caption) {
        //  $scope.caption = caption;
        //  console.log(caption);
        //
        //  //var playerInstance = jwplayer("myElement");
        //  //playerInstance.setup({
        //  //  playlist: [{
        //  //    file: video.url,
        //  //    //file: "/captions/assets/video/payphone.webm",
        //  //    // image: "/assets/sintel.jpg",
        //  //    tracks: [{
        //  //      file: caption.url,
        //  //      label: "English",
        //  //      kind: "captions",
        //  //      "default": true
        //  //    }]
        //  //  }]
        //  //});
        //  //playerInstance.on('ready', function () {
        //  //  $("video.jw-video").attr("id", "videoElm")
        //  //});
        //});

      });
      //  a little bit of browser sniffing
      if (!window.navigator.userAgent.match(/\bMSIE\b/)) {
        //	Only IE returns a file path we can load into the video element and then only when we're running locally on the intranet zone
        $(".browseForVideoFile").css("display", "none");
      } else {
        if (location.hostname.indexOf(".") != -1) {
          //	IE only returns a file path we can load into the video element when we're running locally on the intranet zone
          $(".browseForVideoFile").css("display", "none");
        }

        if (typeof document.documentMode == "undefined" || document.documentMode < 9) {
          //	we can't run in IE before 9
          $("#warning").css("display", "block");
        }
      }

      //  browsing for a caption file requires FileReader
      if (typeof window.FileReader == 'undefined') {
        $(".browseForCaptionFile").css("display", "none");
      }

      //	saving caption files locally requires msSaveOrOpenBlob
      if (typeof window.navigator.msSaveOrOpenBlob == 'undefined') {
        $("#blobBuilderSave, #saveToDiskAdvice").css("display", "none");
      }

      //	copying to the clipboard requires clipboardData
      if (typeof window.clipboardData == 'undefined') {
        $("#copyToClipboard").css("display", "none");
      }

      //  this disables and grays out the demo portions except for loading a video
      function DisableDemoExceptLoadVideo() {
        $(".grayNoVideo a, .grayNoVideo button, .grayNoVideo input, .grayNoVideo textarea").prop("disabled", true);
        $(".grayNoVideo, .grayNoVideo a").css("color", "rgb(153,153,153)");
      }

      DisableDemoExceptLoadVideo();

      //  this enables the demo after a successful video load
      function EnableDemoAfterLoadVideo() {
        $(".grayNoVideo, .grayNoVideo a").removeAttr("style");
        $(".grayNoVideo a, .grayNoVideo button, .grayNoVideo input, .grayNoVideo textarea").prop("disabled", false);
        $("#pauseButton, #saveCaptionAndPlay, #justSaveCaption").prop("disabled", true); // these are still disabled
        $("#textCaptionEntry").prop("readonly", true);
      }

      //  this resets our state on a video load
      function ResetState() {
        captionsArray.length = 0;
        autoPauseAtTime = -1;

        $("#videoElm").each(function () {
          $(this).get(0).pause();
          $(this).attr("src", "");
        });

        $("#display div").remove();
        $("textarea, #ttURL, #ttFile").val("");
        $("#captionFormatNone").prop("checked", true);
        $("#textCaptionEntry").val("").prop("readonly", true).removeClass("playing");
        $("#captionTitle").html("&nbsp;");
        DisableDemoExceptLoadVideo();
      }

      ResetState();

      $("#clearAllCaptions").click(function () {
        captionsArray.length = 0;
        autoPauseAtTime = -1;
        $("#display div").remove();
        $("#captionTitle").html("&nbsp;");
        $("#textCaptionEntry").val("");
        $("#captionFormatNone").click();
      });

      //  keep track of whether we've attached an error handler to the video element
      //  we don't do this during initialization because it immediately throws an error because we have no source
      var errorHandlerBound = false;

      //  sets the video source to the URL s
      function LoadVideoSource(s) {
        ResetState();

        if (!errorHandlerBound) {
          $("#videoElm").bind("error", function (event) {
            var vh = $(this).height();

            // error handling straight from the HTML5 video spec (http://dev.w3.org/html5/spec-author-view/video.html)
            switch (event.originalEvent.target.error.code) {
              case event.originalEvent.target.error.MEDIA_ERR_ABORTED:
                $("#videoError").text("You aborted the video playback.");
                break;
              case event.originalEvent.target.error.MEDIA_ERR_NETWORK:
                $("#videoError").text("A network error caused the video download to fail part-way.");
                break;
              case event.originalEvent.target.error.MEDIA_ERR_DECODE:
                $("#videoError").text("The video playback was aborted due to a corruption problem or because the video used features your browser did not support.");
                break;
              case event.originalEvent.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                $("#videoError").text("The video could not be loaded, either because the server or network failed or because the format is not supported.");
                break;
              default:
                $("#videoError").text("An unknown error occurred.");
                break;
            }

            $("#videoError").height(vh).css("display", "block");
            $(this).css("display", "none");
          });

          errorHandlerBound = true;
        }

        s = Trim(s);
        if (s != "") {
          $("#videoError").css("display", "none");
          $("#videoElm").css("display", "block").attr("src", s).prop("controls", true).get(0).load();
        }
      }

      //  when a video file is loaded, jack it straight to the video element as its src
      //  this is IE-specific so jQuery doesn't recognize the event with "bind"
      $("#videoFile").get(0).addEventListener("change", function () {
        $("#videoURL").val(""); // clear any value that may be in the video URL edit box
        LoadVideoSource($("#videoFile").val());
      }, false);

      $("#loadVideoFromUrl").click(function () {
        LoadVideoSource($("#videoURL").val());
      });

      $("#loadSampleVideo").click(function () {
        var url = null;
        var vid = $("#videoElm").get(0);
        if (vid.canPlayType("video/mp4")) {
          url = "./captions/assets/video/payphone.mp4";
        } else if (vid.canPlayType("video/webm")) {
          url = "./captions/assets/video/payphone.webm";
        }

        if (url) {
          $("#videoURL").val(url);
          $("#loadVideoFromUrl").click();
        }
      });

      function LoadCaptionFile(fileObject) {
        if (window.FileReader) {
          var reader = new window.FileReader();

          reader.onload = function () {
            ProcessProxyVttResponse({status: "success", response: reader.result});
          };

          reader.onerror = function (evt) {
            alert("Error reading caption file. Code = " + evt.code);
          };

          try {
            reader.readAsText(fileObject);
          } catch (exc) {
            alert("Exception thrown reading caption file. Code = " + exc.code);
          }
        } else {
          alert("Your browser does not support FileReader.");
        }
      }

      //  this button should be disabled when window.FileReader is undefined
      //  we expect browsers that support FileReader to have a files collection on the input[type=file] control
      $("#ttFile").get(0).addEventListener("change", function () {
        var filesObject = $("#ttFile").get(0).files;
        if (filesObject && filesObject.length > 0) {
          LoadCaptionFile(filesObject[0]);
          console.log(filesObject);
        } else {
          alert("No files collection on input[type=file] control.");
        }
      }, false);

      $("#loadCaptionFileFromUrl").click(function () {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://localhost:3000/captions/assets/video/captions-en.vtt', true);

        // Hack to pass bytes through unprocessed.
        xhr.overrideMimeType('text/plain; charset=UTF-8');

        xhr.onreadystatechange = function (e) {
          if (this.readyState == 4 && this.status == 200) {
            var binStr = this.responseText;
            ProcessProxyVttResponse({status: "success", response: binStr});
          }
        };

        xhr.send();
        //	insert a script request that returns a function invocation and a simple json object
        var s = document.createElement("script");
        s.setAttribute("type", "text/javascript");
        s.setAttribute("src", $("#ttURL").val());
        document.body.appendChild(s);

        //	this would work except for the cross-domain restrictions
        //	$.ajax({
        //	url: $("#ttURL").val(),
        //	success: function (data) {
        //	ProcessProxyVttResponse({ status: "success", response: data });
        //	}
        //	});
      });

      $("#loadSampleCaptionFile").click(function () {
        // data = $("#ttURL").val(window.location.origin + "/captions-en.vtt");
        $.get("/captions/assets/video/entrack.vtt", function (data) {
          ProcessProxyVttResponse({status: "success", response: data});
        });
        $("#videoElm").append('<track src="/captions/assets/video/entrack.vtt" kind="captions" srclang="en" label="English" default>');
        // $("#loadCaptionFileFromUrl").click();
      });

      //  index into captionsArray of the caption being displayed. -1 if none.
      var captionBeingDisplayed = -1;

      function DisplayExistingCaption(seconds) {
        var ci = FindCaptionIndex(seconds);
        if (ci != captionBeingDisplayed) {
          captionBeingDisplayed = ci;
          if (ci != -1) {
            var theCaption = captionsArray[ci];
            $("#captionTitle").text("Caption for segment from " + FormatTime(theCaption.start) + " to " + FormatTime(theCaption.end) + ":");
            $("#textCaptionEntry").val(theCaption.caption);
          } else {
            $("#captionTitle").html("&nbsp;");
            $("#textCaptionEntry").val("");
          }
        }
      }

      function existingCaptionsEndTime() {
        return captionsArray.length > 0 ? captionsArray[captionsArray.length - 1].end : 0;
      }

      function videoPlayEventHandler() {
        captionBeingDisplayed = -1;

        //  give Opera a beat before doing this
        window.setTimeout(function () {
          $("#textCaptionEntry").val("").prop("readonly", true).addClass("playing");
          $("#pauseButton").prop("disabled", false);
          $("#playButton, #justSaveCaption, #saveCaptionAndPlay").prop("disabled", true);
        }, 16);
      }

      function videoPauseEventHandler() {
        $("#playButton, #justSaveCaption, #saveCaptionAndPlay").prop("disabled", false);
        $("#textCaptionEntry").removeClass("playing").prop("readonly", false);
        $("#pauseButton").prop("disabled", true);

        var playTime = $("#videoElm").prop("currentTime");
        var captionsEndTime = existingCaptionsEndTime();
        if (playTime - 1 < captionsEndTime) {
          var ci = FindCaptionIndex(playTime - 1);
          if (ci != -1) {
            var theCaption = captionsArray[ci];
            $("#captionTitle").text("Edit caption for segment from " + FormatTime(theCaption.start) + " to " + FormatTime(theCaption.end) + ":");
            $("#textCaptionEntry").val(theCaption.caption);
            captionBeingDisplayed = ci;
          } else {
            $("#captionTitle").text("No caption at this time code.");
            $("#textCaptionEntry").val("");
            captionBeingDisplayed = -1;
          }
        } else {
          $("#captionTitle").text("Enter caption for segment from " + FormatTime(existingCaptionsEndTime()) + " to " + FormatTime(playTime) + ":");
          $("#textCaptionEntry").val("");
          captionBeingDisplayed = -1;
        }

        $("#textCaptionEntry").focus().get(0).setSelectionRange(1000, 1000); // set focus and selection point to end
      }

      function videoTimeUpdateEventHandler() {
        var playTime = $("#videoElm").prop("currentTime");

        if (autoPauseAtTime >= 0 && playTime >= autoPauseAtTime) {
          autoPauseAtTime = -1;
          $("#videoElm").get(0).pause();
          return;
        }

        var captionsEndTime = existingCaptionsEndTime();
        if (playTime < captionsEndTime) {
          DisplayExistingCaption(playTime);
        } else {
          $("#captionTitle").text("Pause to enter caption for segment from " + FormatTime(captionsEndTime) + " to " + FormatTime(playTime) + ":");
          if (captionBeingDisplayed != -1) {
            $("#textCaptionEntry").val("");
            captionBeingDisplayed = -1;
          }
        }
      }

      //  the video element's event handlers
      $("#videoElm").bind({
        play: videoPlayEventHandler,
        timeupdate: videoTimeUpdateEventHandler,
        pause: videoPauseEventHandler,
        canplay: EnableDemoAfterLoadVideo,
        loadeddata: EnableDemoAfterLoadVideo    // opera doesn't appear to fire canplay but does fire loadeddata
      });

      $("#playButton").click(function () {
        $("#videoElm").get(0).play();
      });

      $("#pauseButton").click(function () {
        $("#videoElm").get(0).pause();
      });

      function SaveCurrentCaption() {
        var playTime = $("#videoElm").prop("currentTime");
        var captionsEndTime = existingCaptionsEndTime();
        if (playTime - 1 < captionsEndTime) {
          var ci = FindCaptionIndex(playTime - 1);
          if (ci != -1) {
            UpdateCaption(ci, $("#textCaptionEntry").val());
          }
        } else {
          AddCaption(captionsEndTime, playTime, $("#textCaptionEntry").val());
        }
      }

      $("#justSaveCaption").click(function () {
        SaveCurrentCaption();
      });

      $("#saveCaptionAndPlay").click(function () {
        SaveCurrentCaption();
        $("#videoElm").get(0).play();
      });

      function CaptionFileExtension() {
        return $("#captionFormatVTT").prop("checked") ? "vtt" : $("#captionFormatTTML").prop("checked") ? "ttml" : "";
      }

      function RefreshMarkupDisplay() {
        var videoName = $("#videoElm").attr("src");
        $("#markupVideoSrc").text(videoName.match(/[\/\\]([\w\-]+\.\w{3,4})$/)[1]);

        var ttName = $("#ttURL").val() || ($("#ttFile").get(0).files && $("#ttFile").get(0).files.length > 0 ? ("#ttFile").get(0).files[0].name : videoName);
        $("#markupCaptionSrc").text(ttName.match(/[\/\\]([\w\-]+\.)\w{3,4}$/)[1] + CaptionFileExtension());
      }

      $("#captionFormatTTML, #captionFormatVTT").click(function () {
        $("#captionFileAndMarkup").css("display", "block").get(0).scrollIntoView(false);
        RefreshCaptionFileDisplay();
        RefreshMarkupDisplay();
      });

      $("#captionFormatNone").click(function () {
        $("#captionFileAndMarkup").css("display", "none");
      });

      // function should never be called if window.MSBlobBuilder is undefined
      function SaveCaptionAsTextBlob(s) {
        var filename = $("#videoElm").attr("src");
        if (filename) {
          var bb = new window.MSBlobBuilder();
          bb.append(s.replace(/\r\n|\r|\n/g, "\r\n")); //	we normalize the line breaks to cr-lf for Notepad
          window.navigator.msSaveOrOpenBlob(bb.getBlob("text/plain"), filename.match(/[\/\\]([\w\-]+\.)\w{3,4}$/)[1] + CaptionFileExtension());
        }
      }

      // button is not enabled if window.MSBlobBuilder is undefined
      $("#blobBuilderSave").click(function () {
        UpdateCaptionFileSource();
        SaveCaptionAsTextBlob($("#captionFile").val());
      });

      // button is not enabled if window.clipboardData is undefined
      $("#copyToClipboard").click(function () {
        UpdateCaptionFileSource();
        window.clipboardData.clearData("Text");
        window.clipboardData.setData("Text", $("#captionFile").val().replace(/\r\n|\r|\n/g, "\r\n")); // we normalize the line breaks to cr-lf for Notepad
      });

    };

    function Trim(s) {
      return s.replace(/^\s+|\s+$/g, "");
    }

//	parses webvtt time string format into floating point seconds
    function ParseTime(sTime) {

      //  parse time formatted as hours:mm:ss.sss where hours are optional
      if (sTime) {
        var m = sTime.match(/^\s*(\d+)?:?(\d+):([\d\.]+)\s*$/);
        if (m != null) {
          return (m[1] ? parseFloat(m[1]) : 0) * 3600 + parseFloat(m[2]) * 60 + parseFloat(m[3]);
        } else {
          m = sTime.match(/^\s*(\d{2}):(\d{2}):(\d{2}):(\d{2})\s*$/);
          if (m != null) {
            var seconds = parseFloat(m[1]) * 3600 + parseFloat(m[2]) * 60 + parseFloat(m[3]) + parseFloat(m[4]) / 30;
            return seconds;
          }
        }
      }

      return 0;
    }

//	formats floating point seconds into the webvtt time string format
    function FormatTime(seconds) {
      var hh = Math.floor(seconds / (60 * 60));
      var mm = Math.floor(seconds / 60) % 60;
      var ss = seconds % 60;

      return (hh == 0 ? "" : (hh < 10 ? "0" : "") + hh.toString() + ":") + (mm < 10 ? "0" : "") + mm.toString() + ":" + (ss < 10 ? "0" : "") + ss.toFixed(3);
    }

//	our state
    var captionsArray = [];
    var autoPauseAtTime = -1;

    function FindCaptionIndexLinearSearch(seconds) {
      if (captionsArray.length < 1)
        return -1;

      //	linear search isn't optimal but it's safe
      for (var i = 0; i < captionsArray.length; ++i) {
        if (captionsArray[i].start <= seconds && seconds < captionsArray[i].end)
          return i;
      }

      return -1;
    }

    function FindCaptionIndex(seconds) {
      var below = -1;
      var above = captionsArray.length;
      var i = Math.floor((below + above) / 2);

      while (below < i && i < above) {

        if (captionsArray[i].start <= seconds && seconds < captionsArray[i].end)
          return i;

        if (seconds < captionsArray[i].start) {
          above = i;
        } else {
          below = i;
        }

        i = Math.floor((below + above) / 2);
      }

      return -1;
    }


    function PlayCaptionFromList(listRowId) {
      var captionsArrayIndex = parseInt(listRowId.match(/ci(\d+)/)[1]);

      var vid = $('#videoElm').get(0);
      // vid.pause();
      vid.currentTime = captionsArray[captionsArrayIndex].start;
      autoPauseAtTime = captionsArray[captionsArrayIndex].end;
      vid.play();
    }

    function DisplayVTTSource() {
      var s = "WEBVTT\r\n\r\n";

      for (var i = 0; i < captionsArray.length; ++i) {
        if (captionsArray[i].caption != "") {
          s += (FormatTime(captionsArray[i].start) + " --> " + FormatTime(captionsArray[i].end) + "\r\n");
          s += captionsArray[i].caption + "\r\n\r\n";
        }
      }

      $("#captionFile").val(s);
      $("#captionFileKind").text(".vtt");
    }

    function DisplayTTMLSource() {
      var s = '<?xml version="1.0" encoding="UTF-8"?>\r\n<tt xmlns="http://www.w3.org/ns/ttml" xml:lang="en" >\r\n  <body>\r\n    <div>\r\n';

      for (var i = 0; i < captionsArray.length; ++i) {
        if (captionsArray[i].caption != "") {
          s += '      <p begin="' + captionsArray[i].start.toFixed(3) + 's" end="' + captionsArray[i].end.toFixed(3) + 's">' + XMLEncode(captionsArray[i].caption).replace(/\r\n|\r|\n/g, "<br />") + "</p>\r\n";
        }
      }

      s += "    </div>\r\n  </body>\r\n</tt>\r\n";

      $("#captionFile").val(s);
      $("#captionFileKind").text(".ttml");
    }

    var captionFileSourceUpdateTimer = null;

    function UpdateCaptionFileSource() {
      captionFileSourceUpdateTimer = null;
      if ($("#captionFormatVTT").prop("checked"))
        DisplayVTTSource();
      else if ($("#captionFormatTTML").prop("checked"))
        DisplayTTMLSource();
    }

    function RefreshCaptionFileDisplay() {
      if ($("#captionFileAndMarkup").css("display") != "none") {
        if (captionFileSourceUpdateTimer === null)
          captionFileSourceUpdateTimer = window.setTimeout(UpdateCaptionFileSource, 16);
      }
    }

    function XMLEncode(s) {
      return s.replace(/\&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');   //.replace(/'/g, '&apos;').replace(/"/g, '&quot;');
    }

    function XMLDecode(s) {
      return s.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&apos;/g, "'").replace(/&quot;/g, '"').replace(/&amp;/g, '&');
    }

    function UpdateCaption(ci, captionText) {
      captionsArray[ci].caption = captionText;
      $("#ci" + ci.toString() + " span:last-child").html(XMLEncode(captionText).replace(/\r\n|\r|\n/g, "<br/>"));

      RefreshCaptionFileDisplay();
    }

    function AddCaptionListRow(ci) {
      var theId = "ci" + ci.toString();
      $("#display").append("<div id=\"" + theId + "\"><span>" + FormatTime(captionsArray[ci].start) + "</span><span>" + FormatTime(captionsArray[ci].end) + "</span><span>" + XMLEncode(captionsArray[ci].caption).replace(/\r\n|\r|\n/g, "<br/>") + "</span></div>");
      $("#" + theId).click(function () {
        PlayCaptionFromList($(this).attr("id"));
      });
    }

    function AddCaption(captionStart, captionEnd, captionText) {
      captionsArray.push({start: captionStart, end: captionEnd, caption: Trim(captionText)});
      AddCaptionListRow(captionsArray.length - 1);
      RefreshCaptionFileDisplay();
    }

    function SortAndDisplayCaptionList() {
      captionsArray.sort(function (a, b) {
        return a.start - b.start;
      });

      $("#display div").remove();
      for (var ci = 0; ci < captionsArray.length; ++ci) {
        AddCaptionListRow(ci);
      }

      RefreshCaptionFileDisplay();
    }


//-----------------------------------------------------------------------------------------------------------------------------------------
//	Partial parser for WebVTT files based on the spec at http://dev.w3.org/html5/webvtt/
//-----------------------------------------------------------------------------------------------------------------------------------------

    function ParseAndLoadWebVTT(vtt) {

      var rxSignatureLine = /^WEBVTT(?:\s.*)?$/;

      var vttLines = vtt.split(/\r\n|\r|\n/); // create an array of lines from our file

      if (!rxSignatureLine.test(vttLines[0])) { // must start with a signature line
        alert("Not a valid time track file.");
        return;
      }

      var rxTimeLine = /^([\d\.:]+)\s+-->\s+([\d\.:]+)(?:\s.*)?$/;
      var rxCaptionLine = /^(?:<v\s+([^>]+)>)?([^\r\n]+)$/;
      var rxBlankLine = /^\s*$/;
      var rxMarkup = /<[^>]>/g;

      var cueStart = null, cueEnd = null, cueText = null;

      function appendCurrentCaption() {
        if (cueStart && cueEnd && cueText) {
          captionsArray.push({start: cueStart, end: cueEnd, caption: Trim(cueText)});
        }

        cueStart = cueEnd = cueText = null;
      }

      for (var i = 1; i < vttLines.length; i++) {

        if (rxBlankLine.test(vttLines[i])) {
          appendCurrentCaption();
          continue;
        }

        if (!cueStart && !cueEnd && !cueText && vttLines[i].indexOf("-->") == -1) {
          //	this is a cue identifier we're ignoring
          continue;
        }

        var timeMatch = rxTimeLine.exec(vttLines[i]);
        if (timeMatch) {
          appendCurrentCaption();
          cueStart = ParseTime(timeMatch[1]);
          cueEnd = ParseTime(timeMatch[2]);
          continue;
        }

        var captionMatch = rxCaptionLine.exec(vttLines[i]);
        if (captionMatch && cueStart && cueEnd) {
          //	captionMatch[1] is the optional voice (speaker) we're ignoring
          var capLine = captionMatch[2].replace(rxMarkup, "");
          if (cueText)
            cueText += " " + capLine;
          else {
            cueText = capLine;
          }
        }
      }

      appendCurrentCaption();

      SortAndDisplayCaptionList();
    }

//-----------------------------------------------------------------------------------------------------------------------------------------
//	A very partial parser for TTML files based on the spec at http://www.w3.org/TR/ttaf1-dfxp/
//  see samples at \\iefs\users\franko\ttml
//-----------------------------------------------------------------------------------------------------------------------------------------

    function ParseAndLoadTTML(ttml) {

      var rxBr = /<br\s*\/>/g;
      var rxMarkup = /<[^>]+>/g;
      var rxP = /<p\s+([^>]+)>\s*((?:\s|.)*?)\s*<\/p>/g;
      var rxTime = /(begin|end|dur)\s*=\s*"([\d.:]+)(h|m|s|ms|t)?"/g;

      var tickRateMatch = ttml.match(/<tt\s[^>]*ttp:tickRate\s*=\s*"(\d+)"[^>]*>/i);
      var tickRate = (tickRateMatch != null) ? parseInt(tickRateMatch[1], 10) : 1;
      if (tickRate == 0)
        tickRate = 1;

      var pMatch;
      while ((pMatch = rxP.exec(ttml)) != null) {
        var cues = {};
        var timeMatch;
        rxTime.lastIndex = 0;
        var attrs = pMatch[1];
        while ((timeMatch = rxTime.exec(attrs)) != null) {
          var seconds;
          var metric = timeMatch[3];
          if (metric) {
            seconds = parseFloat(timeMatch[2]);
            if (metric == "h")
              seconds *= (60 * 60);
            else if (metric == "m")
              seconds *= 60;
            else if (metric == "ms")
              seconds /= 1000;
            else if (metric == "t")
              seconds /= tickRate;
          }
          else {
            seconds = ParseTime(timeMatch[2]);
          }

          cues[timeMatch[1]] = seconds;
        }

        if ("begin" in cues && ("end" in cues || "dur" in cues)) {
          var cueEnd = "end" in cues ? cues.end : (cues.begin + cues.dur);
          var cueText = Trim(XMLDecode(pMatch[2].replace(/[\r\n]+/g, "").replace(rxBr, "\n").replace(rxMarkup, "").replace(/ {2,}/g, " ")));
          captionsArray.push({start: cues.begin, end: cueEnd, caption: cueText});
        }
      }

      SortAndDisplayCaptionList();
    }

//  invoked by script insertion of proxyvtt.ashx
    function ProcessProxyVttResponse(obj) {
      if (obj.status == "error")
        alert("Error loading caption file: " + obj.message);
      else if (obj.status == "success") {
        //  delete any captions we've got
        captionsArray.length = 0;

        if (obj.response.indexOf("<tt") != -1) {
          ParseAndLoadTTML(obj.response);
        } else if (obj.response.indexOf("WEBVTT") == 0) {
          ParseAndLoadWebVTT(obj.response);
        } else {
          alert("Unrecognized caption file format.");
        }
      }
    }




  }
]);
