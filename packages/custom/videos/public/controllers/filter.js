'use strict';

angular.module('mean.videos').filter('formatTime', function () {
  return function (seconds) {
    var hh = Math.floor(seconds / (60 * 60));
    var mm = Math.floor(seconds / 60) % 60;
    var ss = seconds % 60;

    // return (hh === 0 ? '' : (hh < 10 ? '0' : '') + hh.toString() + ':') + (mm < 10 ? '0' : '') + mm.toString() + ':' + (ss < 10 ? '0' : '') + ss.toFixed(3);
    return (hh === 0 ? '' : (hh < 10 ? '0' : '') + hh.toString() + ':') + (mm < 10 ? '0' : '') + mm.toString() + ':' + (ss < 10 ? '0' : '') + ss.toFixed(2);
  }
}).filter('cut', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return '';

    max = parseInt(max, 10);
    if (!max) return value;
    if (value.length <= max) return value;

    value = value.substr(0, max);
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ');
      if (lastspace != -1) {
        value = value.substr(0, lastspace);
      }
    }

    return value + (tail || ' …');
  };
}).filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});;