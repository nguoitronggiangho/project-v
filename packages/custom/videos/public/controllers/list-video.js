(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('ListVideoController', ListVideoController);

  ListVideoController.$inject = ['Videos', 'Categories', '$mdMedia', '$scope'];

  /* @ngInject */
  function ListVideoController(Videos, Categories, $mdMedia, $scope) {
    var vm = this;
    vm.title = 'ListVideoController';
    vm.videos = [];
    vm.categories = [];
    vm.disableLoadmore = false;

    activate();

    ////////////////

    function activate() {
      getAllCategory()
    }

    function getAllCategory() {
      Categories.query(function(categories) {
        return vm.categories = categories
      })
    }
    
    this.query = {
      page: 1,
      order: 'created_at',
      limit: 50,
      cate: null
    };

    var skip = vm.query.limit;

    this.sort = [{
      display: 'Upload date',
      value: 'created_at'
    },{
      display:  'View count',
      value: 'view'
    },{
      display: 'Rating',
      value: 'vote'
    }];
    
    this.type = [{
      display: 'Video',
      value: 'video '
    }];
    
    this.findVideo = function() {
      Videos.getFeedHome(vm.query, success)
    };
    
    function success(videos) {
      return vm.videos = videos
    }
    
    this.loadMore = function() {
      skip += 20;
      Videos.query({skip: skip, limit: 20, cate: this.query.cate, order: this.query.order}, function(data) {
        angular.forEach(data, function(video) {
          vm.videos.push(video)
        });
        if (data.length < 1) {
          vm.disableLoadmore = true
        }
      })
    };
    
    this.loadMoreChannel = function() {
      var channel = [];
      angular.forEach(vm.videos.topChannel, function(item) {
        channel.push(item.channel._id)
      });
      Videos.loadMoreChannel({channel: channel}, function(data) {
        angular.forEach(data, function(item) {
          vm.videos.topChannel.push(item);
          channel.push(item.channel._id)
        });
        if (data.length < 1) {
          vm.disableLoadmore = true
        }
      })
    };

    $scope.$watch(function() {
      if ($mdMedia('(min-width: 1563px)')) {
        $('html').addClass('content-snap-width-3');
        // $('.container').width(1262)
      } else if ($mdMedia('(min-width: 1357px) and (max-width: 1562px)')) {
        $('html').addClass('content-snap-width-2');
        // $('.container').width(1056)
      } else if ($mdMedia('(min-width: 911px) and (max-width: 1356px)')) {
        $('html').addClass('content-snap-width-1');
        // $('.container').width(850)
      } else if ($mdMedia('(min-width: 705px) and (max-width: 910px)')) {
        $('.container').width(644)
      }
      return $mdMedia('(min-width: 1563px)');
    }, function(big) {
      // console.log(big);
      $scope.bigScreen = big;
    });
    
    
    
  }

})();

