angular.module('mean.videos').controller('VideosPlusController', ['$scope', 'Global', 'Videos', '$stateParams', '$location', 'MeanUser', 'Circles', 'Captions', '$sce',
  function ($scope, Global, Videos, $stateParams, $location, MeanUser, Circles, Captions, $sce) {
    $scope.test = "test";
    this.currentTime = 0;
    this.totalTime = 0;
    this.state = null;
    this.volume = 1;
    this.isCompleted = false;
    this.API = null;
    this.seeking = {
      currentTime: 0,
      duration: 0
    };
    this.seeked = {
      currentTime: 0,
      duration: 0
    };

    this.onPlayerReady = function (API) {
      this.API = API;
    };

    this.onError = function (event) {
      console.log("VIDEOGULAR ERROR EVENT");
      console.log(event);
    };

    this.onCompleteVideo = function () {
      this.isCompleted = true;
    };

    this.onUpdateState = function (state) {
      this.state = state;

      if (state == 'play') {
        videoPlayEventHandler();
      }
      if (state == 'pause') {
        videoPauseEventHandler();
      }

    };

    this.onUpdateTime = function (currentTime, totalTime) {
      this.currentTime = currentTime;
      this.totalTime = totalTime;
      videoTimeUpdateEventHandler();
    };

    this.onSeeking = function (currentTime, duration) {
      this.seeking.currentTime = currentTime;
      this.seeking.duration = duration;
    };

    this.onSeeked = function (currentTime, duration) {
      this.seeked.currentTime = currentTime;
      this.seeked.duration = duration;
    };

    this.onUpdateVolume = function (newVol) {
      this.volume = newVol;
    };

    this.onUpdatePlayback = function (newSpeed) {
      this.API.playback = newSpeed;
    };

    this.media = [
      {
        sources: [
          {
            src: $sce.trustAsResourceUrl("/videos/assets/video/oceans.mp4"),
            type: "video/mp4"
          },
          {
            src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"),
            type: "video/ogg"
          },
          {
            src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"),
            type: "video/webm"
          }
        ]
      },
      {
        sources: [
          {
            src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov"),
            type: "video/mp4"
          },
          {
            src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/big_buck_bunny_720p_stereo.ogg"),
            type: "video/ogg"
          }
        ]
      }
    ];

    this.config = {
      width:100,
      responsive:false,
      playsInline: false,
      nativeFullscreen: true,
      autoHide: false,
      autoHideTime: 3000,
      autoPlay: false,
      sources: this.media[0].sources,
      tracks: this.media[0].tracks,
      loop: false,
      preload: "auto",
      controls: false,
      theme: {
        url: "/videos/assets/lib/videogular-themes-default/videogular.css"
      },
      plugins: {
        poster: {
          url: "http://videogular.com/demo/assets/images/videogular.png"
        },
        subtitle: [
          {
            src: "/captions/assets/video/caption-en.vtt",
            kind: "captions",
            srclang: "en",
            label: "English",
            default: "default"
          },
          {
            src: "http://videogular.com/demo/assets/subs/pale-blue-dot-es.vtt",
            kind: "captions",
            srclang: "es",
            label: "Spanish",
            default: null
          }
        ],
        analytics: {
          category: "Videogular",
          label: "Main",
          events: {
            ready: true,
            play: true,
            pause: true,
            stop: true,
            complete: true,
            progress: 10
          }
        }
      }
    };


    //$scope.editVideoPage = function () {

      function LoadCaptionFile(fileObject) {
        if (window.FileReader) {
          var reader = new window.FileReader();

          reader.onload = function () {
            ProcessProxyVttResponse({status: "success", response: reader.result});
          };

          reader.onerror = function (evt) {
            alert("Error reading caption file. Code = " + evt.code);
          };

          try {
            reader.readAsText(fileObject);
          } catch (exc) {
            alert("Exception thrown reading caption file. Code = " + exc.code);
          }
        } else {
          alert("Your browser does not support FileReader.");
        }
      }

      //  this button should be disabled when window.FileReader is undefined
      //  we expect browsers that support FileReader to have a files collection on the input[type=file] control
      $("#ttFile").get(0).addEventListener("change", function () {
        var filesObject = $("#ttFile").get(0).files;
        if (filesObject && filesObject.length > 0) {
          LoadCaptionFile(filesObject[0]);
          console.log(filesObject);
        } else {
          alert("No files collection on input[type=file] control.");
        }
      }, false);

      $("#loadCaptionFileFromUrl").click(function () {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://localhost:3000/captions/assets/video/captions-en.vtt', true);

        // Hack to pass bytes through unprocessed.
        xhr.overrideMimeType('text/plain; charset=UTF-8');

        xhr.onreadystatechange = function (e) {
          if (this.readyState == 4 && this.status == 200) {
            var binStr = this.responseText;
            ProcessProxyVttResponse({status: "success", response: binStr});
          }
        };

        xhr.send();
        //	insert a script request that returns a function invocation and a simple json object
        var s = document.createElement("script");
        s.setAttribute("type", "text/javascript");
        s.setAttribute("src", $("#ttURL").val());
        document.body.appendChild(s);

        //	this would work except for the cross-domain restrictions
        //	$.ajax({
        //	url: $("#ttURL").val(),
        //	success: function (data) {
        //	ProcessProxyVttResponse({ status: "success", response: data });
        //	}
        //	});
      });

      $("#loadSampleCaptionFile").click(function () {
        // data = $("#ttURL").val(window.location.origin + "/captions-en.vtt");
        $.get("/captions/assets/video/entrack.vtt", function (data) {
          ProcessProxyVttResponse({status: "success", response: data});
        });
        //$("#videoElm").append('<track src="/captions/assets/video/entrack.vtt" kind="captions" srclang="en" label="English" default>');
        // $("#loadCaptionFileFromUrl").click();
      });
      //  index into captionsArray of the caption being displayed. -1 if none.
      var captionBeingDisplayed = -1;

      function DisplayExistingCaption(seconds) {
        var ci = FindCaptionIndex(seconds);
        if (ci != captionBeingDisplayed) {
          captionBeingDisplayed = ci;
          if (ci != -1) {
            var theCaption = captionsArray[ci];
            $("#captionTitle").text("Caption for segment from " + FormatTime(theCaption.start) + " to " + FormatTime(theCaption.end) + ":");
            $("#textCaptionEntry").val(theCaption.caption);
          } else {
            $("#captionTitle").html("&nbsp;");
            $("#textCaptionEntry").val("");
          }
        }
      }

      function existingCaptionsEndTime() {
        return captionsArray.length > 0 ? captionsArray[captionsArray.length - 1].end : 0;
      }

      function videoPlayEventHandler() {
        captionBeingDisplayed = -1;

        //  give Opera a beat before doing this
        window.setTimeout(function () {
          $("#textCaptionEntry").val("").prop("readonly", true).addClass("playing");
          $("#pauseButton").prop("disabled", false);
          $("#playButton, #justSaveCaption, #saveCaptionAndPlay").prop("disabled", true);
        }, 16);
      }

      function videoPauseEventHandler() {
        $("#playButton, #justSaveCaption, #saveCaptionAndPlay").prop("disabled", false);
        $("#textCaptionEntry").removeClass("playing").prop("readonly", false);
        $("#pauseButton").prop("disabled", true);

        var playTime = $("#videoElm").prop("currentTime");
        var captionsEndTime = existingCaptionsEndTime();
        if (playTime - 1 < captionsEndTime) {
          var ci = FindCaptionIndex(playTime - 1);
          if (ci != -1) {
            var theCaption = captionsArray[ci];
            $("#captionTitle").text("Edit caption for segment from " + FormatTime(theCaption.start) + " to " + FormatTime(theCaption.end) + ":");
            $("#textCaptionEntry").val(theCaption.caption);
            captionBeingDisplayed = ci;
          } else {
            $("#captionTitle").text("No caption at this time code.");
            $("#textCaptionEntry").val("");
            captionBeingDisplayed = -1;
          }
        } else {
          $("#captionTitle").text("Enter caption for segment from " + FormatTime(existingCaptionsEndTime()) + " to " + FormatTime(playTime) + ":");
          $("#textCaptionEntry").val("");
          captionBeingDisplayed = -1;
        }

        $("#textCaptionEntry").focus().get(0).setSelectionRange(1000, 1000); // set focus and selection point to end
      }

      function videoTimeUpdateEventHandler() {
        var playTime = $("#videoElm").prop("currentTime");

        if (autoPauseAtTime >= 0 && playTime >= autoPauseAtTime) {
          autoPauseAtTime = -1;
          $("#videoElm").get(0).pause();
          return;
        }

        var captionsEndTime = existingCaptionsEndTime();
        if (playTime < captionsEndTime) {
          DisplayExistingCaption(playTime);
        } else {
          $("#captionTitle").text("Pause to enter caption for segment from " + FormatTime(captionsEndTime) + " to " + FormatTime(playTime) + ":");
          if (captionBeingDisplayed != -1) {
            $("#textCaptionEntry").val("");
            captionBeingDisplayed = -1;
          }
        }
      }

      //  the video element's event handlers
      $("#videoElm").bind({
        play: videoPlayEventHandler,
        timeupdate: videoTimeUpdateEventHandler,
        pause: videoPauseEventHandler,
        //canplay: EnableDemoAfterLoadVideo,
        //loadeddata: EnableDemoAfterLoadVideo    // opera doesn't appear to fire canplay but does fire loadeddata
      });

      $("#playButton").click(function () {
        $("#videoElm").get(0).play();
      });

      $("#pauseButton").click(function () {
        $("#videoElm").get(0).pause();
      });

    function SaveCurrentCaption() {
      var playTime = $("#videoElm").prop("currentTime");
      var captionsEndTime = existingCaptionsEndTime();
      if (playTime - 1 < captionsEndTime) {
        var ci = FindCaptionIndex(playTime - 1);
        if (ci != -1) {
          UpdateCaption(ci, $("#textCaptionEntry").val());
        }
      } else {
        AddCaption(captionsEndTime, playTime, $("#textCaptionEntry").val());
      }
    }

    $("#justSaveCaption").click(function () {
      SaveCurrentCaption();
    });

    $("#saveCaptionAndPlay").click(function () {
      SaveCurrentCaption();
      $("#videoElm").get(0).play();
    });


      function Trim(s) {
        return s.replace(/^\s+|\s+$/g, "");
      }

//	parses webvtt time string format into floating point seconds
      function ParseTime(sTime) {

        //  parse time formatted as hours:mm:ss.sss where hours are optional
        if (sTime) {
          var m = sTime.match(/^\s*(\d+)?:?(\d+):([\d\.]+)\s*$/);
          if (m != null) {
            return (m[1] ? parseFloat(m[1]) : 0) * 3600 + parseFloat(m[2]) * 60 + parseFloat(m[3]);
          } else {
            m = sTime.match(/^\s*(\d{2}):(\d{2}):(\d{2}):(\d{2})\s*$/);
            if (m != null) {
              var seconds = parseFloat(m[1]) * 3600 + parseFloat(m[2]) * 60 + parseFloat(m[3]) + parseFloat(m[4]) / 30;
              return seconds;
            }
          }
        }

        return 0;
      }

//	formats floating point seconds into the webvtt time string format
      function FormatTime(seconds) {
        var hh = Math.floor(seconds / (60 * 60));
        var mm = Math.floor(seconds / 60) % 60;
        var ss = seconds % 60;

        return (hh == 0 ? "" : (hh < 10 ? "0" : "") + hh.toString() + ":") + (mm < 10 ? "0" : "") + mm.toString() + ":" + (ss < 10 ? "0" : "") + ss.toFixed(3);
      }

//	our state
      var captionsArray = [];
      var autoPauseAtTime = -1;

      function FindCaptionIndexLinearSearch(seconds) {
        if (captionsArray.length < 1)
          return -1;

        //	linear search isn't optimal but it's safe
        for (var i = 0; i < captionsArray.length; ++i) {
          if (captionsArray[i].start <= seconds && seconds < captionsArray[i].end)
            return i;
        }

        return -1;
      }

      function FindCaptionIndex(seconds) {
        var below = -1;
        var above = captionsArray.length;
        var i = Math.floor((below + above) / 2);

        while (below < i && i < above) {

          if (captionsArray[i].start <= seconds && seconds < captionsArray[i].end)
            return i;

          if (seconds < captionsArray[i].start) {
            above = i;
          } else {
            below = i;
          }

          i = Math.floor((below + above) / 2);
        }

        return -1;
      }


      function PlayCaptionFromList(listRowId) {
        var captionsArrayIndex = parseInt(listRowId.match(/ci(\d+)/)[1]);

        var vid = $('#videoElm').get(0);
        // vid.pause();
        vid.currentTime = captionsArray[captionsArrayIndex].start;
        autoPauseAtTime = captionsArray[captionsArrayIndex].end;
        vid.play();
      }

      function AddCaptionListRow(ci) {
        var theId = "ci" + ci.toString();
        $("#display").append("<div id=\"" + theId + "\"><span>" + FormatTime(captionsArray[ci].start) + "</span><span>" + FormatTime(captionsArray[ci].end) + "</span><span>" + XMLEncode(captionsArray[ci].caption).replace(/\r\n|\r|\n/g, "<br/>") + "</span></div>");

        $("#" + theId).click(function () {
          PlayCaptionFromList($(this).attr("id"));
        });
      }

      function AddCaption(captionStart, captionEnd, captionText) {
        captionsArray.push({start: captionStart, end: captionEnd, caption: Trim(captionText)});
        AddCaptionListRow(captionsArray.length - 1);
      }

      function SortAndDisplayCaptionList() {
        captionsArray.sort(function (a, b) {
          return a.start - b.start;
        });

        $("#display div").remove();
        for (var ci = 0; ci < captionsArray.length; ++ci) {
          AddCaptionListRow(ci);
        }
      }



      function XMLEncode(s) {
        return s.replace(/\&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');   //.replace(/'/g, '&apos;').replace(/"/g, '&quot;');
      }

      function XMLDecode(s) {
        return s.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&apos;/g, "'").replace(/&quot;/g, '"').replace(/&amp;/g, '&');
      }


      //-----------------------------------------------------------------------------------------------------------------------------------------
//	Partial parser for WebVTT files based on the spec at http://dev.w3.org/html5/webvtt/
//-----------------------------------------------------------------------------------------------------------------------------------------

      function ParseAndLoadWebVTT(vtt) {

        var rxSignatureLine = /^WEBVTT(?:\s.*)?$/;

        var vttLines = vtt.split(/\r\n|\r|\n/); // create an array of lines from our file

        if (!rxSignatureLine.test(vttLines[0])) { // must start with a signature line
          alert("Not a valid time track file.");
          return;
        }

        var rxTimeLine = /^([\d\.:]+)\s+-->\s+([\d\.:]+)(?:\s.*)?$/;
        var rxCaptionLine = /^(?:<v\s+([^>]+)>)?([^\r\n]+)$/;
        var rxBlankLine = /^\s*$/;
        var rxMarkup = /<[^>]>/g;

        var cueStart = null, cueEnd = null, cueText = null;

        function appendCurrentCaption() {
          if (cueStart && cueEnd && cueText) {
            captionsArray.push({start: cueStart, end: cueEnd, caption: Trim(cueText)});
          }

          cueStart = cueEnd = cueText = null;
        }

        for (var i = 1; i < vttLines.length; i++) {

          if (rxBlankLine.test(vttLines[i])) {
            appendCurrentCaption();
            continue;
          }

          if (!cueStart && !cueEnd && !cueText && vttLines[i].indexOf("-->") == -1) {
            //	this is a cue identifier we're ignoring
            continue;
          }

          var timeMatch = rxTimeLine.exec(vttLines[i]);
          if (timeMatch) {
            appendCurrentCaption();
            cueStart = ParseTime(timeMatch[1]);
            cueEnd = ParseTime(timeMatch[2]);
            continue;
          }

          var captionMatch = rxCaptionLine.exec(vttLines[i]);
          if (captionMatch && cueStart && cueEnd) {
            //	captionMatch[1] is the optional voice (speaker) we're ignoring
            var capLine = captionMatch[2].replace(rxMarkup, "");
            if (cueText)
              cueText += " " + capLine;
            else {
              cueText = capLine;
            }
          }
        }

        appendCurrentCaption();

        SortAndDisplayCaptionList();
      }

//-----------------------------------------------------------------------------------------------------------------------------------------
//	A very partial parser for TTML files based on the spec at http://www.w3.org/TR/ttaf1-dfxp/
//  see samples at \\iefs\users\franko\ttml
//-----------------------------------------------------------------------------------------------------------------------------------------

      function ParseAndLoadTTML(ttml) {

        var rxBr = /<br\s*\/>/g;
        var rxMarkup = /<[^>]+>/g;
        var rxP = /<p\s+([^>]+)>\s*((?:\s|.)*?)\s*<\/p>/g;
        var rxTime = /(begin|end|dur)\s*=\s*"([\d.:]+)(h|m|s|ms|t)?"/g;

        var tickRateMatch = ttml.match(/<tt\s[^>]*ttp:tickRate\s*=\s*"(\d+)"[^>]*>/i);
        var tickRate = (tickRateMatch != null) ? parseInt(tickRateMatch[1], 10) : 1;
        if (tickRate == 0)
          tickRate = 1;

        var pMatch;
        while ((pMatch = rxP.exec(ttml)) != null) {
          var cues = {};
          var timeMatch;
          rxTime.lastIndex = 0;
          var attrs = pMatch[1];
          while ((timeMatch = rxTime.exec(attrs)) != null) {
            var seconds;
            var metric = timeMatch[3];
            if (metric) {
              seconds = parseFloat(timeMatch[2]);
              if (metric == "h")
                seconds *= (60 * 60);
              else if (metric == "m")
                seconds *= 60;
              else if (metric == "ms")
                seconds /= 1000;
              else if (metric == "t")
                seconds /= tickRate;
            }
            else {
              seconds = ParseTime(timeMatch[2]);
            }

            cues[timeMatch[1]] = seconds;
          }

          if ("begin" in cues && ("end" in cues || "dur" in cues)) {
            var cueEnd = "end" in cues ? cues.end : (cues.begin + cues.dur);
            var cueText = Trim(XMLDecode(pMatch[2].replace(/[\r\n]+/g, "").replace(rxBr, "\n").replace(rxMarkup, "").replace(/ {2,}/g, " ")));
            captionsArray.push({start: cues.begin, end: cueEnd, caption: cueText});
          }
        }

        SortAndDisplayCaptionList();
      }


//-----------------------------------------------------------------------------------------------------------------------------------------
//  A very partial parser for srt files
//-----------------------------------------------------------------------------------------------------------------------------------------
    function ParseAndLoadSrt(data) {

      // declare needed variables
      var retObj = {
            title: "",
            remote: "",
            data: []
          },
          subs = [],
          i = 0,
          idx = 0,
          lines,
          time,
          text,
          endIdx,
          sub;

      // Here is where the magic happens
      // Split on line breaks
      lines = data.split( /(?:\r\n|\r|\n)/gm );
      endIdx = lastNonEmptyLine( lines ) + 1;

      for( i=0; i < endIdx; i++ ) {
        sub = {};
        text = [];

        i = nextNonEmptyLine( lines, i );
        sub.id = parseInt( lines[i++], 10 );

        // Split on '-->' delimiter, trimming spaces as well
        time = lines[i++].split( /[\t ]*-->[\t ]*/ );

        sub.start = toSeconds( time[0] );

        // So as to trim positioning information from end
        idx = time[1].indexOf( " " );
        if ( idx !== -1) {
          time[1] = time[1].substr( 0, idx );
        }
        sub.end = toSeconds( time[1] );

        // Build single line of text from multi-line subtitle in file
        while ( i < endIdx && lines[i] ) {
          text.push( lines[i++] );
        }

        // Join into 1 line, SSA-style linebreaks
        // Strip out other SSA-style tags
        sub.text = text.join( "\\N" ).replace( /\{(\\[\w]+\(?([\w\d]+,?)+\)?)+\}/gi, "" );

        // Escape HTML entities
        sub.text = sub.text.replace( /</g, "&lt;" ).replace( />/g, "&gt;" );

        // Unescape great than and less than when it makes a valid html tag of a supported style (font, b, u, s, i)
        // Modified version of regex from Phil Haack's blog: http://haacked.com/archive/2004/10/25/usingregularexpressionstomatchhtml.aspx
        // Later modified by kev: http://kevin.deldycke.com/2007/03/ultimate-regular-expression-for-html-tag-parsing-with-php/
        sub.text = sub.text.replace( /&lt;(\/?(font|b|u|i|s))((\s+(\w|\w[\w\-]*\w)(\s*=\s*(?:\".*?\"|'.*?'|[^'\">\s]+))?)+\s*|\s*)(\/?)&gt;/gi, "<$1$3$7>" );
        sub.text = sub.text.replace( /\\N/gi, "<br />" );
        subs.push( createTrack( "subtitle", sub ) );
        captionsArray.push({start: sub.start, end: sub.end, caption: sub.text});
      }

      retObj.data = subs;
      console.log(retObj);
      SortAndDisplayCaptionList();
      return retObj;
    };

    function createTrack( name, attributes ) {
      var track = {};
      track[name] = attributes;
      return track;
    }

    // Simple function to convert HH:MM:SS,MMM or HH:MM:SS.MMM to SS.MMM
    // Assume valid, returns 0 on error
    function toSeconds( t_in ) {
      var t = t_in.split( ':' );

      try {
        var s = t[2].split( ',' );

        // Just in case a . is decimal seperator
        if ( s.length === 1 ) {
          s = t[2].split( '.' );
        }

        return parseFloat( t[0], 10 ) * 3600 + parseFloat( t[1], 10 ) * 60 + parseFloat( s[0], 10 ) + parseFloat( s[1], 10 ) / 1000;
      } catch ( e ) {
        return 0;
      }
    }

    function nextNonEmptyLine( linesArray, position ) {
      var idx = position;
      while ( !linesArray[idx] ) {
        idx++;
      }
      return idx;
    }

    function lastNonEmptyLine( linesArray ) {
      var idx = linesArray.length - 1;

      while ( idx >= 0 && !linesArray[idx] ) {
        idx--;
      }

      return idx;
    }

//  invoked by script insertion of proxyvtt.ashx
    function ProcessProxyVttResponse(obj) {
      if (obj.status == "error")
        alert("Error loading caption file: " + obj.message);
      else if (obj.status == "success") {
        //  delete any captions we've got
        captionsArray.length = 0;
        // console.log(obj);
        // console.log(obj.response);

        if (obj.response.indexOf("<tt") != -1) {
          ParseAndLoadTTML(obj.response);
        } else if (obj.response.indexOf("WEBVTT") == 0) {
          ParseAndLoadWebVTT(obj.response);
        } else if (obj.response.indexOf('1') == 0) {
          console.log('srt');
          ParseAndLoadSrt(obj.response);
        } else {
          alert("Unrecognized caption file format.");
        }
      }
    }


    $scope.findVideoCaption = function () {
      Captions.findVideoCaption({
        videoId: $stateParams.videoId
      }, function (captionCollection) {
        $scope.captionCollection = captionCollection;

        if (captionCollection.hide) {
          $scope.hasCaption = false;
          $scope.showUploadCaption = true;
        } else {
          $scope.hasCaption = true;
          $scope.showUploadCaption = false;
        }
        console.log(captionCollection);
      })
    };

    $scope.selectToLang = function () {
      var language = $scope.toLangSelected;
      if ($scope.captionCollection[language.language_symbol]) {
        $scope.captionToEdit = $scope.captionCollection[language.language_symbol][0];
      } else {
        $scope.captionToEdit = {};
      }
    };

    $scope.selectFromLang = function () {
      var language = $scope.fromLangSelected;
      if ($scope.captionCollection[language.language_symbol]) {
        $scope.fromLanguageCaption = $scope.captionCollection[language.language_symbol][0];
      } else {
        $scope.fromLanguageCaption = {}
      }

    };

    $scope.deleteCaption = function (captionId) {
      console.log(captionId);
    };

    $scope.createMaterBrand = function () {
      console.log($scope.caption);
      console.log($scope.language);
    };

    $scope.createLang = function () {
      Captions.createLang({

      })
    };

    $scope.loadCaptionFromLang = function (caption) {
      //console.log(caption);
      $.get(caption.url).done(function (data) {
        ProcessProxyVttResponse({status: "success", response: data});
      }).fail(function() {
        alert( "load error" );
      });
    };

    //$scope.loadCaptionToEdit = function (caption) {
    //  console.log(caption);
    //  $scope.captionToEdit = caption;
    //};

    $scope.formEditCaptionSubmitted = function () {
      console.log($scope.captionToEdit);
      var caption = $scope.captionToEdit;
      var id = caption._id;
      if (!caption.updated) {
        caption.updated = [];
      }
      caption.updated.push(new Date().getTime());
      Captions.update({captionId: id}, caption);
    };

    $(document).on('click',".segment-card-item", function () {
      var id = $(this).data('order');
      $(this).addClass('active').siblings().removeClass('active');
      $(document).find('.action-bar.active').removeClass('active').hide();
      $(this).siblings().find('.history-wrapper').hide();
      $(this).find('#action-'+id).addClass("active").show();
      $(this).find('.textedit').focus()
    });

    $(document).on('click', '.card-cancel', function (e) {
      e.stopPropagation();
      $(document).find('.action-bar.active').hide();
      $('.segment-card-item').removeClass('active');
      $('.history-wrapper').hide();
    });

    $(document).on('click', '.action-history', function(e) {
      e.stopPropagation();
      var id = $(this).data('order');
      $(document).find('.history-wrapper-'+id).toggle();
    });

    $scope.editSubmited = function (index) {
      console.log($scope.captionToEdit.sentence[index + 1][0]);
      var data = $scope.captionToEdit.sentence[index + 1][0];
      Captions.checkout({
        language: $scope.toLangSelected,
        videoId: $stateParams.videoId,
        captionId: $scope.captionToEdit._id,
        text: data.text,
        timeSstart: data.timeStart,
        timeEnd: data.timeEnd,
        order: data.order
      }, function (response) {
        console.log(response)
      });
    };

    $scope.shoot = function () {
      if (captionsArray.length >0 && $scope.language && $scope.videoId) {
        var data = {};
        data.sentence = captionsArray;
        data.videoId = $scope.videoId;
        data.language = $scope.language;
        var caption = new Captions(data);
        caption.$save().then(function (response) {
          console.log(response.caption._id);
          $scope.captionId = response.caption._id;
          $scope.message = response.message;
          $('#captionId').val(response.caption._id)
        });
      } else {
        console.log('error create')
      }
    };


    }
  //}
]);