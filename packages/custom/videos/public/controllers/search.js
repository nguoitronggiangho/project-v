(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('SearchController', SearchController);

  SearchController.$inject = ['$stateParams', 'Videos', 'Search', 'Request', '$mdToast'];

  /* @ngInject */
  function SearchController($stateParams, Videos, Search, Request, $mdToast) {
    var vm = this;
    vm.title = 'SearchController';
    vm.keyword = $stateParams.keyWord;
    vm.searchData = [];

    activate();

    ////////////////

    function activate() {
      // getSearchYoutubeData().then(function() {
      //   console.log('search youtube done')
      // });
      getSearchData().then(function() {
        // console.log('get data done')
      })
    }

    function getSearchData() {
      return Search.getSearchResult($stateParams.keyWord)
          .then(function(data) {
            vm.searchData = data;
            return vm.searchData;
          })
    }

    function getSearchYoutubeData() {
      return Search.getSearchYoutubeData  ($stateParams.keyWord)
          .then(function(data) {
            vm.searchYoutubeData = data;
            return vm.searchYoutubeData;
          })
    }

    vm.requestSub = function(video) {
      // console.log(video);
      var data = {};
      data.title = video.snippet.title;
      data.videoId = video.id.videoId;
      data.language = null;
      
      var request = new Request(data);

      request.$save(function(response) {
        // $location.path('request/' + response._id);
        console.log(response);
        $mdToast.show(
            $mdToast.simple()
                .textContent('Gửi đề xuất thành công!')
                .position('top right')
                .hideDelay(3000)
        );
      })
    }
  }

})();

