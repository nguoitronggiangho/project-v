(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('SubscriptionsController', SubscriptionsController);

  SubscriptionsController.$inject = ['Subscriptions'];

  /* @ngInject */
  function SubscriptionsController(Subscriptions) {
    var vm = this;
    vm.title = 'SubscriptionsController';

    activate();

    ////////////////

    function activate() {
      getSubscriptionsData()
    }
    
    function getSubscriptionsData() {
      Subscriptions.getSubscriptionsData()
          .then(function(response) {
            // console.log(response);
            vm.subscriptionsData = response
          })
    }
  }

})();

