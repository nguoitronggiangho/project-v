(function() {
  'use strict';

  angular
      .module('mean.videos')
      .controller('UploadCaptionController', UploadCaptionController);
  
  UploadCaptionController.$inject = ['$stateParams', '$scope', 'Captions'];

  /* @ngInject */
  function UploadCaptionController($stateParams, $scope, Captions) {
    var vm = this;
    vm.title = 'UploadCaptionController';

    activate();

    ////////////////

    function activate() {
      console.log($stateParams.videoId);
      getAllCaptions();
    }

    $scope.videoId = $stateParams.videoId;

    $scope.upload = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
      console.log($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event);
      $scope.fileSelected = true;
    };

    function getAllCaptions() {
      Captions.findLanguage(function (language) {
        $scope.languages = language;
      })
    }

  }

})();

