(function() {
  'use strict';

  angular
      .module('mean.videos')
      .factory('Trending', Trending);

  Trending.$inject = ['$resource', '$http'];

  /* @ngInject */
  function Trending($resource, $http) {
    var service = {
      mainPage: mainPage
    };
    return service;

    ////////////////

    function mainPage() {
      return $http.get('/api/trending/mainpage')
          .then(success)
          .catch(error)
    }
    
    function success(response) {
      // console.log(response);
      return response.data
    }
    function error(err) {
      console.log(err);
    }
  }

})();

