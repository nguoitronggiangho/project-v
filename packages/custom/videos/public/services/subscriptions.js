(function() {
  'use strict';

  angular
      .module('mean.videos')
      .factory('Subscriptions', Subscriptions);

  Subscriptions.$inject = ['$http'];

  /* @ngInject */
  function Subscriptions($http) {
    var service = {
      getSubscriptionsData: getSubscriptionsData
    };
    return service;

    ////////////////

    function getSubscriptionsData() {
      return $http.get('/api/feed/subscriptions')
          .then(success)
          .catch(error)
    }

    function success(response) {
      return response.data
    }
    function error(err) {
      console.log(err);
    }
  }

})();

