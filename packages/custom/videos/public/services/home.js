'use strict';

angular.module('mean.videos').factory('Home', ['$resource',
  function($resource) {
    return $resource('api/captions/:captionId', null, {
      update: {
        method: 'PUT'
      },
      trending: {
        method: 'GET',
        url: 'api/videos/trending/get',
        isArray: true
      },
      getTopVolunteers: {
        method: 'GET',
        url: 'api/captions/volunteer/get',
        isArray: true
      },
      getTopByCategory: {
        method: 'GET',
        url: 'api/videos/top-by-category/get',
        isArray: true
      }
    });
  }
]);