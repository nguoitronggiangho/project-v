(function() {
  'use strict';

  angular
      .module('mean.videos')
      .service('Search', Search);

  Search.$inject = ['$http', '$log', '$rootScope'];

  /* @ngInject */
  function Search($http, $log, $rootScope) {
    return {
      getSearchResult: searchVideos,
      getSearchYoutubeData: searchYoutube,
      getKeyword: getKeyword,
      createYoutube: youtube
    };

    ////////////////

    function searchVideos(keyWord) {
      return $http.get('api/search/' + keyWord)
          .then(getAvengersComplete)
          .catch(getAvengersFailed);

      function getAvengersComplete(response) {
        return response.data;
      }

      function getAvengersFailed(error) {
        $log.error('XHR Failed for getSearchResult.' + error.data);
      }
    }
    
    function searchYoutube(key) {
      console.log('key: ' +key);
      return $http.get('https://www.googleapis.com/youtube/v3/search', {
              params: {
                key: 'AIzaSyD2K6OooNWMPgEWlkAkgAIRctksFyKk1vY',
                type: 'video',
                maxResults: '8',
                part: 'id,snippet',
                fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails/default,items/snippet/channelTitle',
                q: key
              }
            })
          .then(searchYoutubeComplete)
          .catch(searchYoutubeFailed);
      
      function searchYoutubeComplete(response) {
        return response.data;
      }
      
      function searchYoutubeFailed(error) {
        console.log('XHR Failed to get youtube search result ' + error.data);
        console.log(error);
      }
      
    }

    function getKeyword(key) {
      return $http.get('api/keyword/'+key);
      //     .then(getKeywordComplete)
      //     .catch(getKeywordError);
      //
      // function getKeywordComplete(response) {
      //   $log.info(response);
      //   return response.data;
      // }
      //
      // function getKeywordError(error) {
      //   $log.error('XHR Failed for getSearchKeyword' + error.data)
      // }
    }
    
    //create Youtube video
    function youtube(data) {
      return $http.post('api/videos/create/youtube', data)
          .then(createYoutubeComplete)
          .catch(createYoutubeFailed);
      
      function createYoutubeComplete(reponse) {
        // console.log(reponse);
        return reponse
      }
      
      function createYoutubeFailed(error) {
        return error;
      }
    }
  }

})();

