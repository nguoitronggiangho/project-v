'use strict';

angular.module('mean.videos').factory('Videos', ['$resource',
  function($resource) {
    return $resource('api/videos/:videoId', {
      videoId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      getFeedHome: {
        url: 'api/feed/home',
        method: 'GET'
      },
      loadMoreChannel: {
        url: 'api/feed/loadmorechannel',
        method: 'POST',
        isArray: true
      },
      getRandom: {
        url: 'api/videos/view/random',
        method: 'GET',
        isArray: true
      },
      youtube: {
        method: 'POST',
        url: 'api/videos/create/youtube'
      },
      findUserVideos: {
        method: 'GET',
        url: 'api/videos/find-user-videos/get',
        isArray: true
      },
      getAllCategories: {
        method: 'GET',
        url: 'api/category/get',
        isArray: true
      },
      vote: {
        url: 'api/videos/vote/post',
        method: 'POST'
      },
      addFavorite: {
        method: 'GET',
        url: 'api/user-metadata/favorite/:videoId'
      },
      report: {
        url: 'api/videos/report/post',
        method: 'POST'
      }
    });
  }
]);
