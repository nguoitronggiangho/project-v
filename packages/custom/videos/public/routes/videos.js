'use strict';

angular.module('mean.videos').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('all videos', {
      url: '/videos',
      templateUrl: 'videos/views/list.html',
      resolve: {
        loggedin: function (MeanUser) {
          return true;
        }
      }
    })
    .state('create video', {
      url: '/video/created',
      templateUrl: '/videos/views/create.html',
      resolve: {
        loggedin: function (MeanUser) {
          return MeanUser.checkLoggedin();
        }
      }
    })
    .state('edit video', {
      url: '/videos/:videoId/edit',
      templateUrl: '/videos/views/edit.html',
      resolve: {
        loggedin: function (MeanUser) {
          return MeanUser.checkLoggedin();
        }
      }
    })
    .state('video by id', {
      url: '/videos/:videoId',
      templateUrl: '/videos/views/view.html',
      resolve: {
        loggedin: function (MeanUser) {
          return true;
        }
      }
    })
    .state('upload-video', {
      url: '/video/upload',
      templateUrl: '/videos/views/upload.html',
      resolve: {
        loggedin: function (MeanUser) {
          return true;
        }
      }
    })
    .state('search', {
      url: '/search/:keyWord',
      templateUrl: '/videos/views/search.html'
    })
    .state('trending', {
      url: '/feed/trending',
      templateUrl: '/videos/views/trending.html'
    })
    .state('subscriptions', {
      url: '/feed/subscriptions',
      templateUrl: '/videos/views/subscriptions.html',
      resolve: {
        loggedin: function(MeanUser) {
          return true
        }
      }
    })
    .state('legal', {
      url: '/privacy',
      templateUrl: '/videos/views/pages/policy.html'
    })
    .state('upload-caption', {
      url: '/caption/:videoId/upload',
      templateUrl: '/videos/views/upload-captions.html'
    });
  }
]);
