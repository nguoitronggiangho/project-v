'use strict';

var mean = require('../../../../../core');
var connection = [];

module.exports = function (System, app, auth, database) {

  // io.sockets.on('connection', function (socket) {
  //
  //   socket.once('disconnect', function() {
  //     connection.splice(connection.indexOf(socket), 1);
  //     socket.disconnect();
  //     console.log('Disconnected: %s sockets remaining ' + connection.length)
  //   });
  //
  //   connection.push(socket);
  //   console.log('Connected: %s sockets conected ' + connection.length);

    // setInterval(function () {
    //   io.emit('time', new Date);
    // }, 5000);
  // });
  // Home route
  var index = require('../controllers/index')(System);
  app.route('/')
      .get(index.render);
  app.route('/api/aggregatedassets')
      .get(index.aggregatedList);

  app.get('/*', function (req, res, next) {
    res.header('workerID', JSON.stringify(mean.options.workerid));
    next(); // http://expressjs.com/guide.html#passing-route control
  });

  app.get('/api/get-public-config', function (req, res) {
    var config = mean.loadConfig();

    return res.send(config.public);
  });
};
