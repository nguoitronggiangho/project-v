'use strict';

angular.module('mean.system', ['ui.router', 'mean-factory-interceptor', 'ngMaterial'])
  .run(['$rootScope', function($rootScope) {
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      var toPath = toState.url;
      toPath = toPath.replace(new RegExp('/', 'g'), '');
      toPath = toPath.replace(new RegExp(':', 'g'),'-');
      $rootScope.state = toPath;
      if($rootScope.state === '' ) {
        $rootScope.state = 'firstPage';
      }
    });
  }])
    .config(function($mdThemingProvider) {
      var customPrimary = {
        '50': '#54ecff',
        '100': '#3be9ff',
        '200': '#21e6ff',
        '300': '#08e3ff',
        '400': '#00d3ed',
        '500': '#00BCD4',
        '600': '#00a5ba',
        '700': '#008fa1',
        '800': '#007887',
        '900': '#00626e',
        'A100': '#6eefff',
        'A200': '#87f1ff',
        'A400': '#a1f4ff',
        'A700': '#004b54',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
        // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
          '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
      };
      $mdThemingProvider
          .definePalette('customPrimary',
              customPrimary);

      var customAccent = {
        '50': '#a60038',
        '100': '#bf0041',
        '200': '#d9004a',
        '300': '#f20053',
        '400': '#ff0d5f',
        '500': '#ff2670',
        '600': '#ff5992',
        '700': '#ff73a3',
        '800': '#ff8cb3',
        '900': '#ffa6c4',
        'A100': '#ff5992',
        'A200': '#FF4081',
        'A400': '#ff2670',
        'A700': '#ffbfd5',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
        // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
          '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
      };
      $mdThemingProvider
          .definePalette('customAccent',
              customAccent);

      var customWarn = {
        '50': '#ffb280',
        '100': '#ffa266',
        '200': '#ff934d',
        '300': '#ff8333',
        '400': '#ff741a',
        '500': '#ff6400',
        '600': '#e65a00',
        '700': '#cc5000',
        '800': '#b34600',
        '900': '#993c00',
        'A100': '#ffc199',
        'A200': '#ffd1b3',
        'A400': '#ffe0cc',
        'A700': '#e1000c',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
        // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
          '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
      };
      $mdThemingProvider
          .definePalette('customWarn',
              customWarn);

      $mdThemingProvider.theme('votesubtheme')
          .primaryPalette('customPrimary')
          .accentPalette('customAccent')
          .warnPalette('customWarn');
      $mdThemingProvider.setDefaultTheme('votesubtheme');
    })
;
