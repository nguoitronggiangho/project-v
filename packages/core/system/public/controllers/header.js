'use strict';

angular.module('mean.system').controller('HeaderController', ['$scope', '$rootScope', 'Menus', 'MeanUser', '$state', '$log', '$timeout', '$q', 'Videos', '$translate', '$location', 'Search', 'Channel', '$mdDialog', '$mdMedia',  function($scope, $rootScope, Menus, MeanUser, $state, $log, $timeout, $q, Videos, $translate, $location, Search, Channel, $mdDialog, $mdMedia) {

    var self = this;

    /*
     |--------------------------------------------------------------------------
     | Search bar
     |--------------------------------------------------------------------------
     */
    self.states        = [];
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;

    self.newState = newState;

    self.searchSubmit = function() {
      console.log(self.searchText);
      $location.path('search/'+self.searchText);
    };

    function newState(state) {
      alert("Sorry! You'll need to create a Constituion for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var deferred;

      deferred = $q.defer();
      if (query && query.length > 2) {
        Search.getKeyword(query).success(function(data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      } else {
        return null;
      }
    }

    function searchTextChange(text) {
      // $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
      var keyword = '';
      if (item) {
        keyword = item.keyWord;
        $location.path('search/'+ keyword);
      }

      $log.info('Item changed to ' + JSON.stringify(item));
    }

    /*
     |--------------------------------------------------------------------------
     | Select language dropdown
     |--------------------------------------------------------------------------
     */
    var originatorEv;
    this.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };
    this.changeLanguage = function (key) {
      $translate.use(key);
    };
    $rootScope.$on('$translateChangeSuccess', function(event, data) {
      // var language ;
      // $rootScope.lang = 'Vietnamese';

      if (data.language === 'en') {
        $rootScope.lang = 'English'
      } else {
        $rootScope.lang = 'Vietnamese'
      }



      // $rootScope.default_direction = language === 'ar' ? 'rtl' : 'ltr';
      // $rootScope.opposite_direction = language === 'ar' ? 'ltr' : 'rtl';
      //
      // $rootScope.default_float = language === 'ar' ? 'right' : 'left';
      // $rootScope.opposite_float = language === 'ar' ? 'left' : 'right';
    });


    
    var vm = this;

    vm.menus = {};
    vm.hdrvars = {
      authenticated: MeanUser.loggedin,
      user: MeanUser.user, 
      isAdmin: MeanUser.isAdmin
    };

    // Default hard coded menu items for main menu
    var defaultMainMenu = [];

    // Query menus added by modules. Only returns menus that user is allowed to see.
    function queryMenu(name, defaultMenu) {

      Menus.query({
        name: name,
        defaultMenu: defaultMenu
      }, function(menu) {
        vm.menus[name] = menu;
      });
    }

    // Query server for menus and check permissions
    queryMenu('main', defaultMainMenu);
    queryMenu('account', []);


    $scope.isCollapsed = false;

    $rootScope.$on('loggedin', function() {
      queryMenu('main', defaultMainMenu);

      vm.hdrvars = {
        authenticated: MeanUser.loggedin,
        user: MeanUser.user,
        isAdmin: MeanUser.isAdmin
      };
    });

    vm.logout = function(){
      MeanUser.logout();
    };

    $rootScope.$on('logout', function() {
      vm.hdrvars = {
        authenticated: false,
        user: {},
        isAdmin: false
      };
      queryMenu('main', defaultMainMenu);
      $state.go('home');
    });

    /*
     |--------------------------------------------------------------------------
     | User infomation dropdown
     |--------------------------------------------------------------------------
     */
    vm.getChannelByUser = function() {
      if (MeanUser.loggedin) {
        Channel.getChannelByUser().then(function(channels) {
          // console.log(channels);
          return vm.channels = channels
        })
      }
    };
    $(document).on('click', function(event) {
      if (!$(event.target).closest('#nav-user').length) {
        $('.nav-user-dropdown').hide()
      }
    });
    $('#nav-user').on('click', function() {
      $('.nav-user-dropdown').toggle()
    });
    /*
     |--------------------------------------------------------------------------
     | create channel
     |--------------------------------------------------------------------------
     */
    vm.createChannel = function(ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
      $mdDialog.show({
            controller: DialogController,
            templateUrl: '/system/views/_create-channel-request.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: useFullScreen
          })
          .then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
          }, function() {
            $scope.status = 'You cancelled the dialog.';
          });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    };

    DialogController.$inject = ['$scope', '$mdDialog', 'Channel', '$mdToast'];
    function DialogController($scope, $mdDialog, Channel, $mdToast) {
      $scope.hide = function() {
        $mdDialog.hide();
      };
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.answer = function(answer) {
        $mdDialog.hide(answer);
      };

      $scope.create = function(isValid) {

        // var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
        // var videoId = $scope.request.url.match(myregexp)[1];

        if (isValid) {
          // $scope.article.permissions.push('test test');

          var data = {};
          data.name = $scope.request.name;
          data.description = $scope.request.description;
          data.type = 'create';
          var channel = new Channel.channelRequest(data);

          channel.$save(function(response) {
            // $location.path('request/' + response._id);
            // console.log(response);
            // $scope.requests.push(response);
            $mdDialog.cancel();
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Gửi đề xuất thành công!')
                    .position('top right')
                    .hideDelay(3000)
            );
          });

          $scope.request = {};

        } else {
          $scope.submitted = true;
        }
      };
    }

  }
]);
