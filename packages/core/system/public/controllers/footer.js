(function() {
  'use strict';

  angular
      .module('mean.system')
      .controller('FooterController', FooterController);

  FooterController.$inject = ['$scope', '$mdDialog', '$mdMedia', 'Captions'];

  /* @ngInject */
  function FooterController($scope, $mdDialog, $mdMedia, Captions) {
    var vm = this;
    vm.title = 'FooterController';
    
    activate();

    $scope.showAdvanced = function(ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
      $mdDialog.show({
            controller: DialogController,
            templateUrl: '/system/views/_request.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: useFullScreen
          })
          .then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
          }, function() {
            $scope.status = 'You cancelled the dialog.';
          });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    };

    ////////////////

    function activate() {
      
    }


    /**
     * Dialog Controller
     * @param $scope
     * @param $mdDialog
     * @constructor
     */

    DialogController.$inject = ['$scope', '$mdDialog', 'Captions', 'Request', '$mdToast'];
    function DialogController($scope, $mdDialog, Captions, Request, $mdToast) {
      $scope.hide = function() {
        $mdDialog.hide();
      };
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.answer = function(answer) {
        $mdDialog.hide(answer);
      };
      $scope.getLanguages = function() {
        Captions.findLanguage(function(languages) {
          $scope.languages = languages;
        })
      };

      $scope.create = function(isValid) {

        var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
        var videoId = $scope.request.url.match(myregexp)[1];

        if (isValid) {
          // $scope.article.permissions.push('test test');

          var data = {};
          data.title = $scope.request.title;
          data.videoId = videoId;
          data.language = $scope.selectedLanguage;
          var request = new Request(data);

          request.$save(function(response) {
            // $location.path('request/' + response._id);
            console.log(response);
            // $scope.requests.push(response);
            $mdDialog.cancel();
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Gửi đề xuất thành công!')
                    .position('top right')
                    .hideDelay(3000)
            );
          });

          $scope.request = {};

        } else {
          $scope.submitted = true;
        }
      };
    }

    
  }

})();

