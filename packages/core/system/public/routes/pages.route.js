angular
    .module('mean.system')
      .config(config);

      config.$inject = ['$stateProvider'];

      /* @ngInject */
      function config ($stateProvider) {
        $stateProvider.state('subtitling-community', {
          url: '/subtitling_community',
          templateUrl: '/system/pages/subtitling-community.html'
        })
      }
