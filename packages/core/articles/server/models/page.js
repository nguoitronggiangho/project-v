'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

/**
 * Tags Schema
 */
var PageSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  }
},{
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'upadted_at'
  }
});

PageSchema.path('name').validate(function (name) {
  return !!name;
}, 'Page name cannot be blank');

PageSchema.plugin(findOrCreate);

mongoose.model('Page', PageSchema);