'use strict';

//Articles service used for articles REST endpoint
angular.module('mean.articles').factory('Articles', ['$resource',
  function($resource) {
    return $resource('api/articles/:articleId', {
      articleId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      getAllPages: {
        method: 'GET',
        url: 'api/page',
        isArray: true
      },
      createPage: {
        method: 'POST',
        url: 'api/page'
      },
      getArticlesByPage: {
        method: 'GET',
        url: 'api/page/:page',
        isArray: true
      }
    });
  }
]);
